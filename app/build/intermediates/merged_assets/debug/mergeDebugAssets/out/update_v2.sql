ALTER TABLE [CMembers] ADD COLUMN [memSexID] Integer;
ALTER TABLE [CMembers] ADD COLUMN [NationalIDNo] nvarchar(17);
ALTER TABLE [CMembers] ADD COLUMN [PhoneNo] nvarchar(20);
ALTER TABLE [CMembers] ADD COLUMN [Walletno] nvarchar(20);
ALTER TABLE [CMembers] ADD COLUMN [AdmissionDate] datetime;
ALTER TABLE [CLoans] ADD COLUMN [ColcDate] datetime;
ALTER TABLE [Transact] ADD COLUMN [ProductSymbol] nchar(3);
ALTER TABLE [Transact] ADD COLUMN [Balance] Integer;
ALTER TABLE [Transact] ADD COLUMN [SMSStatus] Integer;
ALTER TABLE [POList] ADD COLUMN [SMSSupport] Integer;
ALTER TABLE [POList] ADD COLUMN [SurveySL] Integer;

CREATE TABLE IF NOT EXISTS [SurveySL] (
    _id INTEGER,
    [SDate] datetime,
    [OrgNo] nchar(4),
    [OrgMemNo] nchar(6),
    [Age] SMALLINT DEFAULT 0,
    [Education]  SMALLINT DEFAULT 0,
    [Home] SMALLINT DEFAULT 0,
    [OI_No] SMALLINT DEFAULT 0,
    [OI_DL] SMALLINT DEFAULT 0,
    [OI_OB] SMALLINT DEFAULT 0,
    [OI_PB] SMALLINT DEFAULT 0,
    [OI_OTHERS] SMALLINT DEFAULT 0,
    [OI_ISOURCE] SMALLINT DEFAULT 0,
    [CI_NO] SMALLINT DEFAULT 0,
    [CI_0_5] SMALLINT DEFAULT 0,
    [CI_5_10] SMALLINT DEFAULT 0,
    [CI_10_PLUS] SMALLINT DEFAULT 0,
    [CI_TOTAL] SMALLINT DEFAULT 0,
    [MA_CC] SMALLINT DEFAULT 0,
    [MA_OC] SMALLINT DEFAULT 0,
    [MA_CR] SMALLINT DEFAULT 0,
    [PL_O_O] SMALLINT DEFAULT 0,
    [PL_O_P] SMALLINT DEFAULT 0,
    [PL_L_O] SMALLINT DEFAULT 0,
    [PL_L_P] SMALLINT DEFAULT 0,
    [OA_LAND] SMALLINT DEFAULT 0,
    [OA_TV] SMALLINT DEFAULT 0,
    [OA_MC] SMALLINT DEFAULT 0,
    [OA_FR] SMALLINT DEFAULT 0,
    [PI_NGO] SMALLINT DEFAULT 0,
    [PI_BANK] SMALLINT DEFAULT 0,
    [PI_REL] SMALLINT DEFAULT 0,
    [PI_WS] SMALLINT DEFAULT 0,
    [PI_OS] SMALLINT DEFAULT 0,
    [Complete] SMALLINT DEFAULT 0,
    [UpdateAt] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
    [SentAt] datetime NULL,
    PRIMARY KEY ([SDate], [OrgNo], [OrgMemNo])
);

ALTER TABLE [SurveySL] ADD COLUMN  [UpdateAt] datetime DEFAULT (DateTime(current_timestamp, 'localtime'));
ALTER TABLE [SurveySL] ADD COLUMN  [SentAt] datetime NULL;

CREATE TABLE IF NOT EXISTS [SLSVOList] (
	[OrgNo] nchar(4) PRIMARY KEY,
	[OrgName] nvarchar(255),
	[OrgCategory] nchar(2)
);

CREATE TABLE IF NOT EXISTS [SLSMemList] (
	[OrgNo] nchar(4) PRIMARY KEY,
	[OrgMemNo] nvarchar(6),
	[MemberName] nvarchar(128),
	[memSexID] SMALLINT
);
