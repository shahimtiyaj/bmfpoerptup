ALTER TABLE [POList] ADD COLUMN [TransSlNo] int NULL DEFAULT 0;
ALTER TABLE [POList] ADD COLUMN [ProjectCode] Integer DEFAULT 0;
ALTER TABLE [POList] ADD COLUMN [LastSyncTime] datetime ;
ALTER TABLE [Transact] ADD COLUMN [TransSlNo] int NULL DEFAULT 0;
ALTER TABLE [TransTrail] ADD COLUMN [TrxType] nchar(13);

CREATE TABLE [TrxMapping] (
	[TrxShortType] nchar(2),
	[TrxTypeValue] int,
	[ColcFor] nchar(2),
    PRIMARY KEY ([TrxShortType], [TrxTypeValue], [ColcFor])
);
