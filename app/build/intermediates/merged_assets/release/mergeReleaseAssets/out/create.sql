CREATE TABLE [LoanStatus] (
	[LnStatus] Integer PRIMARY KEY,
	[StName] nvarchar(20)
);
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (0, 'Current');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (1, 'Closed');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (2, 'Late1');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (3, 'Late2');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (4, 'NIBL1');
INSERT INTO [LoanStatus] ([LnStatus], [StName]) VALUES (5, 'NIBL2');

CREATE TABLE [OrgCategories] (
	[OrgCategoryID] nchar(2) PRIMARY KEY,
	[OrgCategoryName] nvarchar(255),
	[Status] tinyint
);

CREATE TABLE [Projects] (
	[ProjectCode] nchar(3) PRIMARY KEY,
	[ProjectName] nvarchar(255),
	[OrgCategory] nchar(2),
	[Status] tinyint
);

CREATE TABLE [POList] (
	[CONo] nchar(8) PRIMARY KEY,
	[COName] nvarchar(255),
	[SessionNo] Integer NULL,
	[OpenDate] datetime NULL,
	[OpeningBal] Integer NULL,
	[Password] nvarchar(10) NULL,
	[EMethod] smallint NULL DEFAULT 0,
	[CashInHand] Integer NULL,
	[EnteredBy] nvarchar(10) NULL,
	[SYSDATE] datetime NULL,
	[SYSTIME] datetime NULL,
	[AltCOName] nvarchar(255),
	[BranchCode] nchar(4),
	[BranchName] nvarchar(255),
	[BODate] datetime NULL,
	[BOStatus] Integer,
	[SMSSupport] Integer DEFAULT 0,
	[SurveySL] Integer DEFAULT 0,
	[ProjectCode] Integer DEFAULT 0,
	[TransSlNo] Integer DEFAULT 0,
	[LastSyncTime] datetime

);
DELETE FROM POList;
INSERT INTO POList([CONo], [COName], [SessionNo], [OpenDate], [OpeningBal], [Password],
[EMethod], [CashInHand], [EnteredBy], [AltCOName], [BranchCode], [BranchName], [BODate],
[BOStatus], [SMSSupport], [SurveySL], [ProjectCode], [TransSlNo], [LastSyncTime])
	VALUES('00000000', 'Nobody', 0, '2015-02-03', 0, '', 0, 0, '00000000', 'Nobody', '0000', '', '2015-02-03', 1, 1, 0, 0, 0, '2015-01-01 12:00:00');

CREATE TABLE [VOList] (
	[OrgNo] nchar(4) PRIMARY KEY,
	[OrgName] nvarchar(255),
	[OrgCategory] nchar(2),
	[MemSexID] int,
	[SavColcOption] int,
	[LoanColcOption] int,
	[SavColcDate] datetime,
	[LoanColcDate] datetime,
	[CONo] nchar,
	[TargetDate] datetime,
	[StartDate] datetime,
	[EndDate] datetime,
	[NextTargetDate] datetime,
	[Territory] nvarchar(255),
	[UpdatedAt] datetime,
	[ProjectCode] nchar,
	[BranchCode] nchar
);

CREATE TABLE [LoanProducts] (
	[ProductNo] nchar(3),
	[ProductName] nvarchar(255),
	[IntrRate] real,
	[IntCalcMethod] tinyint,
	[IntRateIntrvlDays] int,
	[IntrFactorLoan] real,
	PRIMARY KEY ([ProductNo])
);

CREATE TABLE [SavingsProducts] (
	[ProductNo] nchar(3),
	[ProductName] nvarchar(255),
	PRIMARY KEY ([ProductNo])
);

CREATE TABLE [SavingsInterestPolicy] (
	[ProductNo] nchar(3),
	[OrgCategory] nchar(2),
	[ProjectCode] nchar(3),
	[MinSavBalance] int,
	[MaxSavBalance] int,
	[InterestRate] real,
	PRIMARY  KEY ([ProductNo], [OrgCategory], [ProjectCode])
);

CREATE TABLE [CMembers] (
	[ProjectCode] nchar(3),
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[MemberName] nvarchar(255),
	[SavBalan] Integer,
	[SavPayable] Integer,
	[CalcIntrAmt] real,
	[TargetAmtSav] Integer,
	[ReceAmt] Integer,
	[ColcDate] datetime,
	[AdjAmt] Integer,
	[SB] Integer,
	[SIA] Integer,
	[SavingsRealized] Integer,
	[memSexID] Integer,
	[NationalIDNo] nvarchar(17),
	[PhoneNo] nvarchar(20),
	[Walletno] nvarchar(20),
	[AdmissionDate] datetime,
	PRIMARY KEY ([OrgNo], [OrgMemNo])
);

CREATE TABLE [CLoans] (
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[ProjectCode] nchar(3),
	[LoanNo] Integer,
	[LoanSlNo] Integer,
	[ProductNo] nchar(3),
	[IntrFactorLoan] real,
	[PrincipalAmt] Integer,
	[SchmCode] nchar(3),
	[InstlAmtLoan] Integer,
	[DisbDate] datetime,
	[LnStatus] Integer,
	[PrincipalDue] Integer,
	[InterestDue] Integer,
	[TotalDue] Integer,
	[TargetAmtLoan] Integer,
	[TotalReld] Integer,
	[Overdue] Integer,
	[BufferIntrAmt] real,
	[ReceAmt] Integer,
	[IAB] Integer,
	[ODB] Integer,
	[TRB] Integer,
	[LB] Integer,
	[PDB] Integer,
	[IDB] Integer,
	[InstlPassed] Integer,
	[OldInterestDue] real,
	[ProductSymbol] nchar(3),
	[LoanRealized] Integer,
	[ColcDate] datetime,
	PRIMARY KEY ([OrgNo], [OrgMemNo], [LoanNo])
);
CREATE TABLE [Transact] (
	_id INTEGER PRIMARY KEY,
	[TransSlNo] int NULL DEFAULT 0,
	[ColcID] int NULL DEFAULT 0,
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[ProjectCode] nchar(3),
	[LoanNo] int,
	[ColcAmt] Integer,
	[SavAdj] Integer,
	[ColcDate] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
	[ColcFor] nchar(1),
	[ProductSymbol] nchar(3),
    [Balance] Integer DEFAULT 0,
    [SMSStatus] Integer DEFAULT 0
);

CREATE TABLE [WPTransact] (
	_id  INTEGER PRIMARY KEY,
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[ProjectCode] nchar(3),
	[LoanNo] int,
	[ColcAmt] Integer,
	[ColcDate] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
	[ColcFor] nchar(1),
	[Verified] nchar(1) DEFAULT '0'
);

CREATE TABLE [TransTrail] (
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[ProjectCode] nchar(3),
	[LoanNo] int,
	[Tranamount] Integer,
	[ColcDate] datetime NOT NULL,
	[Transno] nchar(13),
	[TrxType] nchar(2) NOT NULL,
	[ColcFor] nchar(1),
	[UpdatedAt] datetime NULL,
	PRIMARY KEY ([Transno])
);

CREATE TABLE [GoodLoans] (
	[OrgNo] nchar(4),
	[OrgMemNo] nchar(6),
	[MaxAmt] int,
	[Taken] nchar(1),
	[UpdatedAt] datetime NULL,
	[_Month] Integer,
	[_Year] Integer,
	PRIMARY KEY ([OrgNo], [OrgMemNo])
);

CREATE TABLE [SurveySL] (
    _id INTEGER,
    [SDate] datetime,
    [OrgNo] nchar(4),
    [OrgMemNo] nchar(6),
    [Age] SMALLINT DEFAULT 0,
    [Education]  SMALLINT DEFAULT 0,
    [Home] SMALLINT DEFAULT 0,
    [OI_No] SMALLINT DEFAULT 0,
    [OI_DL] SMALLINT DEFAULT 0,
    [OI_OB] SMALLINT DEFAULT 0,
    [OI_PB] SMALLINT DEFAULT 0,
    [OI_OTHERS] SMALLINT DEFAULT 0,
    [OI_ISOURCE] SMALLINT DEFAULT 0,
    [CI_NO] SMALLINT DEFAULT 0,
    [CI_0_5] SMALLINT DEFAULT 0,
    [CI_5_10] SMALLINT DEFAULT 0,
    [CI_10_PLUS] SMALLINT DEFAULT 0,
    [CI_TOTAL] SMALLINT DEFAULT 0,
    [MA_CC] SMALLINT DEFAULT 0,
    [MA_OC] SMALLINT DEFAULT 0,
    [MA_CR] SMALLINT DEFAULT 0,
    [PL_O_O] SMALLINT DEFAULT 0,
    [PL_O_P] SMALLINT DEFAULT 0,
    [PL_L_O] SMALLINT DEFAULT 0,
    [PL_L_P] SMALLINT DEFAULT 0,
    [OA_LAND] SMALLINT DEFAULT 0,
    [OA_TV] SMALLINT DEFAULT 0,
    [OA_MC] SMALLINT DEFAULT 0,
    [OA_FR] SMALLINT DEFAULT 0,
    [PI_NGO] SMALLINT DEFAULT 0,
    [PI_BANK] SMALLINT DEFAULT 0,
    [PI_REL] SMALLINT DEFAULT 0,
    [PI_WS] SMALLINT DEFAULT 0,
    [PI_OS] SMALLINT DEFAULT 0,
    [Complete] SMALLINT DEFAULT 0,
    [UpdateAt] datetime NOT NULL DEFAULT (DateTime(current_timestamp, 'localtime')),
    [SentAt] datetime NULL,
    PRIMARY KEY ([SDate], [OrgNo], [OrgMemNo])
);

CREATE TABLE [SLSVOList] (
	[OrgNo] nchar(4) PRIMARY KEY,
	[OrgName] nvarchar(255),
	[OrgCategory] nchar(2)
);

CREATE TABLE [SLSMemList] (
	[OrgNo] nchar(4),
	[OrgMemNo] nvarchar(6),
	[MemberName] nvarchar(128),
	[memSexID] SMALLINT,
	PRIMARY KEY ([OrgNo], [OrgMemNo])
);

CREATE TABLE [TrxMapping] (
	[TrxShortType] nchar(2),
	[TrxTypeValue] int,
	[ColcFor] nchar(2),
    PRIMARY KEY ([TrxShortType], [TrxTypeValue], [ColcFor])
);


UPDATE [CMembers] SET [SB]=[SavBalan], [SIA]=[TargetAmtSav];
UPDATE [CLoans] SET [IAB]=CASE WHEN [TargetAmtLoan] > IfNull([ReceAmt], 0) THEN [TargetAmtLoan] - IfNull([ReceAmt], 0)
ELSE 0 END, [ODB]=[Overdue], [TRB]=[TotalReld], [LB]=[PrincipalDue]+[InterestDue], [PDB]=[PrincipalDue], [IDB]=[InterestDue];