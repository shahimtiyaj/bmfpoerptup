package net.qsoft.brac.bmfpoerptup;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.data.VO;

import java.util.ArrayList;
import java.util.HashMap;

public class GoodLoanListActivity extends SSActivity {
	private static final String TAG = GoodLoanListActivity.class.getSimpleName();

	ListView lv = null;
	ArrayList<HashMap<String, String>> items=null;
	String vono=null;
	Button cmdOK;
	TextView branchName;
	TextView voName;

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_good_loan_list);
		cmdOK = (Button) findViewById(R.id.okButton);
		cmdOK.setVisibility(View.GONE);
		branchName = (TextView) findViewById(R.id.textBranchName);
		voName = (TextView) findViewById(R.id.textVOName);
		
		lv = (ListView) findViewById(R.id.listViewGLL);
		
		if(getIntent().hasExtra(P8.VONO)) {
			vono = getIntent().getExtras().getString(P8.VONO);
//			Log.d(TAG, "In has extra " + vono);
		} 
		if(VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}			

		createGoddLoanList(vono);
	}

	private void createGoddLoanList(String von) {
		Log.d(TAG, "VO No.: " + (von==null ? "": von));
		DAO da = new DAO(this);
		da.open();
		PO po = da.getPO();
		VO vo = da.getVO(von);
		items = da.getGoodLoanByVO(von);
//		Log.d(TAG, "Items: " + items );
		da.close();
		
		branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());
		voName.setText(von + " - " + vo.get_OrgName());
		
        String[] from = new String[] {"MemNo", "MemName", "MaxAmt", "Taken"};
        int[] to = { R.id.textMemNo, R.id.textMemName, R.id.textMaxAmt,R.id.textRem };

        SimpleAdapter  adapter = new SimpleAdapter(getApplicationContext(), items,
                R.layout.good_loan_row, from, to);
        lv.setAdapter(adapter);
	}

	public void onCancel(View view) {
		// Back
		finish();
	}	
}
