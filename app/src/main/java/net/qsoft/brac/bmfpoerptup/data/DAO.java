package net.qsoft.brac.bmfpoerptup.data;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import net.qsoft.brac.bmfpoerptup.App;
import net.qsoft.brac.bmfpoerptup.P8;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


public class DAO {
    private static final String TAG = DAO.class.getSimpleName();
    public String req;

    public enum DBoardItem {
        VOS, MEMBERS, BORROWERS, CURBORROWERS, LOANS, CURLOANS, L1LOANS, L2LOANS, N1LOANS, N2LOANS, OUTLOANS, OVERLOANS, SAVE_TARGET, SAVE_YET_COLLECTED, REPAY_TARGET, REPAY_YET_COLLECTED, PO_CASH_IN_HAND
    }

    // Loan status type constans
    public static final int LS_CURRENT = 0;
    public static final int LS_CLOSED = 1;
    public static final int LS_LATE1 = 2;
    public static final int LS_LATE2 = 3;
    public static final int LS_NIBL1 = 4;
    public static final int LS_NIBL2 = 5;

    // Database fields
    private SQLiteDatabase db;
    private DBHelper dbHelper;

    public DAO(Context context) {
        dbHelper = new DBHelper(context);
    }

    protected void finalize() throws Throwable {
        // TODO Auto-generated method stub
        if (db != null && db.isOpen())
            db.close();
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    //
    public Cursor getRecordsCursor(String sql, String[] param) {
        Cursor curs = null;
        curs = db.rawQuery(sql, param);
        return curs;
    }

//	public static Cursor getRecords(String sql, String[] param) {
//		Cursor curs = null;
//		DAO da = new DAO(App.getContext());
//		da.open();
//		try {
//			curs = da.getRecordsCursor(sql, param);
//		}
//		catch (Exception e){
//			throw e;
//		}
//		finally {
//			da.close();
//		}
//
//		return curs;
//	}


    public void execSQL(String sql, String[] param) throws SQLException {
        db.execSQL(sql, param);
    }

    public static void executeSQL(String sql, String[] param) {
        DAO da = new DAO(App.getContext());
        da.open();
        try {
            da.execSQL(sql, param);
        } catch (Exception e) {
            throw e;
        } finally {
            da.close();
        }
    }

    public void WriteSyncTime(String dt) {
        ContentValues cv = new ContentValues();
        cv.clear();
        cv.put("[SYSDATE]", dt);
        //cv.put("[LastSyncTime]", dt);
        db.update(DBHelper.TBL_PO, cv, null, null);
    }


    public void UpdateEMethod(int emethod) {
        ContentValues cv = new ContentValues();
        cv.clear();
        cv.put("[EMethod]", emethod);
        db.update(DBHelper.TBL_PO, cv, null, null);
    }

    public void UpdateTranSL(int sl) {
        ContentValues cv = new ContentValues();
        cv.clear();
        cv.put("[TransSlNo]", sl);
        db.update(DBHelper.TBL_PO, cv, null, null);
    }

    public void UpdateEColcDate(String colcDate) {
        ContentValues cv = new ContentValues();
        cv.clear();
        cv.put("[ColcDate]", String.valueOf(P8.ConvertStringToDate(colcDate, "yyyy-MM-dd")));
        db.update(DBHelper.TBL_CMEMBERS, cv, null, null);
    }

    public String GetSyncTime() {
        String ret = "";
        Cursor curs = null;

        try {
            curs = db
                    .rawQuery("SELECT [SYSDATE] FROM " + DBHelper.TBL_PO, null);
            //.rawQuery("SELECT [LastSyncTime] FROM " + DBHelper.TBL_PO, null);

            if (curs.moveToFirst()) {
                ret = curs.getString(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return ret;
    }


    public Branch getBranch() {
        Branch br = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_BRANCH, new String[]{"BranchCode",
                            "BranchName", "BranchOpeningStatus", "BranchOpenDate",
                            "BranchCloseDate", "OpeningDayType"}, null, null, null,
                    null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                br = new Branch(curs.getString(0), curs.getString(1),
                        curs.getInt(2), curs.getString(3), curs.getString(4),
                        curs.getString(5));
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return br;
    }

    public PO getPO() {
        PO cl = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_PO, new String[]{"CONo", "COName",
                    "SessionNo", "OpenDate", "OpeningBal", "Password",
                    "EMethod", "CashInHand", "EnteredBy", "SYSDATE", "SYSTIME",
                    "AltCOName", "BranchCode", "BranchName", "BODate",
                    "BOStatus", "SMSSupport", "SurveySL", "ProjectCode", "TransSlNo"}, null, null, null, null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                cl = new PO(curs.getString(0), curs.getString(1),
                        curs.getInt(2), curs.getString(3), curs.getInt(4),
                        curs.getString(5), curs.getInt(6), curs.getInt(7),
                        curs.getString(8), curs.getString(9),
                        curs.getString(10), curs.getString(11),
                        curs.getString(12), curs.getString(13),
                        curs.getString(14), curs.getInt(15),
                        curs.getInt(16), curs.getInt(17),
                        curs.getInt(18), curs.getInt(19));
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return cl;

    }

    public VO getvo() {
        VO cl = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_VOLIST, new String[]{
                            "[OrgNo]", "[OrgName]", "[OrgCategory]", "[MemSexID]",
                            "[SavColcOption]", "[LoanColcOption]", "[SavColcDate]",
                            "[LoanColcDate]", "[TargetDate]"},
                    null, null, null, null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                cl = new VO(curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getInt(3),
                        curs.getInt(4),
                        curs.getInt(5),
                        P8.ConvertStringToDate(curs.getString(6), "yyyy-MM-dd"),
                        P8.ConvertStringToDate(curs.getString(7), "yyyy-MM-dd"),
                        P8.ConvertStringToDate(curs.getString(8), "yyyy-MM-dd"));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return cl;
    }

    public List<VO> getVOList() {
        List<VO> list = new ArrayList<VO>();
        VO vo = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_VOLIST, new String[]{"[OrgNo]",
                    "[OrgName]", "[OrgCategory]", "[MemSexID]",
                    "[SavColcOption]", "[LoanColcOption]", "[SavColcDate]",
                    "[LoanColcDate]", "[TargetDate]"}, null, null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    vo = new VO(curs.getString(0), curs.getString(1),
                            curs.getString(2), curs.getInt(3), curs.getInt(4),
                            curs.getInt(5), P8.ConvertStringToDate(
                            curs.getString(6), "yyyy-MM-dd"),
                            P8.ConvertStringToDate(curs.getString(7),
                                    "yyyy-MM-dd"), P8.ConvertStringToDate(curs.getString(8), "yyyy-MM-dd"));

                    list.add(vo);
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return list;
    }

    public List<VO> getTodaysVO(List<VO> srcVO) {
        List<VO> lst = new ArrayList<VO>();
        if (srcVO == null || srcVO.isEmpty())
            srcVO = getVOList();

        for (VO vo : srcVO) {
            if (vo.isTodaysVO()) {
                lst.add(vo);
            }
        }
        return lst;
    }

    public VO getVO(String vono) {
        VO vo = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_VOLIST, new String[]{"[OrgNo]",
                            "[OrgName]", "[OrgCategory]", "[MemSexID]",
                            "[SavColcOption]", "[LoanColcOption]", "[SavColcDate]",
                            "[LoanColcDate]", "[TargetDate]"}, "[OrgNo]=?", new String[]{vono},
                    null, null, null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                vo = new VO(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getInt(3),
                        curs.getInt(4),
                        curs.getInt(5),
                        P8.ConvertStringToDate(curs.getString(6), "yyyy-MM-dd"),
                        P8.ConvertStringToDate(curs.getString(7), "yyyy-MM-dd"),
                        P8.ConvertStringToDate(curs.getString(8), "yyyy-MM-dd"));
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return vo;
    }

    public Integer getRecordCount(String tblSql, String[] cond) {
        Integer cnt = 0;
        Cursor curs = null;

        Log.d(TAG, "getRecordCount: " + tblSql);

        try {
            curs = db.rawQuery(tblSql, cond);
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                cnt = curs.getInt(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return cnt;
    }

    public Integer getMemberCount(String VONo) {
        Integer cnt = 0;

        if (VONo == null || VONo.isEmpty())
            cnt = getRecordCount("SELECT Count(*) as cnt FROM "
                    + DBHelper.TBL_CMEMBERS, null);
        else
            cnt = getRecordCount("SELECT Count(*) as cnt FROM "
                            + DBHelper.TBL_CMEMBERS + " WHERE [OrgNo]=?",
                    new String[]{VONo});

        return cnt;
    }

    /*
     * public Integer getTodaysVOCount() { return
     * getRecordCount("SELECT Count(*) as cnt FROM " + DBHelper.TBL_VOLIST +
     * " WHERE ", cond) }
     */

    public Integer getVOCount(String vono) {
        Integer cnt = 0;
        Cursor curs = null;
        if (vono == null || vono.isEmpty())
            vono = "%";
        try {
            curs = db.rawQuery("SELECT COUNT(*) AS cnt FROM "
                            + DBHelper.TBL_VOLIST + " WHERE [OrgNO] LIKE ?",
                    new String[]{vono});
            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                cnt = curs.getInt(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return cnt;

    }

    public Integer getCollectionTotal(String vono) {
        Integer ret = 0;
        Cursor curs = null;
        if (vono == null || vono.isEmpty())
            vono = "%";
        String[] vv = {vono};

        try {
            curs = db.rawQuery("SELECT Sum([ColcAmt]) AS amt FROM "
                    + DBHelper.TBL_TRANSACT + " WHERE [OrgNO] LIKE ?", vv);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                ret = curs.getInt(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return ret;
    }

    public Integer getCollectionTotal(String vono, String colcfor) {
        Integer ret = 0;
        Cursor curs = null;
        if (vono == null || vono.isEmpty())
            vono = "%";
        String[] vv = {vono, colcfor};

        try {
            curs = db.rawQuery("SELECT Sum([ColcAmt]) AS amt FROM "
                    + DBHelper.TBL_TRANSACT
                    + " WHERE [OrgNO] LIKE ? AND [ColcFor]=?", vv);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                ret = curs.getInt(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return ret;
    }

    public ArrayList<HashMap<String, String>> getCollectionItems(String vono) {
        ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
        if (vono == null || vono.isEmpty())
            vono = "%";
        String[] vv = {vono};
        String sql = "";

        // Reading all values
        Log.d("Generating: ", "Collection items for listview ...");

        try {
            // curs =
            // db.rawQuery("select T.[OrgNo], M.[OrgName], T.[OrgMemNo], M.[MemberName], T.[ColcAmt], T.[ColcFor] "
            // +
            // "from [Transact] as T inner join ( select C.[OrgMemNo], C.[MemberName],  C.[OrgNo], V.[OrgName] "
            // +
            // "from [CMembers] as C inner join [VOList] as V ON C.[OrgNo]=V.[OrgNo] ) as M"
            // +
            // "ON T.[OrgNo]=M.[OrgNO] AND T.[OrgMemNo] = M.[OrgMemNo] " +
            // "where T.[OrgNo] LIKE ? order by T.[OrgNo], T.[OrgMemNo]", vv);
            sql = "select T.[OrgNo], M.[OrgName], T.[OrgMemNo], M.[MemberName], T.[ColcAmt], T.[ColcFor] "
                    + "from [Transact] as T inner join ( select C.[OrgMemNo], C.[MemberName],  C.[OrgNo], V.[OrgName] "
                    + "from [CMembers] as C inner join [VOList] as V ON C.[OrgNo]=V.[OrgNo] ) as M "
                    + "ON T.[OrgNo]=M.[OrgNO] AND T.[OrgMemNo] = M.[OrgMemNo] "
                    + "where T.[OrgNo] LIKE ? order by T.[OrgNo], T.[OrgMemNo]";

            Log.d(TAG, sql);

            curs = db.rawQuery(sql, vv);

            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                String orgno = "";
                do {
                    map = new HashMap<String, String>();

                    map.put("VONo", curs.getString(0));
                    if (!orgno.equals(curs.getString(0))) {
                        orgno = curs.getString(0);
                        map.put("VOName", curs.getString(1));
                    } else {
                        map.put("VOName", "");
                    }
                    map.put("MemNo", curs.getString(2));
                    map.put("MemName", curs.getString(3));
                    map.put("Amt", Integer.toString(curs.getInt(4)));
                    map.put("Type", curs.getString(5));

                    items.add(map);

                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return items;
    }

    private void CalculateStat(ArrayList<HashMap<String, String>> items,
                               String VONo) {
        Cursor curs = null;
        HashMap<String, String> loans;
        Integer cnt = 0, cntCurbor = 0, cntOD = 0, amtOD = 0, cntOut = 0, amtOut = 0, cntYTS = 0, amtYTS = 0, cntTSR = 0, amtTSR = 0;
        Integer tcnt = 0, tamt = 0, cntIAT = 0, amtIAT = 0, cntRT = 0, amtRT = 0;
        if (VONo == null || VONo.isEmpty())
            VONo = "%";
        String[] vv = {VONo};

        try {
            curs = db
                    .rawQuery(
                            "select count(*) AS BORROWER, sum(LNCount) AS MCNT,  sum(CURLNS) AS CURBOR "
                                    + "from (SELECT   [OrgNo], [OrgMemNo], Count([LoanNo]) as LNCount,  Max(CASE WHEN IfNull([LoanNo], 0) > 0 AND IfNull([LnStatus],0) = 0 THEN 1 ELSE 0 END) as CURLNS "
                                    + "FROM  CLoans WHERE [OrgNo] LIKE ? AND [LB] > 0 group by OrgNo, OrgMemNo )",
                            vv);
            if (curs.moveToFirst()) {
                tcnt = curs.getInt(0);
                cnt = curs.getInt(1);
                cntCurbor = curs.getInt(2);
            }
            curs.close();

            dbItemsAdd(items, DBoardItem.BORROWERS.toString(),
                    "No. of Borrowers", tcnt.toString(), "");

            dbItemsAdd(items, DBoardItem.CURBORROWERS.toString(),
                    "No. of Curr. Borrowers", cntCurbor.toString(), "");

            loans = dbItemsAdd(items, DBoardItem.LOANS.toString(),
                    "No. of Loans", cnt.toString(), "");

            curs = db
                    .rawQuery(
                            "SELECT "
                                    + "Sum(CASE WHEN IfNull(CM.[SIA], 0) > 0 THEN 1 ELSE 0 END) as TSaveCnt, "
                                    + "Sum(CASE WHEN IfNull(CM.[SIA], 0) > 0 THEN IfNull(CM.[SIA], 0) ELSE 0 END) as TSave, "
                                    + "Sum(CASE WHEN IfNull(CM.[TargetAmtSav], 0) > 0 THEN 1 ELSE 0 END) as TReceCnt, "
                                    + "Sum(CASE WHEN IfNull(CM.[TargetAmtSav], 0) > 0 THEN IfNull(CM.[TargetAmtSav], 0) ELSE 0 END) as TRece "
                                    + "FROM CMembers CM INNER JOIN VOList VO ON CM.[OrgNo]=VO.[OrgNo] "
                                    + "WHERE CM.[OrgNo] LIKE ?", vv);

            /*
             * curs = db.rawQuery( "SELECT " +
             * "Sum(CASE WHEN VO.[TargetDate] = Date('now') AND IfNull(CM.[SIA], 0) > 0 THEN 1 ELSE 0 END) as TSaveCnt, "
             * +
             * "Sum(CASE WHEN VO.[TargetDate] = Date('now') THEN IfNull(CM.[SIA], 0) ELSE 0 END) as TSave, "
             * +
             * "Sum(CASE WHEN VO.[TargetDate] = Date('now') AND IfNull(CM.[ReceAmt], 0) > 0 THEN 1 ELSE 0 END) as TReceCnt, "
             * +
             * "Sum(CASE WHEN VO.[TargetDate] = Date('now') THEN IfNull(CM.[ReceAmt], 0) ELSE 0 END) as TRece "
             * +
             * "FROM CMembers CM INNER JOIN VOList VO ON CM.[OrgNo]=VO.[OrgNo] "
             * + "WHERE CM.[OrgNo] LIKE ?", vv);
             */

            if (curs.moveToFirst()) {
                cntYTS = curs.getInt(0);
                amtYTS = curs.getInt(1);
                cntTSR = curs.getInt(2);
                amtTSR = curs.getInt(3);
            }
            curs.close();

            curs = db
                    .rawQuery(
                            "SELECT LS.LnStatus, LS.STNAME, COUNT(LN.LnStatus) AS CNT, "
                                    + "Sum(CASE When IfNull(LN.LB, 0) > 0 THEN 1 ELSE 0 END) AS cntOLB, "
                                    + "Sum(CASE When IfNull(LN.LB, 0) > 0 THEN IfNull(LN.LB, 0) ELSE 0 END) as OLB, "
                                    + "Sum(CASE When IfNull(LN.ODB, 0) > 0 THEN 1 ELSE 0 END) AS cntODB, "
                                    + "Sum(CASE When IfNull(LN.ODB, 0) > 0 THEN IfNull(LN.ODB, 0) ELSE 0 END) AS ODB, "
                                    + "Sum(CASE When IfNull(LN.IAB, 0) > 0 THEN 1 ELSE 0 END) AS cntIAB, "
                                    + "Sum(CASE When IfNull(LN.IAB, 0) > 0 THEN IfNull(LN.IAB, 0) ELSE 0 END) AS IAB, "
                                    + "Sum(CASE When IfNull(LN.TargetAmtLoan, 0) > 0 THEN 1 ELSE 0 END) AS cntTA, "
                                    + "Sum(CASE When IfNull(LN.TargetAmtLoan, 0) > 0 THEN IfNull(LN.TargetAmtLoan, 0) ELSE 0 END) AS TA "
                                    + "FROM LoanStatus LS LEFT OUTER JOIN CLoans LN on ln.LnStatus=LS.LnStatus "
                                    + "LEFT OUTER JOIN VOList VO ON LN.[OrgNo]=VO.[OrgNo] WHERE LN.[OrgNo] LIKE ?"
                                    + "GROUP BY LS.LnStatus, LS.STNAME ORDER BY LS.LnStatus",
                            vv);

            /*
             * "SELECT LS.LnStatus, LS.STNAME, COUNT(LN.LnStatus) AS CNT, " +
             * "Sum(CASE When IfNull(LN.LB, 0) > 0 THEN 1 ELSE 0 END) AS cntOLB, "
             * +
             * "Sum(CASE When IfNull(LN.LB, 0) > 0 THEN IfNull(LN.LB, 0) ELSE 0 END) as OLB, "
             * +
             * "Sum(CASE When IfNull(LN.ODB, 0) > 0 THEN 1 ELSE 0 END) AS cntODB, "
             * +
             * "Sum(CASE When IfNull(LN.ODB, 0) > 0 THEN IfNull(LN.ODB, 0) ELSE 0 END) AS ODB, "
             * +
             * "Sum(CASE When VO.[TargetDate] = Date('now') AND IfNull(LN.IAB, 0) > 0 THEN 1 ELSE 0 END) AS cntIAB, "
             * +
             * "Sum(CASE When VO.[TargetDate] = Date('now') AND IfNull(LN.IAB, 0) > 0 THEN IfNull(LN.IAB, 0) ELSE 0 END) AS IAB, "
             * +
             * "Sum(CASE When VO.[TargetDate] = Date('now') AND IfNull(LN.ReceAmt, 0) > 0 THEN 1 ELSE 0 END) AS cntRA, "
             * +
             * "Sum(CASE When VO.[TargetDate] = Date('now') AND IfNull(LN.ReceAmt, 0) > 0 THEN IfNull(LN.ReceAmt, 0) ELSE 0 END) AS RA "
             * +
             * "FROM LoanStatus LS LEFT OUTER JOIN CLoans LN on ln.LnStatus=LS.LnStatus "
             * +
             * "LEFT OUTER JOIN VOList VO ON LN.[OrgNo]=VO.[OrgNo] WHERE LN.[OrgNo] LIKE ?"
             * + "GROUP BY LS.LnStatus, LS.STNAME ORDER BY LS.LnStatus", vv);
             */

            cnt = 0;
            if (curs.moveToFirst()) {
                do {
                    tcnt = curs.getInt(2);
                    tamt = curs.getInt(4);

                    cnt += tcnt;

                    cntOut += curs.getInt(3);
                    amtOut += curs.getInt(4);

                    cntIAT += curs.getInt(7);
                    amtIAT += curs.getInt(8);
                    cntRT += curs.getInt(9);
                    amtRT += curs.getInt(10);

                    switch (curs.getInt(0)) {
                        case LS_CURRENT:
                            cntOD += curs.getInt(5);
                            amtOD += curs.getInt(6);

                            dbItemsAdd(items, DBoardItem.CURLOANS.toString(),
                                    curs.getString(1) + " Loan", tcnt.toString(),
                                    tamt.toString());
                            break;

                        case LS_CLOSED:

                            break;

                        case LS_LATE1:
                            dbItemsAdd(items, DBoardItem.L1LOANS.toString(),
                                    curs.getString(1), tcnt.toString(),
                                    tamt.toString());
                            break;

                        case LS_LATE2:
                            dbItemsAdd(items, DBoardItem.L2LOANS.toString(),
                                    curs.getString(1), tcnt.toString(),
                                    tamt.toString());
                            break;

                        case LS_NIBL1:
                            dbItemsAdd(items, DBoardItem.N1LOANS.toString(),
                                    curs.getString(1), tcnt.toString(),
                                    tamt.toString());
                            break;

                        case LS_NIBL2:
                            dbItemsAdd(items, DBoardItem.N2LOANS.toString(),
                                    curs.getString(1), tcnt.toString(),
                                    tamt.toString());

                    }

                } while (curs.moveToNext());
                loans.put("Num", cnt.toString());
                loans.put("Amt", "");

                dbItemsAdd(items, DBoardItem.OUTLOANS.toString(),
                        "Loan outstanding", cntOut.toString(),
                        amtOut.toString());
                dbItemsAdd(items, DBoardItem.OVERLOANS.toString(),
                        "Curr. Overdue", cntOD.toString(), amtOD.toString());

                // if (VONo == "%") {
                dbItemsAdd(items, DBoardItem.SAVE_TARGET.toString(),
                        "Savings Target", cntTSR.toString(), amtTSR.toString());
                /*
                 * dbItemsAdd(items, DBoardItem.SAVE_YET_COLLECTED.toString(),
                 * "Savings Yet Collected", cntYTS.toString(),
                 * amtYTS.toString());
                 */
                dbItemsAdd(items, DBoardItem.REPAY_TARGET.toString(),
                        "Inst. Collec. Target", cntRT.toString(),
                        amtRT.toString());
                dbItemsAdd(items, DBoardItem.REPAY_YET_COLLECTED.toString(),
                        "Inst. Yet to Collect", cntIAT.toString(),
                        amtIAT.toString());
                // }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

    }

    private HashMap<String, String> dbItemsAdd(
            ArrayList<HashMap<String, String>> items, String strId,
            String strDesc, String strNum, String strAmt) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Id", strId);
        map.put("Desc", strDesc);
        map.put("Num", strNum);
        map.put("Amt", strAmt);
        items.add(map);

        return map;
    }

    public ArrayList<HashMap<String, String>> getDashboardItems(String vono) {
        ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
        int i = 0;

        // Reading all values
        Log.d("Generating: ", "Dashboard items for listview ...");

        // Total VO
        if (vono == null)
            dbItemsAdd(items, DBoardItem.VOS.toString(), "No. of Group/VO",
                    getVOCount(vono).toString(), "");

        // Member count
        dbItemsAdd(items, DBoardItem.MEMBERS.toString(), "No. of Member",
                getMemberCount(vono).toString(), "");

        CalculateStat(items, vono);

        // PO cash in hand
        // if (vono == null) {
        // PO po = getPO();
        dbItemsAdd(items, DBoardItem.PO_CASH_IN_HAND.toString() + "S",
                "Collection Savings", "", getCollectionTotal(vono, "S")
                        .toString());

        dbItemsAdd(items, DBoardItem.PO_CASH_IN_HAND.toString() + "L",
                "Collection Loan", "", getCollectionTotal(vono, "L").toString());

        dbItemsAdd(items, DBoardItem.PO_CASH_IN_HAND.toString(),
                "Collection Total", "", getCollectionTotal(vono).toString());
        /*
         * } else { dbItemsAdd(items, DBoardItem.PO_CASH_IN_HAND.toString(),
         * "Collection Total", getCollectionTotal(vono, "S").toString(),
         * getCollectionTotal(vono, "L").toString()); }
         */
        return items;
    }

    public ArrayList<HashMap<String, String>> getWPMemberList() {
        String sql = "SELECT DISTINCT " + DBHelper.TBL_CMEMBERS + "."
                + DBHelper.FLD_ORG_NO + ", " + DBHelper.TBL_CMEMBERS + "."
                + DBHelper.FLD_ORG_MEM_NO + "," + DBHelper.TBL_CMEMBERS + "."
                + DBHelper.FLD_ORG_MEM_NAME + " " + "FROM" + " "
                + DBHelper.TBL_CMEMBERS + " " + "INNER JOIN" + " "
                + DBHelper.TBL_WPTRANSACT + " " + "ON" + " "
                + DBHelper.TBL_CMEMBERS + "." + DBHelper.FLD_ORG_MEM_NO + "="
                + DBHelper.TBL_WPTRANSACT + "." + DBHelper.FLD_ORG_MEM_NO;

        return getMemberList(sql + ";", null);
    }

    public ArrayList<HashMap<String, String>> getMemberList(DBoardItem dbitm,
                                                            String vono) {
        String sql = "";
        String[] param = null;

        Log.d(TAG, "DBITEM: " + dbitm.toString() + "  VONO: "
                + ((vono == null) ? "null" : vono));

        switch (dbitm) {
            case MEMBERS:
                sql = "SELECT " + DBHelper.FLD_ORG_NO + ", "
                        + DBHelper.FLD_ORG_MEM_NO + ", "
                        + DBHelper.FLD_ORG_MEM_NAME + " " + "FROM "
                        + DBHelper.TBL_CMEMBERS;
                if (!(vono == null || vono.length() == 0)) {
                    sql += " WHERE " + DBHelper.FLD_ORG_NO + " = ?";
                    param = new String[]{vono};
                }
                Log.d(TAG, sql);
                break;

            case BORROWERS:
            case LOANS:
            case OUTLOANS:
                sql = MembersSql(vono);
                Log.d(TAG, sql);
                break;

            case CURBORROWERS:
            case CURLOANS:
                sql = MembersSql(vono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_CURRENT;

                break;
            case L1LOANS:
                sql = MembersSql(vono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_LATE1;
                break;
            case L2LOANS:
                sql = MembersSql(vono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_LATE2;
                break;
            case N1LOANS:
                sql = MembersSql(vono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_NIBL1;
                break;
            case N2LOANS:
                sql = MembersSql(vono) + " AND LN." + DBHelper.FLD_LOAN_STATUS
                        + "=" + LS_NIBL2;
                break;
            case OVERLOANS:
                sql = MembersSql(vono) + " AND LN." + DBHelper.FLD_OVERDUE_BAL
                        + " > 0 AND LN." + DBHelper.FLD_LOAN_STATUS + "="
                        + LS_CURRENT;
                break;
            case PO_CASH_IN_HAND:
                break;
            case REPAY_YET_COLLECTED:
                sql = MembersSql(vono, " AND IfNull(LN.IAB, 0) > 0");
                break;
            case REPAY_TARGET:
                sql = MembersSql(vono) + " AND IfNull(LN.TargetAmtLoan, 0) > 0";
                break;
            case SAVE_YET_COLLECTED:
                sql = MembersSql(vono) + " AND IfNull(CM.SIA, 0) > 0";
                break;
            case SAVE_TARGET:
                sql = MembersSql(vono) + " AND IfNull(CM.TargetAmtSav, 0) > 0";
                break;
            case VOS:
                break;
            default:
                break;

        }
        return getMemberList(sql + ";", param);
    }

    private String MembersSql(String vono) {

        String sql = "SELECT DISTINCT CM." + DBHelper.FLD_ORG_NO + ", CM."
                + DBHelper.FLD_ORG_MEM_NO + ", CM." + DBHelper.FLD_ORG_MEM_NAME
                + " FROM " + DBHelper.TBL_CMEMBERS + " CM INNER JOIN "
                + DBHelper.TBL_CLOANS + " LN ON CM." + DBHelper.FLD_ORG_NO
                + " = LN." + DBHelper.FLD_ORG_NO + " AND CM."
                + DBHelper.FLD_ORG_MEM_NO + " = LN." + DBHelper.FLD_ORG_MEM_NO;
        sql += " WHERE IfNull(LN." + DBHelper.FLD_LOAN_NO + ", 0) > 0";
        if (!(vono == null || vono.length() == 0)) {
            sql += " AND CM." + DBHelper.FLD_ORG_NO + " = '" + vono + "'";
        }

        return sql;
    }

    public String GetTargetDate(String orgno) {
        String selectQuery = "";
        Cursor curs = null;

        try {
            curs = db
                    .rawQuery("SELECT [TargetDate] FROM " + DBHelper.TBL_VOLIST + " Where OrgNo = " + orgno, null);
            if (curs.moveToFirst()) {
                selectQuery = curs.getString(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return selectQuery;
    }


    // Member list segregated by loan status, used only for loans, yet to loan
    // collections
    private String MembersSql(String vono, String ExtraCond) {

        String sql = "SELECT CM." + DBHelper.FLD_ORG_NO + ", CM."
                + DBHelper.FLD_ORG_MEM_NO + ", CM." + DBHelper.FLD_ORG_MEM_NAME
                + ", LN." + DBHelper.FLD_LOAN_NO + ", LN."
                + DBHelper.FLD_LOAN_STATUS + ", ST."
                + DBHelper.FLD_LOAN_STATUS_NAME + " FROM "
                + DBHelper.TBL_CMEMBERS + " CM INNER JOIN "
                + DBHelper.TBL_CLOANS + " LN ON CM." + DBHelper.FLD_ORG_NO
                + " = LN." + DBHelper.FLD_ORG_NO + " AND CM."
                + DBHelper.FLD_ORG_MEM_NO + " = LN." + DBHelper.FLD_ORG_MEM_NO
                + " INNER JOIN " + DBHelper.TBL_LOANSTATUS + " ST ON LN."
                + DBHelper.FLD_LOAN_STATUS + " = ST."
                + DBHelper.FLD_LOAN_STATUS;
        sql += " WHERE IfNull(LN." + DBHelper.FLD_LOAN_NO + ", 0) > 0";
        if (!(vono == null || vono.length() == 0)) {
            sql += " AND CM." + DBHelper.FLD_ORG_NO + " = '" + vono + "'";
        }
        if (!ExtraCond.isEmpty())
            sql += ExtraCond;
        // add order by
        sql += " ORDER BY LN." + DBHelper.FLD_LOAN_STATUS;
        return sql;

    }

    public ArrayList<HashMap<String, String>> getMemberList(String strSql,
                                                            String[] args) {
        ArrayList<HashMap<String, String>> itms = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
        Log.d(TAG, "From second getMemberList - " + strSql);
        try {
            curs = db.rawQuery(strSql, args);

            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                if (curs.getColumnCount() <= 3) {
                    do {
                        map = new HashMap<String, String>();
                        map.put(DBHelper.FLD_ORG_NO, curs.getString(0));
                        map.put(DBHelper.FLD_ORG_MEM_NO, curs.getString(1));
                        map.put(DBHelper.FLD_ORG_MEM_NAME, curs.getString(2));
                        map.put(DBHelper.FLD_LOAN_NO, "");
                        map.put(DBHelper.FLD_LOAN_STATUS_NAME, "");
                        itms.add(map);
                    } while (curs.moveToNext());
                } else {
                    int st = -100;
                    do {
                        if (st != curs.getInt(4)) {
                            // Header row
                            st = curs.getInt(4);
                            map = new HashMap<String, String>();
                            map.put(DBHelper.FLD_ORG_NO, "");
                            map.put(DBHelper.FLD_ORG_MEM_NO, "");
                            map.put(DBHelper.FLD_ORG_MEM_NAME, "");
                            map.put(DBHelper.FLD_LOAN_NO, "");
                            map.put(DBHelper.FLD_LOAN_STATUS_NAME,
                                    curs.getString(5));
                            itms.add(map);
                        }
                        map = new HashMap<String, String>();
                        map.put(DBHelper.FLD_ORG_NO, curs.getString(0));
                        map.put(DBHelper.FLD_ORG_MEM_NO, curs.getString(1));
                        map.put(DBHelper.FLD_ORG_MEM_NAME, curs.getString(2));
                        map.put(DBHelper.FLD_LOAN_NO, curs.getString(3));
                        map.put(DBHelper.FLD_LOAN_STATUS_NAME, "");
                        itms.add(map);
                    } while (curs.moveToNext());
                }

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return itms;
    }

    public Cursor getMemberList(final CharSequence constraint,
                                final String strSql, final String fixedWheres) {
        String whr = "";
        String sql = "";
        if (strSql == null || strSql.length() == 0) {
            sql = "SELECT " + DBHelper.TBL_CMEMBERS + "." + DBHelper.FLD_ORG_NO
                    + ", " + DBHelper.TBL_CMEMBERS + "."
                    + DBHelper.FLD_ORG_MEM_NO + ", " + DBHelper.TBL_CMEMBERS
                    + "." + DBHelper.FLD_ORG_MEM_NAME;
            ;
        } else
            sql = strSql;

        if (!(fixedWheres == null || fixedWheres.length() == 0))
            whr = "(" + fixedWheres + ")";

        String[] selargs = null;
        if (!(constraint == null || constraint.length() == 0)) {
            String value = "%" + constraint + "%";
            if (whr.length() > 0)
                whr += " AND (" + DBHelper.TBL_CMEMBERS + "."
                        + DBHelper.FLD_ORG_MEM_NO + " LIKE ? OR "
                        + DBHelper.TBL_CMEMBERS + "."
                        + DBHelper.FLD_ORG_MEM_NAME + " LIKE ?)";
            selargs = new String[]{value, value};
        }

        if (whr.length() > 0)
            whr = " WHERE " + whr;
        sql = sql + whr;
        return db.rawQuery(sql, selargs);
    }

    public ArrayList<CMember> getAllCMemebers() {
        CMember cm = null;
        Cursor curs = null;

        ArrayList<CMember> cMemList = new ArrayList<CMember>();

        try {
            curs = db.query(DBHelper.TBL_CMEMBERS, new String[]{
                            "[ProjectCode]", "[OrgNo]", "[OrgMemNo]", "[MemberName]",
                            "[SavBalan]", "[SavPayable]", "[CalcIntrAmt]",
                            "[TargetAmtSav]", "[ColcDate]", "[ReceAmt]", "[AdjAmt]",
                            "[SB]", "[SIA]", "[SavingsRealized]", "[memSexID]", "[NationalIDNo]",
                            "[PhoneNo]", "[WalletNo]", "[AdmissionDate]"}, null, null, null,
                    null, null);

            if (curs.moveToFirst()) {
                do {
                    cm = new CMember(curs.getString(0), curs.getString(1),
                            curs.getString(2), curs.getString(3),
                            curs.getInt(4), curs.getInt(5), curs.getDouble(6),
                            curs.getInt(7), P8.ConvertStringToDate(
                            curs.getString(8), "yyyy-MM-dd"),
                            curs.getInt(9), curs.getInt(10), curs.getInt(11),
                            curs.getInt(12), curs.getInt(13), curs.getInt(14),
                            curs.getString(15), curs.getString(16), curs.getString(17),
                            P8.ConvertStringToDate(curs.getString(18), "yyyy-MM-dd"));

                    cMemList.add(cm);
                } while (curs.moveToNext());

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return cMemList;
    }

    public CMember getCMember(String vono, String memno) {
        CMember cm = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_CMEMBERS, new String[]{
                            "[ProjectCode]", "[OrgNo]", "[OrgMemNo]", "[MemberName]",
                            "[SavBalan]", "[SavPayable]", "[CalcIntrAmt]",
                            "[TargetAmtSav]", "[ColcDate]", "[ReceAmt]", "[AdjAmt]",
                            "[SB]", "[SIA]", "[SavingsRealized]", "[memSexID]", "IfNull([NationalIDNo],'')",
                            "IfNull([PhoneNo],'')", "IfNull([WalletNo],'')", "IfNull([AdmissionDate], '2001-01-01 12:00:00')"},
                    "[OrgNo]=? AND [OrgMemNo]=?", new String[]{vono, memno},
                    null, null, null);
            if (curs.moveToFirst()) {
                cm = new CMember(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getString(3),
                        curs.getInt(4),
                        curs.getInt(5),
                        curs.getDouble(6),
                        curs.getInt(7),
                        P8.ConvertStringToDate(curs.getString(8), "yyyy-MM-dd"),
                        curs.getInt(9), curs.getInt(10), curs.getInt(11),
                        curs.getInt(12), curs.getInt(13), curs.getInt(14),
                        curs.getString(15), curs.getString(16), curs.getString(17),
                        P8.ConvertStringToDate(curs.getString(18), "yyyy-MM-dd"));

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return cm;
    }

    public ArrayList<HashMap<String, String>> getLoansByMember(String vono,
                                                               String memno, CMember cm) {
        ArrayList<HashMap<String, String>> itms = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
        HashMap<String, String> map;
        Integer intODB;

        Log.d(TAG, "VONo: " + vono + " MemNo.: " + memno);

        if (cm != null) {
            // Add savings record
            map = new HashMap<String, String>();
            map.put("[ProductSymbol]", "");
            map.put("[LoanNo]", "Savings");
            map.put("[TargetAmtLoan]", cm.get_TargetAmtSav().toString());
            map.put("[IAB]", cm.get_SIA().toString());
            map.put("[ReceAmt]", cm.get_ReceAmt().toString());
            map.put("[StName]", "");
            map.put("[ODText]", "");
            map.put("[ODAmt]", "");

            itms.add(map);

        }

        try {
            curs = db
                    .rawQuery(
                            "SELECT L.[LoanNo], L.[TargetAmtLoan], L.[IAB], L.[ReceAmt], L.[LnStatus], "
                                    + "S.[StName], L.[ODB], L.[ProductSymbol] FROM CLoans L INNER JOIN LoanStatus S ON L.[LnStatus]=S.[LnStatus] "
                                    + "WHERE L.[OrgNo]=? AND L.[OrgMemNo]=?",
                            new String[]{vono, memno});

            if (curs.moveToFirst()) {
                do {
                    map = new HashMap<String, String>();
                    map.put("[ProductSymbol]", curs.getString(7));
                    map.put("[LoanNo]", Integer.toString(curs.getInt(0)));
                    map.put("[TargetAmtLoan]", Integer.toString(curs.getInt(1)));
                    map.put("[IAB]", Integer.toString(curs.getInt(2)));
                    map.put("[ReceAmt]", Integer.toString(curs.getInt(3)));
                    map.put("[StName]", curs.getString(5));

                    intODB = curs.getInt(6);
                    if (intODB > 0) {
                        map.put("[ODText]", "Overdue");
                        map.put("[ODAmt]", intODB.toString());
                    }
                    /*
                     * else if(intODB < 0) { map.put("[ODText]", "Advance");
                     * intODB = -intODB; map.put("[ODAmt]", intODB.toString());
                     * }
                     */
                    else {
                        map.put("[ODText]", "");
                        map.put("[ODAmt]", "");
                    }

                    itms.add(map);

                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return itms;
    }

    public ArrayList<CLoan> getAllCLoansByMember(String vono, String memno) {
        ArrayList<CLoan> cls = new ArrayList<CLoan>();
        CLoan cl = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_CLOANS, new String[]{"[OrgNo]",
                            "[OrgMemNo]", "[ProjectCode]", "[LoanNo]", "[LoanSlNo]",
                            "[ProductNo]", "[IntrFactorLoan]", "[PrincipalAmt]",
                            "[SchmCode]", "[InstlAmtLoan]", "[DisbDate]", "[LnStatus]",
                            "[PrincipalDue]", "[InterestDue]", "[TotalDue]",
                            "[TargetAmtLoan]", "[TotalReld]", "[Overdue]",
                            "[BufferIntrAmt]", "[ReceAmt]", "[IAB]", "[ODB]", "[TRB]",
                            "[LB]", "[PDB]", "[IDB]", "[InstlPassed]",
                            "[OldInterestDue]", "[ProductSymbol]", "[LoanRealized]"},
                    "[OrgNo]=? AND [OrgMemNo]=?", new String[]{vono, memno},
                    null, null, null);
            if (curs.moveToFirst()) {
                do {
                    cl = new CLoan(curs.getString(0), curs.getString(1),
                            curs.getString(2), curs.getInt(3), curs.getInt(4),
                            curs.getString(5), curs.getDouble(6),
                            curs.getInt(7), curs.getString(8), curs.getInt(9),
                            P8.ConvertStringToDate(curs.getString(10),
                                    "yyyy-MM-dd"), curs.getInt(11),
                            curs.getInt(12), curs.getInt(13), curs.getInt(14),
                            curs.getInt(15), curs.getInt(16), curs.getInt(17),
                            curs.getDouble(18), curs.getInt(19),
                            curs.getInt(20), curs.getInt(21), curs.getInt(22),
                            curs.getInt(23), curs.getInt(24), curs.getInt(25),
                            curs.getInt(26), curs.getDouble(27),
                            curs.getString(28), curs.getInt(29));
                    cls.add(cl);
                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return cls;
    }

    public CLoan getCloan(Integer ln) {
        CLoan cl = null;
        Cursor curs = null;

        try {
            curs = db.query(DBHelper.TBL_CLOANS, new String[]{"[OrgNo]",
                            "[OrgMemNo]", "[ProjectCode]", "[LoanNo]", "[LoanSlNo]",
                            "[ProductNo]", "[IntrFactorLoan]", "[PrincipalAmt]",
                            "[SchmCode]", "[InstlAmtLoan]", "[DisbDate]", "[LnStatus]",
                            "[PrincipalDue]", "[InterestDue]", "[TotalDue]",
                            "[TargetAmtLoan]", "[TotalReld]", "[Overdue]",
                            "[BufferIntrAmt]", "[ReceAmt]", "[IAB]", "[ODB]", "[TRB]",
                            "[LB]", "[PDB]", "[IDB]", "[InstlPassed]",
                            "[OldInterestDue]", "[ProductSymbol]", "[LoanRealized]"},
                    "[LoanNo]=?", new String[]{ln.toString()}, null, null,
                    null);
            if (curs.moveToFirst()) {
                cl = new CLoan(
                        curs.getString(0),
                        curs.getString(1),
                        curs.getString(2),
                        curs.getInt(3),
                        curs.getInt(4),
                        curs.getString(5),
                        curs.getDouble(6),
                        curs.getInt(7),
                        curs.getString(8),
                        curs.getInt(9),
                        P8.ConvertStringToDate(curs.getString(10), "yyyy-MM-dd"),
                        curs.getInt(11), curs.getInt(12), curs.getInt(13), curs
                        .getInt(14), curs.getInt(15), curs.getInt(16),
                        curs.getInt(17), curs.getDouble(18), curs.getInt(19),
                        curs.getInt(20), curs.getInt(21), curs.getInt(22), curs
                        .getInt(23), curs.getInt(24), curs.getInt(25),
                        curs.getInt(26), curs.getDouble(27),
                        curs.getString(28), curs.getInt(29));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }
        return cl;
    }

    // Get Overdue list
    public ArrayList<HashMap<String, String>> getOverDueByVO(String vono) {
        ArrayList<HashMap<String, String>> memWiseLoanList = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;

        String selectQuery = "SELECT L.[OrgMemNo], M.[MemberName], L.[PrincipalAmt], L.[DisbDate], L.[ODB], L.[LnStatus], S.[StName] "
                + "FROM "
                + DBHelper.TBL_CLOANS
                + " L INNER JOIN "
                + DBHelper.TBL_CMEMBERS
                + " M ON "
                + "L.[OrgNo]=M.[OrgNo] AND L.[OrgMemNo]=M.[OrgMemNo] INNER JOIN "
                + DBHelper.TBL_LOANSTATUS
                + " S ON L.[LnStatus]=S.[LnStatus] "
                + "WHERE L.[ODB] > 0 AND L.[OrgNo]=? "
                + "ORDER BY L.[LnStatus], L.[OrgMemNo]";

        Log.d(TAG, "" + selectQuery);

        try {
            curs = db.rawQuery(selectQuery, new String[]{vono});

            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                int lns = -100;
                do {

                    if (lns != curs.getInt(5)) {
                        // New group header
                        lns = curs.getInt(5);
                        map = new HashMap<String, String>();
                        map.put("GPHead", curs.getString(6));
                        map.put("MemNo", "");
                        map.put("MemberName", "");
                        map.put("DisbAmt", "");
                        map.put("DisbDate", "");
                        map.put("Overdue", "");

                        memWiseLoanList.add(map);
                    }
                    map = new HashMap<String, String>();
                    map.put("GPHead", "");
                    map.put("MemNo", curs.getString(0));
                    map.put("MemberName", curs.getString(1));
                    map.put("DisbAmt", Integer.toString(curs.getInt(2)));
                    map.put("DisbDate", P8.FormatDate(P8.ConvertStringToDate(curs.getString(3)), "dd-MMM-yyyy"));
                    // map.put("DisbDate", curs.getString(3));
                    map.put("Overdue", Integer.toString(curs.getInt(4)));

                    memWiseLoanList.add(map);

                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return memWiseLoanList;
    }

    // Get Good loan list
    public ArrayList<HashMap<String, String>> getGoodLoanByVO(String vono) {
        ArrayList<HashMap<String, String>> goodLoanList = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;

        String selectQuery = "SELECT G.[OrgNo], G.[OrgMemNo], M.[MemberName], G.[MaxAmt], G.[Taken] "
                + "FROM "
                + DBHelper.TBL_GOOD_LOANS
                + " G INNER JOIN "
                + DBHelper.TBL_CMEMBERS
                + " M ON "
                + "G.[OrgNo]=M.[OrgNo] AND G.[OrgMemNo]=M.[OrgMemNo] "
                + "WHERE G.[OrgNo]=? ORDER BY G.[OrgMemNo]";

        Log.d(TAG, "" + selectQuery);

        try {
            curs = db.rawQuery(selectQuery, new String[]{vono});
            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                do {
                    map = new HashMap<String, String>();
                    // map.put(DBHelper.FLD_ID, curs.getString(1));
                    map.put("MemNo", curs.getString(1));
                    map.put("MemName", curs.getString(2));
                    map.put("MaxAmt", Integer.toString(curs.getInt(3)));
                    map.put("Taken", (curs.getString(4).equals("1") ? "Taken"
                            : "Not Taken"));

                    goodLoanList.add(map);

                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return goodLoanList;
    }

    public ArrayList<HashMap<String, String>> getVODisbursementReport(
            String vono) {
        ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
        String selectQuery = "SELECT " + DBHelper.FLD_DISBURSEMENT_DATE + ", "
                + DBHelper.FLD_PRINCIPAL_AMT + ", Count(*) as LDCount "
                + "FROM " + DBHelper.TBL_CLOANS + " " + "WHERE "
                + DBHelper.FLD_ORG_NO + " = ? " + "GROUP BY "
                + DBHelper.FLD_DISBURSEMENT_DATE + ", "
                + DBHelper.FLD_PRINCIPAL_AMT + " " + "ORDER BY "
                + DBHelper.FLD_DISBURSEMENT_DATE;

        Log.d(TAG, "" + selectQuery);


        try {
            curs = db.rawQuery(selectQuery, new String[]{vono});
            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                do {
                    map = new HashMap<String, String>();
                    // map.put(DBHelper.FLD_ID, curs.getString(1));

                    // map.put(DBHelper.FLD_DISBURSEMENT_DATE, (curs.getString(0)));
                    map.put(DBHelper.FLD_DISBURSEMENT_DATE, P8.FormatDate(P8.ConvertStringToDate(curs.getString(0)), "dd-MMM-yyyy"));

                    map.put(DBHelper.FLD_PRINCIPAL_AMT, curs.getString(1));
                    map.put("LDCount", Integer.toString(curs.getInt(2)));

                    items.add(map);

                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return items;
    }

    public ArrayList<HashMap<String, String>> getVODisbursementDetails(
            String vono, String ddate) {
        ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;

        String selectQuery = "SELECT L.[OrgMemNo], M.[MemberName], L.[LoanNo], [LoanSlNo], L.[PrincipalAmt], L.[DisbDate], L.[ODB], L.[LnStatus], S.[StName] "
                + "FROM "
                + DBHelper.TBL_CLOANS
                + " L INNER JOIN "
                + DBHelper.TBL_CMEMBERS
                + " M ON "
                + "L.[OrgNo]=M.[OrgNo] AND L.[OrgMemNo]=M.[OrgMemNo] INNER JOIN "
                + DBHelper.TBL_LOANSTATUS
                + " S ON L.[LnStatus]=S.[LnStatus] "
                + "WHERE L.[OrgNo]=? AND [DisbDate]=?";

        Log.d(TAG, "" + selectQuery);


        try {
            curs = db.rawQuery(selectQuery, new String[]{vono, ddate});
            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                do {
                    map = new HashMap<String, String>();
                    // map.put(DBHelper.FLD_ID, curs.getString(1));
                    map.put(DBHelper.FLD_ORG_MEM_NAME, curs.getString(0)
                            + " - " + curs.getString(1));
                    map.put(DBHelper.FLD_LOAN_NO,
                            Integer.toString(curs.getInt(2)));
                    map.put(DBHelper.FLD_LOAN_SL_NO,
                            Integer.toString(curs.getInt(3)));
                    map.put(DBHelper.FLD_PRINCIPAL_AMT, curs.getString(4));
                    map.put(DBHelper.FLD_DISBURSEMENT_DATE, P8.FormatDate(P8.ConvertStringToDate(curs.getString(5)), "dd-MMM-yyyy"));
                    //map.put(DBHelper.FLD_DISBURSEMENT_DATE, (curs.getString(5)));

                    map.put(DBHelper.FLD_OVERDUE_BAL, curs.getInt(6) > 0 ? ""
                            + curs.getInt(6) : "");
                    map.put(DBHelper.FLD_LOAN_STATUS, curs.getString(7));

                    items.add(map);

                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return items;
    }

    public ArrayList<HashMap<String, String>> getMemWiseLoanRep(Integer ln) {
        ArrayList<HashMap<String, String>> memWiseLoanList = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
        String selectQuery = "SELECT T." + DBHelper.FLD_COLC_DATE + ", " + "L."
                + DBHelper.FLD_TARGET_LOAN_AMT + ", T." + DBHelper.FLD_COLC_AMT
                + " " + "FROM " + DBHelper.TBL_TRANSACT + " T INNER JOIN "
                + DBHelper.TBL_CLOANS + " L " + "ON T." + DBHelper.FLD_LOAN_NO
                + "=" + "L." + DBHelper.FLD_LOAN_NO + " " + "WHERE T."
                + DBHelper.FLD_COLC_FOR + "='L' AND T." + DBHelper.FLD_LOAN_NO
                + "=" + ln.toString() + " " + "UNION " + "SELECT TT."
                + DBHelper.FLD_COLC_DATE + ", " + "LL."
                + DBHelper.FLD_TARGET_LOAN_AMT + ", TT."
                + DBHelper.FLD_TRAN_AMT + " " + "FROM "
                + DBHelper.TBL_TRANS_TRAIL + " TT INNER JOIN "
                + DBHelper.TBL_CLOANS + " LL " + "ON TT."
                + DBHelper.FLD_LOAN_NO + "=" + "LL." + DBHelper.FLD_LOAN_NO
                + " " + "WHERE TT." + DBHelper.FLD_COLC_FOR + "='L' AND TT."
                + DBHelper.FLD_LOAN_NO + "=" + ln.toString() + " "
                + "ORDER BY T." + DBHelper.FLD_COLC_DATE;

        Log.d(TAG, "" + selectQuery);


        try {
            curs = db.rawQuery(selectQuery, null);
            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                do {
                    map = new HashMap<String, String>();
                    //map.put(DBHelper.FLD_ID, curs.getString(1));
                    map.put(DBHelper.FLD_COLC_DATE, P8.FormatDate(
                            P8.ConvertStringToDate(curs.getString(0)),
                            "dd-MMM-yyyy"));
                    map.put(DBHelper.FLD_TARGET_LOAN_AMT, curs.getString(1));
                    map.put(DBHelper.FLD_COLC_AMT, curs.getString(2));

                    memWiseLoanList.add(map);
                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return memWiseLoanList;
    }

    public ArrayList<HashMap<String, String>> getMemWiseSavingsRep(String vo, String memno) {
        ArrayList<HashMap<String, String>> memWiseLoanList = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
        String selectQuery = "SELECT T." + DBHelper.FLD_COLC_DATE + ", " + "M."
                + DBHelper.FLD_TARGET_SAVINGS_AMT + ", T."
                + DBHelper.FLD_COLC_AMT + " FROM " + DBHelper.TBL_TRANSACT
                + " T INNER JOIN " + DBHelper.TBL_CMEMBERS + " M " + "ON T."
                + DBHelper.FLD_ORG_MEM_NO + "=" + "M."
                + DBHelper.FLD_ORG_MEM_NO + " AND " + "T."
                + DBHelper.FLD_ORG_NO + "=" + "M." + DBHelper.FLD_ORG_NO + " "
                + "WHERE T." + DBHelper.FLD_COLC_FOR + "='S' AND T."
                + DBHelper.FLD_ORG_NO + "='" + vo + "' AND T."
                + DBHelper.FLD_ORG_MEM_NO + "='" + memno + "' " + "UNION "
                + "SELECT TT." + DBHelper.FLD_COLC_DATE + ", " + "MM."
                + DBHelper.FLD_TARGET_SAVINGS_AMT + ", "
                + "CASE WHEN substr(TT." + DBHelper.FLD_TRANS_NO + ", 1, 2) IN ('SD', 'ST') THEN -TT." // new changes line
                + DBHelper.FLD_TRAN_AMT + " ELSE TT." + DBHelper.FLD_TRAN_AMT + " END FROM "
                + DBHelper.TBL_TRANS_TRAIL + " TT INNER JOIN "
                + DBHelper.TBL_CMEMBERS + " MM " + "ON TT."
                + DBHelper.FLD_ORG_MEM_NO + "=" + "MM."
                + DBHelper.FLD_ORG_MEM_NO + " AND " + "TT."
                + DBHelper.FLD_ORG_NO + "=" + "MM." + DBHelper.FLD_ORG_NO + " "
                + "WHERE TT." + DBHelper.FLD_COLC_FOR + "='S' AND TT."
                + DBHelper.FLD_ORG_NO + "='" + vo + "' AND TT."
                + DBHelper.FLD_ORG_MEM_NO + "='" + memno + "' " + "ORDER BY T."
                + DBHelper.FLD_COLC_DATE;

        try {
            curs = db.rawQuery(selectQuery, null);
            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                do {
                    map = new HashMap<String, String>();
                    // map.put(DBHelper.FLD_ID, curs.getString(1));
                    map.put(DBHelper.FLD_COLC_DATE, P8.FormatDate(
                            P8.ConvertStringToDate(curs.getString(0)),
                            "dd-MMM-yyyy"));
                    map.put(DBHelper.FLD_TARGET_SAVINGS_AMT, curs.getString(1));
                    map.put(DBHelper.FLD_COLC_AMT, curs.getString(2));

                    memWiseLoanList.add(map);
                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return memWiseLoanList;
    }


    // Loan realization and achievement report data

    public ArrayList<HashMap<String, String>> getLRTAReport() {
        ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
        String selectQuery = "SELECT L.[OrgNo], L.[LnStatus], S.[StName], Count(L.[TargetAmtLoan]) as TargetNum, "
                + "Sum(CASE WHEN L.[ReceAmt] >= L.[TargetAmtLoan] THEN 1 ELSE 0 END) AS AchieveNum, "
                + "Count(L.[TargetAmtLoan]) - Sum(CASE WHEN L.[ReceAmt] >= L.[TargetAmtLoan] THEN 1 ELSE 0 END) as DiffNum, "
                + "Sum(L.[TargetAmtLoan]) as TargetAmt, "
                + "Sum(CASE WHEN L.[ReceAmt] >= L.[TargetAmtLoan] THEN L.[ReceAmt] ELSE 0 END) as AchieveAmt, "
                + "Sum(L.[TargetAmtLoan]) - Sum(CASE WHEN L.[ReceAmt] >= L.[TargetAmtLoan] THEN L.[ReceAmt] ELSE 0 END) as DiffAmt "
                + "FROM "
                + DBHelper.TBL_CLOANS
                + " L INNER JOIN "
                + DBHelper.TBL_VOLIST
                + " V "
                + "ON L.[OrgNo]=V.[OrgNo] INNER JOIN "
                + DBHelper.TBL_LOANSTATUS
                + " S ON L.[LnStatus] = S.[LnStatus] "
                + "WHERE V.[TargetDate] Like '"
                + P8.FormatDate(P8.ToDay(), "yyyy-MM-dd")
                + "%' "
                + "GROUP BY L.[LnStatus], S.[StName], L.[OrgNo] "
                + "ORDER BY L.[LnStatus], L.[OrgNo]";

        Log.d(TAG, selectQuery);

        try {
            curs = db.rawQuery(selectQuery, null);
            HashMap<String, String> map;
            if (curs.moveToFirst()) {
                int lns = -100;

                int intTNum = 0, intANum = 0, intDNum = 0;
                int intTAmt = 0, intAAmt = 0, intDAmt = 0;

                do {
                    if (lns != curs.getInt(1)) {
                        // New group header
                        lns = curs.getInt(1);
                        map = new HashMap<String, String>();
                        map.put("GPHead", curs.getString(2));
                        map.put("OrgNo", "");
                        map.put("TargetNum", "");
                        map.put("AchieveNum", "");
                        map.put("DiffNum", "");
                        map.put("TargetAmt", "");
                        map.put("AchieveAmt", "");
                        map.put("DiffAmt", "");

                        items.add(map);
                    }
                    map = new HashMap<String, String>();
                    // map.put(DBHelper.FLD_ID, curs.getString(1));
                    map.put("GPHead", "");
                    map.put("OrgNo", curs.getString(0));
                    map.put("TargetNum", Integer.toString(curs.getInt(3)));
                    map.put("AchieveNum", Integer.toString(curs.getInt(4)));
                    map.put("DiffNum", Integer.toString(curs.getInt(5)));
                    map.put("TargetAmt", Integer.toString(curs.getInt(6)));
                    map.put("AchieveAmt", Integer.toString(curs.getInt(7)));
                    map.put("DiffAmt", Integer.toString(curs.getInt(8)));

                    items.add(map);

                    // totals
                    intTNum = intTNum + curs.getInt(3);
                    intANum = intANum + curs.getInt(4);
                    intDNum = intDNum + curs.getInt(5);
                    intTAmt = intTAmt + curs.getInt(6);
                    intAAmt = intAAmt + curs.getInt(7);
                    intDAmt = intDAmt + curs.getInt(8);

                } while (curs.moveToNext());

                // Add total row
                map = new HashMap<String, String>();
                map.put("GPHead", "");
                map.put("OrgNo", "Total");
                map.put("TargetNum", Integer.toString(intTNum));
                map.put("AchieveNum", Integer.toString(intANum));
                map.put("DiffNum", Integer.toString(intDNum));
                map.put("TargetAmt", Integer.toString(intTAmt));
                map.put("AchieveAmt", Integer.toString(intAAmt));
                map.put("DiffAmt", Integer.toString(intDAmt));

                items.add(map);

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return items;

    }

    // Get WP colleciton total
    public int getWPCollectionTotal() {
        Integer ret = 0;
        Cursor curs = null;

        try {
            curs = db.rawQuery("SELECT Sum([ColcAmt]) AS amt FROM "
                    + DBHelper.TBL_WPTRANSACT + " WHERE [Verified]!='1'", null);

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                ret = curs.getInt(0);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return ret;
    }

    // Get all without passbook transaction with member list
    public ArrayList<HashMap<String, String>> getWPPendingTransWithMember() {
        ArrayList<HashMap<String, String>> itms = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;
        HashMap<String, String> map;

        try {
            curs = db
                    .rawQuery(
                            "SELECT T.[OrgNo], T.[OrgMemNo], C.[MemberName], T.[LoanNo], T.[ColcAmt], T.[ColcDate], "
                                    + "T.[ColcFor], T.[_id] FROM "
                                    + DBHelper.TBL_WPTRANSACT
                                    + " T INNER JOIN "
                                    + DBHelper.TBL_CMEMBERS
                                    + " C ON T.[OrgNo]=C.[OrgNo] "
                                    + "AND T.[OrgMemNo]= C.[OrgMemNo] WHERE T.[Verified]!='1'",
                            null); // WHERE T.[Verified]='0'
            // Log.d(TAG, "Record count: " + curs.getCount());
            if (curs.moveToFirst()) {
                do {
                    Log.d(TAG,
                            "id: " + curs.getInt(7) + " ORGNO: "
                                    + curs.getString(0) + " MEMNO: "
                                    + curs.getString(1));
                    map = new HashMap<String, String>();
                    map.put("[OrgNo]", curs.getString(0));
                    map.put("[OrgMemNo]", curs.getString(1));
                    map.put("[MemberName]", curs.getString(2));

                    if (curs.getString(6).equals("S"))
                        map.put("[LoanNo]", "Savings");
                    else
                        map.put("[LoanNo]", Integer.toString(curs.getInt(3)));

                    map.put("[ColcAmt]", Integer.toString(curs.getInt(4)));
                    map.put("[ColcDate]", P8.FormatDate(
                            P8.ConvertStringToDate(curs.getString(5)),
                            "dd-MMM-yyyy"));
                    map.put("[_id]", Integer.toString(curs.getInt(7)));

                    itms.add(map);

                } while (curs.moveToNext());
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }
        return itms;
    }

    public void deleteWPTransactions(Integer _id) throws Exception {
        try {
            db.beginTransaction();
            db.delete(DBHelper.TBL_WPTRANSACT, "[_id]=?",
                    new String[]{_id.toString()});
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, e.getMessage(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    // Verified without passbook transactions update
    // Before calling this function, caller must call
    // CMember.set_lastCM(_lastCM) and/or
    // CLoan.set_lastCL(_lastCL) functions.
    public void addVerifiedWPTransactions(Integer _id) throws Exception {
        // Retrieve record from table
        Cursor curs = null;
        String TType = "";
        Integer mAmount = 0;

        Log.d(TAG, "ID: " + _id.toString());

        try {
            db.beginTransaction();
            curs = db
                    .rawQuery(
                            "SELECT [OrgNo], [OrgMemNo], [LoanNo], [ColcAmt], [ColcDate], "
                                    + "[ColcFor] FROM "
                                    + DBHelper.TBL_WPTRANSACT
                                    + " WHERE [_id]=?",
                            new String[]{_id.toString()});
            if (curs.moveToFirst()) {

                mAmount = curs.getInt(3);
                TType = curs.getString(5);
                Log.d(TAG,
                        "Org: " + curs.getString(0) + "\nMem: "
                                + curs.getString(1) + "\nType: " + TType
                                + "\nAmount: " + mAmount.toString());
                curs.close();
                curs = null;

                if (mAmount != 0) {
                    if (TType.equals("S"))
                        addTransaction(CMember.get_lastCM(), TType, mAmount);
                    else if (TType.equals("L"))
                        addTransaction(CLoan.get_lastCL(), TType, mAmount);

                    // Update wptransaction table
                    ContentValues cv = new ContentValues();
                    cv.clear();
                    cv.put("[Verified]", "1");

                    db.update(DBHelper.TBL_WPTRANSACT, cv, "[_id]=?",
                            new String[]{_id.toString()});
                    db.setTransactionSuccessful();
                }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.getMessage(), e);
            throw e;

        } finally {
            if (curs != null)
                curs.close();
            db.endTransaction();
        }

    }

    // Add without passbook transactions to temporary wp table
    public void addWPTransaction(CLoan cl, String TType, Integer amt)
            throws Exception {

        Log.d(TAG, "In addWPTransaction Type " + TType);
        try {
            db.beginTransaction();
            InsertTrans(cl.get_OrgNo(), cl.get_OrgMemNo(),
                    cl.get_ProjectCode(), cl.get_LoanNo(), amt, TType,
                    DBHelper.TBL_WPTRANSACT);
            db.setTransactionSuccessful();

        } catch (Exception e) {
            // TODO: handle exception
            Log.e(TAG, e.getMessage(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    public void addWPTransaction(CMember cm, String TType, Integer amt)
            throws Exception {

        Log.d(TAG, "In addWPTransaction Type " + TType);
        try {
            db.beginTransaction();

            InsertTrans(cm.get_OrgNo(), cm.get_OrgMemNo(),
                    cm.get_ProjectCode(), null, amt, TType,
                    DBHelper.TBL_WPTRANSACT);

            db.setTransactionSuccessful();

        } catch (Exception e) {
            // TODO: handle exception
            Log.e(TAG, e.getMessage(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    public void addTransaction(CLoan cl, String TType, Integer amt)
            throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();

            Log.d(TAG,
                    "Loan(" + TType + ") VONo: " + cl.get_OrgNo() + " MemNo: "
                            + cl.get_OrgMemNo() + " Amount: " + amt.toString());

//			InsertTrans(cl.get_OrgNo(), cl.get_OrgMemNo(),
//					cl.get_ProjectCode(), cl.get_LoanNo(), amt, TType,
//					DBHelper.TBL_TRANSACT);

            cl.Repay(amt);

            InsertTrans(cl.get_OrgNo(), cl.get_OrgMemNo(),
                    cl.get_ProjectCode(), cl.get_LoanNo(), amt, TType, cl.get_ProductSymbol(), cl.get_TRB(), 0,
                    DBHelper.TBL_TRANSACT);

            cv.clear();
            cv.put("[ReceAmt]", cl.get_ReceAmt());
            cv.put("[IAB]", cl.get_IAB());
            cv.put("[ODB]", cl.get_ODB());
            cv.put("[TRB]", cl.get_TRB());
            cv.put("[LB]", cl.get_LB());
            cv.put("[PDB]", cl.get_PDB());
            cv.put("[IDB]", cl.get_IDB());

            db.update(DBHelper.TBL_CLOANS, cv,
                    "[OrgNo]=? AND [OrgMemNo]=? AND [LoanNo]=?", new String[]{
                            cl.get_OrgNo(), cl.get_OrgMemNo(),
                            cl.get_LoanNo().toString()});

            PO po = getPO();
            UpdatePOCash(po.get_CashInHand() + amt);

            db.setTransactionSuccessful();

        } catch (Exception e) {
            // TODO: handle exception
            Log.e(TAG, e.getMessage(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    public void addTransaction(CMember cm, String TType, Integer amt)
            throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();

//			Log.d(TAG,
//					"Savings(" + TType + ") VONo: " + cm.get_OrgNo()
//							+ " MemNo: " + cm.get_OrgMemNo() + " Amount: "
//							+ amt.toString());

//			InsertTrans(cm.get_OrgNo(), cm.get_OrgMemNo(),
//					cm.get_ProjectCode(), null, amt, TType,
//					DBHelper.TBL_TRANSACT);

            cm.Save(amt);

            InsertTrans(cm.get_OrgNo(), cm.get_OrgMemNo(),
                    cm.get_ProjectCode(), null, amt, TType, "", cm.get_SB(), 0,
                    DBHelper.TBL_TRANSACT);

            cv.clear();
            cv.put("[ReceAmt]", cm.get_ReceAmt());
            cv.put("[SB]", cm.get_SB());
            cv.put("[SIA]", cm.get_SIA());

            db.update(DBHelper.TBL_CMEMBERS, cv, "[OrgNo]=? AND [OrgMemNo]=?",
                    new String[]{cm.get_OrgNo(), cm.get_OrgMemNo()});

            PO po = getPO();
            UpdatePOCash(po.get_CashInHand() + amt);

            db.setTransactionSuccessful();

        } catch (Exception e) {
            // TODO: handle exception
            Log.e(TAG, e.getMessage(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    private void UpdatePOCash(Integer amt) {
        ContentValues cv = new ContentValues();

        cv.clear();
        cv.put("[CashInHand]", amt);
        db.update(DBHelper.TBL_PO, cv, null, null);
    }

    private void InsertTrans(String OrgNo, String OrgMemNo, String ProjectCode,
                             Integer LoanNo, Integer amt, String ColcFor, String psym, Integer bal, Integer smsstat, String tblName) {
        ContentValues cv = new ContentValues();

        PO po = getPO();
        int sl = po.get_SL() + 1;

        if (tblName.equalsIgnoreCase(DBHelper.TBL_TRANSACT))

            cv.put("[TransSlNo]", sl);
        cv.put("[ColcID]", 0);
        cv.put("[OrgNo]", OrgNo);
        cv.put("[OrgMemNo]", OrgMemNo);
        cv.put("[ProjectCode]", ProjectCode);
        cv.put("[LoanNo]", LoanNo);
        cv.put("[ColcAmt]", amt);
        cv.put("[ColcFor]", ColcFor);
        cv.put("[ProductSymbol]", psym);
        cv.put("[Balance]", bal);
        cv.put("[SMSStatus]", smsstat);

        db.insert(tblName, null, cv);
        // db.insert(DBHelper.TBL_TRANSACT, null, cv);
        UpdateTranSL(sl);
    }

    private void InsertTrans(String OrgNo, String OrgMemNo, String ProjectCode,
                             Integer LoanNo, Integer amt, String ColcFor, String tblName) {
        ContentValues cv = new ContentValues();

        if (tblName.equalsIgnoreCase(DBHelper.TBL_TRANSACT))
            cv.put("[ColcID]", 0);
        cv.put("[OrgNo]", OrgNo);
        cv.put("[OrgMemNo]", OrgMemNo);
        cv.put("[ProjectCode]", ProjectCode);
        cv.put("[LoanNo]", LoanNo);
        cv.put("[ColcAmt]", amt);
        cv.put("[ColcFor]", ColcFor);
        db.insert(tblName, null, cv);
        // db.insert(DBHelper.TBL_TRANSACT, null, cv);
    }

    public void AssignCollector(String collno, String POName, String branchCode,
                                String branchName, String branchOpenDate, String SysDate, String projectCode, int sl) throws Exception {

        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();
            cv.put("TransSlNo", sl);
            cv.put("CONo", collno);
            cv.put("COName", POName);
            cv.put("EnteredBy", collno);
            cv.put("AltCOName", POName);
            cv.put("EMethod", 0);
            cv.put("CashInHand", 0);
            cv.put("SessionNo", 0);
            cv.put("Password", "");
            cv.put("BranchCode", branchCode);
            cv.put("BranchName", branchName);
            cv.put("BODate", branchOpenDate);
            cv.put("SYSDATE", SysDate);
            cv.put("ProjectCode", projectCode);
            cv.put("LastSyncTime", "2015-01-01 12:00:00");

            db.update(DBHelper.TBL_PO, cv, null, null);
            db.delete(DBHelper.TBL_VOLIST, null, null);
            db.delete(DBHelper.TBL_LOAN_PRODUCTS, null, null);
            db.delete(DBHelper.TBL_CMEMBERS, null, null);
            db.delete(DBHelper.TBL_CLOANS, null, null);
            db.delete(DBHelper.TBL_TRANSACT, null, null);
            db.setTransactionSuccessful();

        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);
            throw e;

        } catch (Exception e) {
            Log.e(TAG, e.toString(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    public void UpdateSessionData(String fileName) throws SQLException,
            IOException {
        try {
            db.beginTransaction();
            executeSQLScript(fileName);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    public void ClearTransactions() throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();
            cv.put("CashInHand", 0);

            db.update(DBHelper.TBL_PO, cv, null, null);
            db.delete(DBHelper.TBL_TRANSACT, null, null);
            cv.clear();
            cv.put("[SentAt]", P8.getDateTime());
            db.update(DBHelper.TBL_SURVEYSL, cv, "([SentAt] IS NULL)", null);
            db.setTransactionSuccessful();

        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);
            throw e;

        } catch (Exception e) {
            Log.e(TAG, e.toString(), e);
            throw e;

        } finally {
            db.endTransaction();
        }
    }

    public ArrayList<HashMap<String, String>> getSMSRepor() {
        ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
        Cursor curs = null;

        String sql = "";

        // Reading all values
        //	Log.d("Generating: ", "Collection items for listview ...");

        try {
            sql = "SELECT T.[OrgNo], T.[OrgMemNo], M.[MemberName], IfNull(M.[PhoneNo],''), T.[ColcAmt], T.[ColcFor], IfNull(T.[ProductSymbol],''), IfNull(T.[Balance],0), IfNull(T.[SMSStatus],0) "
                    + "FROM [Transact] as T INNER JOIN [CMembers] as M "
                    + "ON T.[OrgNo]=M.[OrgNo] AND T.[OrgMemNo] = M.[OrgMemNo] "
                    + "ORDER BY T.[OrgNo], T.[OrgMemNo]";

            Log.d(TAG, sql);

            curs = db.rawQuery(sql, null);

            HashMap<String, String> map;
            if (curs.moveToFirst()) {

                do {
                    map = new HashMap<String, String>();

                    map.put(DBHelper.FLD_ORG_NO, curs.getString(0));
                    map.put(DBHelper.FLD_ORG_MEM_NO, curs.getString(1));
                    map.put(DBHelper.FLD_ORG_MEM_NAME, curs.getString(2));
                    map.put(DBHelper.FLD_ORG_MEM_PHONE, curs.getString(3));
                    map.put(DBHelper.FLD_COLC_AMT, Integer.toString(curs.getInt(4)));
                    if (curs.getString(5).equals("S"))
                        map.put(DBHelper.FLD_COLC_FOR, "Savings");
                    else if (curs.getString(6).equals("DGD"))
                        map.put(DBHelper.FLD_COLC_FOR, "Good Loan");
                    else
                        map.put(DBHelper.FLD_COLC_FOR, "General Loan");
                    if (curs.getInt(8) == 1)
                        map.put(DBHelper.FLD_SMS_SENT_STATUS, "Sent");
                    else if (curs.getString(3).trim().length() > 0)
                        map.put(DBHelper.FLD_SMS_SENT_STATUS, "Pending");

                    items.add(map);

                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
        }

        return items;
    }

    public ArrayList<Transact> getTransactions(String vono, String memno, Integer smsstatus) {
        Transact ct = null;
        Cursor curs = null;

        ArrayList<Transact> ret = new ArrayList<Transact>();

        try {
            curs = db.query(DBHelper.TBL_TRANSACT, new String[]{
                            "_id", "[ColcID]", "[OrgNo]", "[OrgMemNo]", "[ProjectCode]",
                            "[LoanNo]", "[ColcAmt]", "[SavAdj]", "[ColcDate]", "[ColcFor]",
                            "IfNull([ProductSymbol],'')", "[Balance]", "[SMSStatus]"},
                    "[OrgNo]=? AND [OrgMemNo]=? AND [SMSStatus]=?", new String[]{vono, memno, smsstatus.toString()},
                    null, null, null, null);

            if (curs.moveToFirst()) {
                do {
                    ct = new Transact(curs.getInt(0), curs.getInt(1),
                            curs.getString(2), curs.getString(3), curs.getString(4),
                            curs.getInt(5), curs.getInt(6), curs.getInt(7),
                            P8.ConvertStringToDate(curs.getString(8), "yyyy-MM-dd"),
                            curs.getString(9), curs.getString(10), curs.getInt(11),
                            curs.getInt(12));

                    ret.add(ct);
                } while (curs.moveToNext());

            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());
        } finally {
            if (curs != null)
                curs.close();
        }

        return ret;
    }

    public void ConfirmSMSSent(String recs) {
        ContentValues cv = new ContentValues();

        try {
            cv.put("[SMSStatus]", 0); //Previously it was 1
            db.update(DBHelper.TBL_TRANSACT, cv, "_id IN (" + recs + ")", null);

        } catch (SQLException e) {
            Log.e(TAG, e.toString(), e);

        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public HashMap<String, String[]> GetTransForStmByMember(String vono, String memno) {
        HashMap<String, String[]> itms = new HashMap<String, String[]>();
        Cursor curs = null;
        String sHd[] = {"", "", "", ""};
        String sBal[] = {"", "0", "0", "0"};
        String sKey = "";
        Date dDate;

        int i = 1;

        try {
            // Find active loan first
            curs = db
                    .rawQuery(
                            "SELECT [LoanNo], [TRB], [ProductSymbol] FROM CLoans WHERE [OrgNo]=? AND [OrgMemNo]=?",
                            new String[]{vono, memno});
            if (curs.moveToFirst()) {
                do {
//					Log.d(TAG, "Loan# " + i + " = " + curs.getInt(0) + " TRB: " + curs.getInt(1));
                    if (i < 3) {
                        sHd[i] = Integer.toString(curs.getInt(0));
                        sBal[i] = Integer.toString(curs.getInt(1));
                    }
                    i++;
                } while (curs.moveToNext());
            }

            curs.close();

            curs = db
                    .rawQuery(
                            "SELECT [MemberName], [SB] FROM CMembers WHERE [OrgNo]=? AND [OrgMemNo]=?",
                            new String[]{vono, memno});
            if (curs.moveToFirst()) {
                sHd[3] = "Savings";
                sBal[3] = Integer.toString(curs.getInt(1));
            }

            curs.close();

            // Put header and summary value in array list
            itms.put("H", sHd);
            itms.put("B", sBal);

            String sdc = "date('now','start of month','-1 year')";
            if (sHd[0].length() == 0 && sHd[1].length() == 0)
                sdc = "date('now','start of month','-3 months')";

            curs = db
                    .rawQuery(
                            "SELECT [LoanNo], [Tranamount], [ColcDate], [ColcFor], [Transno] FROM "
                                    + DBHelper.TBL_TRANS_TRAIL
                                    + " WHERE Date([ColcDate]) >= "
                                    + sdc
                                    + " AND [OrgNo]=? AND [OrgMemNo]=?"
                                    + " UNION "
                                    + "SELECT [LoanNo],[ColcAmt], [ColcDate], [ColcFor], ''  FROM "
                                    + DBHelper.TBL_TRANSACT
                                    + " WHERE [OrgNo]=? AND [OrgMemNo]=?"
                                    + " ORDER BY [ColcDate]", new String[]{
                                    vono, memno, vono, memno});

            if (curs.moveToFirst()) {
                String[] vl;
                int ln;
                do {
                    dDate = P8.ConvertStringToDate(curs.getString(2));
                    sKey = curs.getString(2).substring(0, 10) + "0";

//					Log.d(TAG, "Date: " + sKey + " Loan# " + curs.getInt(0) + " Value: " + curs.getInt(1) + " For: " + curs.getString(3) + " Tran#: " + curs.getString(4) );
                    i = -1;
                    if (curs.getString(3).equals("S"))
                        i = 3;
                    else if (curs.getString(3).equals("L")) {
                        // Loan, check loan numbers
                        ln = curs.getInt(0);
                        if (sHd[1].equals("" + ln))
                            i = 1;
                        else if (sHd[2].equals("" + ln))
                            i = 2;
                    }
                    if (i > 0 && i <= 3) {
                        // Only those records have matching loan# and savings
                        do {
                            if (itms.containsKey(sKey))
                                vl = itms.get(sKey);
                            else
                                vl = new String[]{
                                        P8.FormatDate(dDate, "dd-MM-yy"), "",
                                        "", ""};

                            if (vl[i].length() == 0) {
                                // Ok, to proceed
                                int value = curs.getInt(1);
                                if (curs.getString(3).equals("S")) {
                                    // Need to check whether transaction is a
                                    // withdrawal
                                    String sType = curs.getString(4).substring(
                                            0, 2);
                                    if (sType.endsWith("SD")
                                            || sType.endsWith("ST"))
                                        value = -value;
                                }
                                vl[i] = Integer.toString(value);
                                itms.put(sKey, vl);
                                break;
                            } else {
                                // Multiple records in same day
                                sKey = sKey.substring(0, 10)
                                        + (Integer.parseInt(sKey.substring(10)) + 1);
                            }
                        } while (true);
                    }
                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            Log.d(TAG, e.getMessage());

        } finally {
            if (curs != null)
                curs.close();
        }

        return itms;
    }

    @SuppressLint("DefaultLocale")
    public String GetTransForStatement(String vono, String memno) {
        StringBuilder sb = new StringBuilder("");
        Cursor curs = null;

        try {
            curs = db
                    .rawQuery(
                            "SELECT [LoanNo], [Tranamount], [ColcDate], [ColcFor] FROM "
                                    + DBHelper.TBL_TRANS_TRAIL
                                    + " WHERE [OrgNo]=? AND [OrgMemNo]=?"
                                    + " UNION "
                                    + "SELECT [LoanNo],[ColcAmt], [ColcDate], [ColcFor] FROM "
                                    + DBHelper.TBL_TRANSACT
                                    + " WHERE [OrgNo]=? AND [OrgMemNo]=?"
                                    + " ORDER BY [ColcDate]", new String[]{
                                    vono, memno, vono, memno});

            if (curs.moveToFirst()) {
                do {
                    String s = String.format(
                            "%-9s %-9s %10s %10s\n",
                            P8.FormatDate(P8.ConvertStringToDate(
                                    curs.getString(2), "yyyy-MM-dd"),
                                    "dd.MM-yy"),
                            curs.getString(3).equals("S") ? "Savings" : Integer
                                    .toString(curs.getInt(0)),
                            curs.getString(3).equals("S") ? " " : Integer
                                    .toString(curs.getInt(1)),
                            curs.getString(3).equals("S") ? Integer
                                    .toString(curs.getInt(1)) : " ");
                    // Log.d(TAG, s);
                    sb.append(s);

                } while (curs.moveToNext());
                if (sb.length() > 0)
                    sb.append("------------------------------------------\n");
            }
            // sb.append("\n");
            curs.close();
            curs = db.rawQuery(
                    "SELECT Sum(TRB) as TotR FROM " + DBHelper.TBL_CLOANS
                            + " WHERE [OrgNo]=? AND [OrgMemNo]=?",
                    new String[]{vono, memno});
            if (curs.moveToFirst()) {
                sb.append(String.format("Total loan realized: %12d\n",
                        curs.getInt(0)));
            }
            curs.close();
            curs = db.rawQuery("SELECT Sum(SB) as TSB FROM "
                    + DBHelper.TBL_CMEMBERS
                    + " WHERE [OrgNo]=? AND [OrgMemNo]=?", new String[]{vono,
                    memno});
            if (curs.moveToFirst()) {
                sb.append(String.format("Total Savings Balance: %10d\n",
                        curs.getInt(0)));
            }

        } catch (Exception e) {
            Log.d(TAG, e.getMessage());

        } finally {
            if (curs != null)
                curs.close();
        }

        return sb.toString();
    }

    public boolean HasTransactions() {
        int cnt = getRecordCount("SELECT Count(*) as cnt FROM "
                + DBHelper.TBL_TRANSACT, null);
        if (cnt == 0) {
            cnt = getRecordCount("SELECT Count(*) as cnt FROM "
                    + DBHelper.TBL_SURVEYSL + " WHERE [SentAt] is null", null);
        }
        return (cnt > 0);
    }

    public JSONObject PrepareDataForSending() throws IOException {

        JSONArray ja = new JSONArray();
        ja.put(ExportTableAsJSon("Transact", "SELECT T.TransSlNo, T.OrgNo, T.OrgMemNo, T.ProjectCode, T.LoanNo, T.ColcAmt, T.SavAdj, "
                        + "Date(T.ColcDate), T.ColcFor, T.ColcDate as ModifyTime, C.CONo, C.EnteredBy, C.SessionNo, "
                        + "T.ProductSymbol, T.Balance, T.SMSStatus "
                        + "FROM Transact T, POList C",
                new String[]{"S", "S", "S", "S", "I", "I", "I", "S", "S", "S",
                        "S", "S", "I", "S", "I", "I"},
                new String[]{"TransSlNo", "OrgNo", "OrgMemNo", "ProjectCode", "LoanNo", "ColcAmt", "SavAdj", "ColcDate",
                        "ColcFor", "ModifyTime", "CONo", "EnteredBy",
                        "SessionNo", "ProductSymbol", "Balance", "SMSStatus"}));

        //bos.write("\n-- Seasonal Loan Survey Data".getBytes());
        ja.put(ExportTableAsJSon("SurveySL", "SELECT Date(SDate), OrgNo, OrgMemNo, Age, Education, Home, " +
                        "OI_No, OI_DL, OI_OB, OI_PB, OI_OTHERS, OI_ISOURCE, CI_NO, CI_0_5, " +
                        "CI_5_10, CI_10_PLUS, CI_TOTAL, MA_CC, MA_OC, MA_CR, PL_O_O, PL_O_P, " +
                        "PL_L_O, PL_L_P, OA_LAND, OA_TV, OA_MC, OA_FR, PI_NGO, PI_BANK, PI_REL, " +
                        "PI_WS, PI_OS, Complete, DateTime([UpdateAt]) FROM SurveySL " +
                        "WHERE [SentAt] is null",
                new String[]{"S", "S", "S", "I", "I", "I",
                        "I", "I", "I", "I", "I", "I", "I", "I",
                        "I", "I", "I", "I", "I", "I", "I", "I",
                        "I", "I", "I", "I", "I", "I", "I", "I", "I",
                        "I", "I", "I", "S"},
                new String[]{"SDate", "OrgNo", "OrgMemNo", "Age", "Education", "Home",
                        "OI_No", "OI_DL", "OI_OB", "OI_PB", "OI_OTHERS", "OI_ISOURCE", "CI_NO", "CI_0_5",
                        "CI_5_10", "CI_10_PLUS", "CI_TOTAL", "MA_CC", "MA_OC", "MA_CR", "PL_O_O", "PL_O_P",
                        "PL_L_O", "PL_L_P", "OA_LAND", "OA_TV", "OA_MC", "OA_FR", "PI_NGO", "PI_BANK", "PI_REL",
                        "PI_WS", "PI_OS", "Complete", "UpdateAt"}));
//        bos.close();
//        return strExportFile;

        JSONObject jo = new JSONObject();

        try {
            jo.put("status", "sucess");
            jo.put("data", ja);


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jo;
    }

    public JSONObject ExportTableAsJSon(String tName, String selSql, String[] colTyes, String[] fldNames) throws IOException {
        Cursor cur = null;
        JSONObject objRet = new JSONObject();
        JSONArray jsonArray = new JSONArray();

        int numcols = fldNames.length;  // cur.getColumnCount();

        try {
            cur = db.rawQuery(selSql, null);
            Log.d(TAG, "Selection SQL: " + selSql + "\nRcords found: " + Integer.toString(cur.getCount()));

            cur.moveToFirst();

            while (cur.getPosition() < cur.getCount()) {
                JSONObject obj = new JSONObject();

                for (int idx = 0; idx < numcols; idx++) {
                    if (colTyes[idx].equals("S")) {
                        obj.put(fldNames[idx], cur.getString(idx));
                    } else {
                        obj.put(fldNames[idx], Integer.toString(cur.getInt(idx)));
                    }
                }
                jsonArray.put(obj);
                cur.moveToNext();
            }
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage());
        } finally {
            if (cur != null)
                cur.close();
        }

        try {
            objRet.put("table", tName);
            objRet.put("data", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return objRet;
    }

    public void SetStatusEntryLocked() throws Exception {
        ContentValues cv = new ContentValues();

        try {
            db.beginTransaction();
            cv.put("EMETHOD", 0);
            db.update(DBHelper.TBL_PO, cv, null, null);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, e.toString(), e);
            throw e;
        } finally {
            db.endTransaction();
        }
    }

    public void executeSQLScript(String dbname) throws IOException,
            SQLException {
        FileInputStream is;
        BufferedReader reader;
        StringBuilder sb = new StringBuilder();
        String stm;
        Integer i;

        is = new FileInputStream(dbname);
        reader = new BufferedReader(new InputStreamReader(is));
        try {
            String line = reader.readLine();
            while (line != null) {
                // Log.d(TAG, line);
                stm = line.trim();
                if (stm.length() > 0) {
                    if (!stm.startsWith("--")) {
                        sb.append(stm);
                        i = sb.indexOf(";");
                        if (i != -1) {
                            // Log.d(TAG, sb.substring(0, i+1));
                            db.execSQL(sb.substring(0, i + 1));
                            sb.delete(0, i + 1);
                        }
                    }
                }
                line = reader.readLine();
            }
        } catch (IOException e) {
            // TODO Handle Script Failed to Load
            Log.e(TAG, e.toString(), e);
            throw e;
        } catch (SQLException e) {
            // TODO Handle Script Failed to Execute
            Log.e(TAG, e.toString(), e);
            throw e;
        } finally {
            reader.close();
            is.close();
        }
    }

    /*
     * public void executeSQLScript(String dbname) throws IOException,
     * SQLException { ByteArrayOutputStream outputStream = new
     * ByteArrayOutputStream(); byte buf[] = new byte[1024]; int len;
     *
     * InputStream inputStream = null;
     *
     * try { inputStream = new FileInputStream(dbname); while ((len =
     * inputStream.read(buf)) != -1) { outputStream.write(buf, 0, len); }
     * outputStream.close(); inputStream.close();
     *
     * String[] createScript = outputStream.toString().split(";"); for (int i =
     * 0; i < createScript.length; i++) { String sqlStatement =
     * createScript[i].trim(); // TODO You may want to parse out comments here
     * if (sqlStatement.length() > 0) { db.execSQL(sqlStatement + ";"); } } }
     * catch (IOException e) { // TODO Handle Script Failed to Load Log.e(TAG,
     * e.toString(), e); throw e; } catch (SQLException e) { // TODO Handle
     * Script Failed to Execute Log.e(TAG, e.toString(), e); throw e; } }
     */
}
