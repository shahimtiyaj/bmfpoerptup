package net.qsoft.brac.bmfpoerptup;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class BCheckResultActivity extends BCheckActivity {

	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	/* (non-Javadoc)
	 * @see net.qsoft.brac.bmfpo.BCheckActivity#onOk(android.view.View)
	 */
	@Override
	public void onOk(View view) {
		if(P8.IsValidAmount(this, editBalance)) {
            int amtBalance = Integer.parseInt(editBalance.getText().toString());
            if (signButton.getText().toString().equals("-"))
            	amtBalance=-amtBalance;
            if(intBalance==amtBalance) {
            	// Balance matched, return OK
            	setResult(Activity.RESULT_OK);
        		finish();
            }
            else {
            	// Balance not matched
            	P8.ErrorSound(this);
            	editBalance.setError(getString(R.string.error_bcheck_balance_mismatch));
            	editBalance.requestFocus();
            }
		}
	}

	/* (non-Javadoc)
	 * @see net.qsoft.brac.bmfpo.BCheckActivity#onCancel(android.view.View)
	 */
	@Override
	public void onCancel(View view) {
		// TODO Auto-generated method stub
    	setResult(Activity.RESULT_CANCELED);
		finish();
	}
}
