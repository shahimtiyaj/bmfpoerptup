package net.qsoft.brac.bmfpoerptup;

import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;

import net.qsoft.brac.bmfpoerptup.VOListAdapter.DataHolder;
import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.DAO.DBoardItem;
import net.qsoft.brac.bmfpoerptup.data.VO;

import java.util.List;

public class VOActivity extends SSActivity {
    private static final String TAG = VOActivity.class.getSimpleName();

    ListView lv = null;
    List<VO> items = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vo);
        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        lv = (ListView) findViewById(R.id.lstView);

        DAO da = new DAO(this);
        da.open();
        items = da.getVOList();
        da.close();

        VOListAdapter adapter = new VOListAdapter(this);
        adapter.addSectionHeaderItem("Todays VO");
        for (VO vo : items) {
            if (vo.isTodaysVO())
                adapter.addItem(vo);
        }

        adapter.addSectionHeaderItem("All Other VOs");
        for (VO vo : items) {
            if (!vo.isTodaysVO())
                adapter.addItem(vo);
        }

        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startAction(2, ((VO) ((DataHolder) lv.getItemAtPosition(position)).vo).get_OrgNo());

//                String item =  ((DataHolder) lv.getItemAtPosition(position)).Label; // .get("Id")) ;
//                //startAction(item);
//                Toast.makeText(getBaseContext(), item, Toast.LENGTH_LONG).show();
            }
        });

        lv.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                // TODO Auto-generated method stub
                startAction(1, ((VO) ((DataHolder) lv.getItemAtPosition(position)).vo).get_OrgNo());

                //startAction(item);
                //Toast.makeText(getBaseContext(), "Long pressed: " + item , Toast.LENGTH_LONG).show();

                return true;
            }
        });

        lv.setAdapter(adapter);
    }

    protected void startAction(Integer opt, String vono) {
        if (opt == 1) {
            // VO Summary
            Intent it = new Intent(this, TTActivity.class);
            it.putExtra(P8.VONO, vono);
//			it.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(it);

//			finish();
        } else if (opt == 2) {
            // Member list
            Intent it = new Intent(this, ClrMemberActivity.class);
            it.putExtra(P8.DBITEMS, DBoardItem.MEMBERS.name());
            it.putExtra(P8.VONO, vono);
            startActivity(it);
        }
    }

    public void onCancel(View view) {
        // Back
        finish();
    }
}
