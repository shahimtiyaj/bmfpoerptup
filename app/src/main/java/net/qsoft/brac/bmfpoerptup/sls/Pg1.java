package net.qsoft.brac.bmfpoerptup.sls;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import net.qsoft.brac.bmfpoerptup.R;
import net.qsoft.brac.bmfpoerptup.data.DBHelper;
import net.qsoft.brac.bmfpoerptup.data.SurveySL;

import java.util.HashMap;

import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.indexMemNo;
import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.memlist;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the  factory method to
 * create an instance of this fragment.
 */
public class Pg1 extends android.app.Fragment {

    private OnFragmentInteractionListener mListener;

    SimpleAdapter adapter;
    EditText inputSearch=null;
    ListView lv=null;

    public Pg1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.sls_fragment_pg1, container, false);
        inputSearch = (EditText) v.findViewById(R.id.inputSearch);
        lv = (ListView) v.findViewById(R.id.lstView);
        String[] from = new String[] {"[OrgNo]", "[OrgMemNo]", "[MemberName]"};
        int[] to = { R.id.textVONo, R.id.textMemNo, R.id.textMemName };

        adapter = new SimpleAdapter(getActivity(), memlist,
                R.layout.member_row, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
//                TextView mOrgNo = (TextView) v.findViewById(R.id.textVONo);
//                TextView mOrgMemNo = (TextView) v.findViewById(R.id.textMemNo);
//                String sOrgNo = mOrgNo.getText().toString().trim();
//                String sOrgMemNo = mOrgMemNo.getText().toString().trim();
                HashMap<String, String> x = (HashMap<String, String>) lv.getItemAtPosition(position);
                String sOrgNo = x.get(DBHelper.FLD_ORG_NO);
                String sOrgMemNo = x.get(DBHelper.FLD_ORG_MEM_NO);

                SurveySL ssl = new SurveySL(sOrgNo, sOrgMemNo);
                if(ssl.Exists())
                    v.setBackgroundResource(R.color.PowderBlue);
                else
                    v.setBackgroundResource(R.color.list_bkgrnd_color);
                return v;
            }
        };
        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> x = (HashMap<String, String>) lv.getItemAtPosition(position);
                String vono = x.get(DBHelper.FLD_ORG_NO);
                String memno = x.get(DBHelper.FLD_ORG_MEM_NO);
                String memName = x.get(DBHelper.FLD_ORG_MEM_NAME);

                mListener.onFragmentInteraction(position, vono, memno, memName);

//                Toast.makeText(getBaseContext(), vono + "-" + memno, Toast.LENGTH_LONG).show();
            }
        });

        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                adapter.getFilter().filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });
        lv.setItemChecked(indexMemNo, true);
        mListener = (OnFragmentInteractionListener) getActivity();
        return v;
    }


//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
////    @Override
////    public void onAttach(Context context) {
////        super.onAttach(context);
////        if (context instanceof OnFragmentInteractionListener) {
////            mListener = (OnFragmentInteractionListener) context;
////        } else {
////            throw new RuntimeException(context.toString()
////                    + " must implement OnFragmentInteractionListener");
////        }
////    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }
//
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
}


