package net.qsoft.brac.bmfpoerptup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class VOResultActivity extends VOActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		lv.setOnItemLongClickListener(null);
		lv.setLongClickable(false);
	}
	
	@Override
	protected void startAction(Integer opt, String vono) {
		if(opt==2) {
			// Return vonumber
			
			Intent it = new Intent();
			it.putExtra(P8.VONO, vono);
        	setResult(Activity.RESULT_OK, it);
    		finish();
		}
	}
	
	@Override
	public void onCancel(View view) {
		// TODO Auto-generated method stub
    	setResult(Activity.RESULT_CANCELED);
		finish();
	}
}
