package net.qsoft.brac.bmfpoerptup;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;

import net.qsoft.brac.bmfpoerptup.Volley.VolleyCustomRequest;
import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.util.CloudRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static android.os.Environment.DIRECTORY_DOWNLOADS;
import static com.android.volley.VolleyLog.d;
import static com.android.volley.VolleyLog.v;
import static net.qsoft.brac.bmfpoerptup.MainActivity.VolleyErrorResponse;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_CLOANS;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_CMEMBERS;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_GOOD_LOANS;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_TRANS_TRAIL;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_TRX_MAPPING;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_VOLIST;

/**
 * Created by QSPA10 on 1/27/2018.
 */

public class SyncDataFileInputStream extends Activity {
    private static final String TAG = SyncDataFileInputStream.class.getSimpleName();
    Handler handler;
    String req = null;
    Context context;
    public static String currentDateTime;
    private DownloadManager downloadManager;
    private long refid;
    private long referenceId;
    ArrayList<Long> list = new ArrayList<>();
    HashMap<String, String> po;
    HashMap<Integer, String> loantrxItems;
    HashMap<Integer, String> saveTrxItems;

    String CoNo = "", BranchCode = "", syncTime, vono, ServerColcFor;
    int ProjectCode = 0;
    int f = 0, f1 = 0, f2 = 0, f3 = 0;
    ProgressDialog progressDialog;
    private Button retry;
    //Date ColcDate;
    String ColcDate;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sync_data_layout);
        initializeViews();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    //JSON Post Request--------------------------------------------
    public void GetDataFromServer() {
        DAO da = new DAO(getApplicationContext());
        da.open();
        currentDateTime = LastDownloadDateTime();
        Log.d("SyncTime:", "SyncTime: " + currentDateTime);

        String hitURL = net.qsoft.brac.bmfpoerptup.App.getSyncUrl();
        HashMap<String, String> params = new HashMap<>();


        try {
            req = da.PrepareDataForSending().toString();
            params.put("req", req);

        } catch (IOException e) {
            e.printStackTrace();
        }

        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, null,
                new Response.Listener<JSONObject>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, "Server Response: " + response.toString());
                        DAO da = new DAO(getApplicationContext());
                        da.open();

                        try {
                            v("Response:%n %s", response.toString(4));
                            String status = response.getString("status");
                            Log.d("Message", status);

                            switch (status) {
                                case "A":
                                    try {
                                        JSONArray data1 = response.getJSONArray("data");
                                        JSONObject c = data1.getJSONObject(0);
                                        PO po = da.getPO();
                                        if (f3 == 0) {
                                            if (po.isPO()) {

                                                POExistsALert(c.getString("cono"), c.getString("coname"),
                                                        c.getString("branchcode"), c.getString("branchname"),
                                                        c.getString("opendate"), " ",
                                                        c.getString("projectcode"), c.getInt("SLno"));

                                            } else {
                                                da.AssignCollector(c.getString("cono"), c.getString("coname"),
                                                        c.getString("branchcode"), c.getString("branchname"),
                                                        c.getString("opendate"), " ",
                                                        c.getString("projectcode"), c.getInt("SLno"));
                                                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                                startActivity(intent);
                                            }

                                        }
                                        f3 = 1;

                                    } catch (Exception e) {
                                        Log.d(TAG, e.getMessage());
                                    }

                                    break;

                                case "PEND":
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Pending Data !", "Still data is pending in sync cloud.Please try later.");
                                    break;
                                case "HP":
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Pending Server Data !", "Still data is pending in server.Please try later.");
                                    break;
                                case "UL":
                                    try {
                                        da.ClearTransactions();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    Toast.makeText(getApplicationContext(), "Deleted Transact Data!! ", Toast.LENGTH_LONG).show();

                                case "D":
                                    if (f1 == 0) {
                                        DownloadJsonFile();
                                        f1 = 1;
                                    }
                                    break;
                                case "INA": //INA = In Active Device
                                    AlertDialog(R.drawable.ic_perm_device_information_black_24dp, "Deactivate Device !", "Your Device is Deactivate.Please Active your device first then try.");
                                    progressDialog.dismiss();
                                    break;
                                case "ASG": //ASG = Already Assign Device
                                    String message = response.getString("message");
                                    AlertDialog(R.drawable.ic_perm_device_information_black_24dp, "Already Assigned!", message);
                                    progressDialog.dismiss();
                                    break;
                                case "CL"://CL = Branch Closed
                                    AlertDialog(R.drawable.ic_do_not_disturb_off_black_24dp, "Branch Closed !", "The Branch you have requested to get data is closed at this moment, Please try again later.Thank you.");
                                    progressDialog.dismiss();
                                    break;
                                case "BCL"://CL = Branch not found
                                    AlertDialog(R.drawable.ic_do_not_disturb_off_black_24dp, "Branch Not Found!", "The Branch you have requested to get data is not found, Please try again later.Thank you.");
                                    progressDialog.dismiss();
                                    break;
                                case "F"://F = Device not found
                                    AlertDialog(R.drawable.ic_do_not_disturb_off_black_24dp, "Device Not Found!", "This Device is not found !!");
                                    progressDialog.dismiss();
                                    break;
                                default:
                                    AlertDialog(R.drawable.ic_perm_device_information_black_24dp, "Device Not Assign !", "Your Device is not Assign yet.Please Assign your device first then try.Thank You !");
                                    progressDialog.dismiss();
                                    break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            da.close();
                        }
                    }
                },

                new Response.ErrorListener()

                {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //progressDialog.dismiss();
                        volleyError.printStackTrace();
                        d(TAG, "Error: " + volleyError.getMessage());
                        P8.ShowError(SyncDataFileInputStream.this, VolleyErrorResponse(volleyError));
                    }
                }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                int AppVersionCode = BuildConfig.VERSION_CODE;
                String AppVersionName = BuildConfig.VERSION_NAME;
                String AppId = BuildConfig.APPLICATION_ID.substring(15);

                HashMap<String, String> headers = new HashMap<String, String>();
                //  headers.put("ProjectCode", String.valueOf(ProjectCode));

                headers.put("ProjectCode", "0".concat(String.valueOf(ProjectCode)));
                headers.put("PIN", CoNo);
                headers.put("BranchCode", BranchCode);
                headers.put("Content-Type", "application/json");
                headers.put("ApiKey", "7f30f4491cb4435984616d1913e88389");
                headers.put("ImeiNo", net.qsoft.brac.bmfpoerptup.App.getDeviceId());
                headers.put("LastSyncTime", P8.getLastSyncTime().toString());
                if (f == 0) {
                    headers.put("req", req);
                    f = 1;
                }
                headers.put("AppVersionCode", String.valueOf(AppVersionCode));
                headers.put("AppVersionName", AppVersionName);
                headers.put("AppId", AppId);

                return headers;
            }

        };

        int socketTimeout = 1800000;//18 Minutes  - change to what you want// 18,0,0,000 miliseconds=18 minutes
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        mRequestQueue.add(postRequest);

        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
    }


    //JSON Post Request--------------------------------------------

    public void SendLastDownloadStatus() {
        String hitURL = App.getLastDownloadStatusURL();
        //String hitURL = "http://103.43.93.162/sc/public/index.php/DownloadStatus";
        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        //  Log.d(TAG, "Server Response: " + response.toString());
                        Log.d("response", "Size: " + response.length());
                        try {
                            v("Response:%n %s", response.toString(4));
                            String status = response.getString("message");
                            // Toast.makeText(getApplicationContext(), status, Toast.LENGTH_LONG).show();
                            Log.d("Message", status);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        d(TAG, "Error: " + volleyError.getMessage());

                        P8.ShowError(SyncDataFileInputStream.this, VolleyErrorResponse(volleyError));
                    }
                }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                int AppVersionCode = BuildConfig.VERSION_CODE;
                String AppVersionName = BuildConfig.VERSION_NAME;
                String AppId = BuildConfig.APPLICATION_ID.substring(15);
                String DownloadStatus = "completed";

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("ProjectCode", String.valueOf(ProjectCode));


                headers.put("BranchCode", BranchCode);

                headers.put("ApiKey", "7f30f4491cb4435984616d1913e88389");
                headers.put("ImeiNo", net.qsoft.brac.bmfpoerptup.App.getDeviceId());
                headers.put("PIN", CoNo);
                headers.put("LastSyncTime", P8.getLastSyncTime().toString());
                headers.put("LastDownloadStatus", DownloadStatus);

                headers.put("AppVersionCode", String.valueOf(AppVersionCode));
                headers.put("AppVersionName", AppVersionName);
                headers.put("AppId", AppId);

                return headers;
            }
        };
        // Add the request to the RequestQueue.
        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
    }

    public void DownloadJsonFile() {
        list.clear();
        // Uri uri = Uri.parse(App.getDownloadDataURL() + CoNo + "results.json");
        //Uri uri = Uri.parse("http://35.229.211.156/data_backup/" + CoNo + "results.json");

        Uri uri = Uri.parse(App.getDownloadDataURL() + CoNo + ".zip");
        DownloadManager.Request request = new DownloadManager.Request(uri);
        //request.addRequestHeader("Accept-Encoding", "gzip, deflate");
        if (DownloadManager.ERROR_HTTP_DATA_ERROR == 404) {
            Toast.makeText(SyncDataFileInputStream.this, "Error", Toast.LENGTH_LONG).show();
        }
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("BMFPO-ERP" + CoNo + "-bmfpoerp.zip");
        request.setDescription("BMFPO Data Downloading" + CoNo + "-bmfpoerp.zip");
        request.setVisibleInDownloadsUi(true);

        if (f2 == 0) {
            request.setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS, CoNo + "-bmfpoerp.zip");
            f2 = 1;
        }

        request.allowScanningByMediaScanner();
        refid = downloadManager.enqueue(request);
        Log.e("OUTNM", "" + refid);
        list.add(refid);
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {

            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            String action = intent.getAction();

            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {

                DownloadManager.Query query = new DownloadManager.Query();

                query.setFilterById(referenceId);

                Cursor c = downloadManager.query(query);

                if (c.moveToFirst()) {
                    int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);

                    if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                        Log.e("IN", "" + referenceId);
                        list.remove(referenceId);

                        if (list.isEmpty()) {
                            Log.e("INSIDE", "" + referenceId);
                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(SyncDataFileInputStream.this, "channelId")
                                            .setSmallIcon(R.drawable.ic_launcher)
                                            .setContentTitle("BMFPO Data").setContentText("PO Data Download completed!");
                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(455, mBuilder.build());

                            SendLastDownloadStatus();
                            progressDialog.dismiss();
                            new ReadJSONAsyncTask().execute();

                            // new MyAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                            // new MyAsyncTask2().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

                        }

                    } else if (DownloadManager.STATUS_FAILED == c.getInt(columnIndex)) {

                        Log.i("handleData()", "Reason: " + c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON)));

                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(SyncDataFileInputStream.this, "channelId")
                                .setSmallIcon(R.drawable.ic_launcher).setContentTitle("PO Data Download Failed!!");
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(455, mBuilder.build());
                        progressDialog.dismiss();
                        AlertDialog(R.drawable.ic_error_black_24dp, "Download Failed !!", "Please , Try to download the file later.");

                    }
                }
            }

            registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ReadJSONFile() {
        DAO da = new DAO(getApplicationContext());
        da.open();
        unpackZip("/storage/emulated/0/Download/", CoNo + "-bmfpoerp.zip");
        try {
            java.io.File bmfpoFile = new java.io.File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS), CoNo + "results.json");
            FileInputStream stream = new FileInputStream(bmfpoFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                jsonStr = Charset.defaultCharset().decode(bb).toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }


            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONObject jsonObj1 = jsonObj.getJSONObject("data");

            // Getting data JSON Array nodes VOLIST
            JSONArray getVolist = jsonObj1.getJSONArray("volist");


            for (int i = 0; i < getVolist.length(); i++) {
                if (getVolist.isNull(i))
                    continue;

                JSONObject c = getVolist.getJSONObject(i);

                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_VOLIST + "(OrgNo, OrgName, OrgCategory, MemSexID, " +
                                "SavColcOption, LoanColcOption, SavColcDate, LoanColcDate, CONo, TargetDate, StartDate, " +
                                "EndDate, NextTargetDate, Territory, UpdatedAt, ProjectCode, BranchCode) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.getString("OrgNo"),
                                c.getString("OrgName"),
                                "null",// c.getString("OrgCategory"),
                                c.getString("MemSexId"),
                                c.getString("ColcOption"),
                                c.getString("ColcOption"),
                                c.getString("TargetDate"),
                                c.getString("TargetDate"),
                                c.getString("PIN"),
                                c.getString("TargetDate"),
                                c.getString("StartDate"),
                                "null",//c.getString("EndDate"),
                                c.getString("NextTargetDate"),
                                "null",//c.getString("Territory"),
                                c.getString("UpdatedAt"),
                                c.getString("ProjectCode"),
                                BranchCode //c.getString("BranchCode")
                        });

            }

      /*

            // Getting data JSON Array nodes CMEMBERS
            JSONArray getCmembers = jsonObj1.getJSONArray("cmembers");

            // looping through All nodes
            for (int i = 0; i < getCmembers.length(); i++) {
                if (getCmembers.isNull(i))
                    continue;

                JSONObject c = getCmembers.getJSONObject(i);

                // ColcDate = P8.ConvertStringToDate(da.GetTargetDate(c.getString("OrgNo")));
                ColcDate = da.GetTargetDate(c.getString("OrgNo"));


                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_CMEMBERS + "(ProjectCode, OrgNo, OrgMemNo, " +
                                "MemberName, SavBalan, SavPayable, CalcIntrAmt, TargetAmtSav, ReceAmt, " +
                                "ColcDate, AdjAmt, SB, SIA, SavingsRealized, " +
                                "MemSexID, NationalIDNo, PhoneNo, Walletno, AdmissionDate) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.getString("ProjectCode"),
                                c.getString("OrgNo"),
                                c.getString("OrgMemNo"),
                                c.getString("MemberName"),
                                c.getString("SavBalan"),
                                c.getString("SavPayable"),
                                c.getString("CalcIntrAmt"),
                                c.getString("TargetAmtSav"),
                                c.getString("ReceAmt"),
                                //P8.FormatDate(ColcDate, "yyyy-MM-dd"),
                                ColcDate,
                                "null", //c.getString("AdjAmt"),
                                c.getString("SavBalan"),
                                c.getString("TargetAmtSav"),
                                "null", //c.getString("SavingsRealized"),
                                c.getString("MemSexId"),
                                c.getString("NationalId"),
                                c.getString("ContactNo"),
                                c.getString("BkashWalletNo"),
                                c.getString("ApplicationDate")
                        });
            }


            // Getting data JSON Array nodes CLOANS
            JSONArray getCLoans = jsonObj1.getJSONArray("cloans");


            // looping through All nodes
            for (int i = 0; i < getCLoans.length(); i++) {

                if (getCLoans.isNull(i))
                    continue;

                JSONObject c = getCLoans.getJSONObject(i);

                int LnSts = c.getInt("LnStatus");
                int LnStatus = LnSts - 1;
                // ColcDate = P8.ConvertStringToDate(da.GetTargetDate(c.getString("OrgNo")));
                ColcDate = da.GetTargetDate(c.getString("OrgNo"));

                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_CLOANS + "(OrgNo, OrgMemNo, ProjectCode, LoanNo, LoanSlNo, " +
                                "ProductNo, IntrFactorLoan, PrincipalAmt, SchmCode, InstlAmtLoan, DisbDate, LnStatus, " +
                                "PrincipalDue, InterestDue, TotalDue, TargetAmtLoan, TotalReld, Overdue, BufferIntrAmt, " +
                                "ReceAmt, IAB, ODB, TRB, LB, PDB, IDB, " +
                                "InstlPassed, OldInterestDue, ProductSymbol, LoanRealized, ColcDate) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.getString("OrgNo"),
                                c.getString("OrgMemNo"),
                                c.getString("ProjectCode"),
                                c.getString("LoanNo"),
                                c.getString("LoanSlNo"),
                                c.getString("ProductNo"),
                                c.getString("IntrFactorLoan"),
                                c.getString("PrincipalAmount"),
                                "null", //c.getString("SchmCode"),
                                c.getString("InstlAmtLoan"),//c.getString("InstallmentAmount"), //change parameter new name
                                c.getString("DisbDate"),
                                String.valueOf(LnStatus),
                                c.getString("PrincipalDue"),
                                c.getString("InterestDue"),
                                c.getString("TotalDue"),
                                c.getString("TargetAmtLoan"),
                                c.getString("TotalReld"),
                                c.getString("Overdue"),
                                c.getString("BufferIntrAmt"),
                                "null",//c.getString("ReceAmt"),
                                c.getString("TargetAmtLoan"),//IAB
                                c.getString("Overdue"),
                                c.getString("TotalReld"),
                                c.getString("TotalDue"),//LB=PrincipalDue+InterestDue
                                c.getString("PrincipalDue"),
                                c.getString("InterestDue"),
                                c.getString("InstlPassed"),
                                "0", //c.getString("OldInterestDue"),
                                c.getString("ProductSymbol"),
                                "null",// c.getString("LoanRealized"),
                                //P8.FormatDate(ColcDate, "yyyy-MM-dd")
                                ColcDate
                        });

            }

*/
            // Getting data JSON Array nodes GoodLoans
            JSONArray goodloans = jsonObj1.getJSONArray("goodloans");

            // looping through All nodes
            for (int i = 0; i < goodloans.length(); i++) {
                if (goodloans.isNull(i))
                    continue;

                JSONObject c = goodloans.getJSONObject(i);

                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_GOOD_LOANS + "(OrgNo, OrgMemNo, MaxAmt," +
                                " Taken, UpdatedAt, _Month, _Year) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.getString("OrgNo"),
                                c.getString("OrgMemNo"),
                                c.getString("MaxAmt"),
                                c.getString("Taken"),
                                c.getString("UpdatedAt"),
                                c.getString("Month"),
                                c.getString("Year")
                        });
            }

            ReadCmembersJSONFile();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ReadCmembersJSONFile() {
        DAO da = new DAO(getApplicationContext());
        da.open();
        try {
            java.io.File bmfpoFile = new java.io.File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS), CoNo + "members.json");
            FileInputStream stream = new FileInputStream(bmfpoFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                jsonStr = Charset.defaultCharset().decode(bb).toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }


            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONObject jsonObj1 = jsonObj.getJSONObject("data");

            // Getting data JSON Array nodes CMEMBERS
            JSONArray getCmembers = jsonObj1.getJSONArray("cmembers");

            // looping through All nodes
            for (int i = 0; i < getCmembers.length(); i++) {
                if (getCmembers.isNull(i))
                    continue;

                JSONObject c = getCmembers.getJSONObject(i);

                // ColcDate = P8.ConvertStringToDate(da.GetTargetDate(c.getString("OrgNo")));
                ColcDate = da.GetTargetDate(c.getString("OrgNo"));


                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_CMEMBERS + "(ProjectCode, OrgNo, OrgMemNo, " +
                                "MemberName, SavBalan, SavPayable, CalcIntrAmt, TargetAmtSav, ReceAmt, " +
                                "ColcDate, AdjAmt, SB, SIA, SavingsRealized, " +
                                "MemSexID, NationalIDNo, PhoneNo, Walletno, AdmissionDate) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.getString("ProjectCode"),
                                c.getString("OrgNo"),
                                c.getString("OrgMemNo"),
                                c.getString("MemberName"),
                                c.getString("SavBalan"),
                                c.getString("SavPayable"),
                                c.getString("CalcIntrAmt"),
                                c.getString("TargetAmtSav"),
                                c.getString("ReceAmt"),
                                //P8.FormatDate(ColcDate, "yyyy-MM-dd"),
                                ColcDate,
                                "null", //c.getString("AdjAmt"),
                                c.getString("SavBalan"),
                                c.getString("TargetAmtSav"),
                                "null", //c.getString("SavingsRealized"),
                                c.getString("MemSexId"),
                                c.getString("NationalId"),
                                c.getString("ContactNo"),
                                c.getString("BkashWalletNo"),
                                c.getString("ApplicationDate")
                        });
            }

            ReadCLoansJSONFile();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ReadCLoansJSONFile() {
        DAO da = new DAO(getApplicationContext());
        da.open();
        try {
            java.io.File bmfpoFile = new java.io.File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS), CoNo + "cloans.json");
            FileInputStream stream = new FileInputStream(bmfpoFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                jsonStr = Charset.defaultCharset().decode(bb).toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }


            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONObject jsonObj1 = jsonObj.getJSONObject("data");


            // Getting data JSON Array nodes CLOANS
            JSONArray getCLoans = jsonObj1.getJSONArray("cloans");


            // looping through All nodes
            for (int i = 0; i < getCLoans.length(); i++) {

                if (getCLoans.isNull(i))
                    continue;

                JSONObject c = getCLoans.getJSONObject(i);

                int LnSts = c.getInt("LnStatus");
                int LnStatus = LnSts - 1;
                // ColcDate = P8.ConvertStringToDate(da.GetTargetDate(c.getString("OrgNo")));
                ColcDate = da.GetTargetDate(c.getString("OrgNo"));

                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_CLOANS + "(OrgNo, OrgMemNo, ProjectCode, LoanNo, LoanSlNo, " +
                                "ProductNo, IntrFactorLoan, PrincipalAmt, SchmCode, InstlAmtLoan, DisbDate, LnStatus, " +
                                "PrincipalDue, InterestDue, TotalDue, TargetAmtLoan, TotalReld, Overdue, BufferIntrAmt, " +
                                "ReceAmt, IAB, ODB, TRB, LB, PDB, IDB, " +
                                "InstlPassed, OldInterestDue, ProductSymbol, LoanRealized, ColcDate) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.getString("OrgNo"),
                                c.getString("OrgMemNo"),
                                c.getString("ProjectCode"),
                                c.getString("LoanNo"),
                                c.getString("LoanSlNo"),
                                c.getString("ProductNo"),
                                c.getString("IntrFactorLoan"),
                                c.getString("PrincipalAmount"),
                                "null", //c.getString("SchmCode"),
                                c.getString("InstlAmtLoan"),//c.getString("InstallmentAmount"), //change parameter new name
                                c.getString("DisbDate"),
                                String.valueOf(LnStatus),
                                c.getString("PrincipalDue"),
                                c.getString("InterestDue"),
                                c.getString("TotalDue"),
                                c.getString("TargetAmtLoan"),
                                c.getString("TotalReld"),
                                c.getString("Overdue"),
                                c.getString("BufferIntrAmt"),
                                "null",//c.getString("ReceAmt"),
                                c.getString("TargetAmtLoan"),//IAB
                                c.getString("Overdue"),
                                c.getString("TotalReld"),
                                c.getString("TotalDue"),//LB=PrincipalDue+InterestDue
                                c.getString("PrincipalDue"),
                                c.getString("InterestDue"),
                                c.getString("InstlPassed"),
                                "0", //c.getString("OldInterestDue"),
                                c.getString("ProductSymbol"),
                                "null",// c.getString("LoanRealized"),
                                //P8.FormatDate(ColcDate, "yyyy-MM-dd")
                                ColcDate
                        });

            }

            if (progressDialog.isShowing()) progressDialog.dismiss();

            da.WriteSyncTime(currentDateTime);
            da.UpdateEMethod(1);
            da.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ReadTransTrailJSONFile() {
        DAO da = new DAO(getApplicationContext());
        da.open();
        //unpackZip("/storage/emulated/0/Download/", CoNo + "-bmfpoerp.zip");
        try {
            java.io.File bmfpoFile = new java.io.File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS), CoNo + "transtrail1.json");
            FileInputStream stream = new FileInputStream(bmfpoFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                jsonStr = Charset.defaultCharset().decode(bb).toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }

            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONObject jsonObj1 = jsonObj.getJSONObject("data");

            // Getting data JSON Array nodes Transtrail
            JSONArray getTranstrail = jsonObj1.getJSONArray("transtrail1");

            // looping through All nodes
            for (int i = 0; i < getTranstrail.length(); i++) {
                if (getTranstrail.isNull(i))
                    continue;

                JSONObject c = getTranstrail.getJSONObject(i);

                String Transno = null;
                String loanTrx;
                String saveTrx;
                String transno;
                int trx = 0;

                // ColcDate = P8.ConvertStringToDate(da.GetTargetDate(c.getString("OrgNo")));
                ColcDate = da.GetTargetDate(c.getString("OrgNo"));

                ServerColcFor = c.getString("ColcFor");
                transno = P8.padLeft(c.getString("TransNo"), 11);
                if (c.has("TrxType") && !c.isNull("TrxType")) {
                    trx = c.getInt("TrxType");
                }

                Log.d("Transtrail_1", "Data updating...");
                Log.d("TrxType ", "Position:" + i + " :" + trx);

                for (int x = 1; x <= loantrxItems.size(); x++) {
                    if (trx == x && ServerColcFor.equals("L")) {
                        loanTrx = loantrxItems.get(x).concat(transno);
                        Transno = loanTrx;
                    } else {
                        for (int y = 1; y <= saveTrxItems.size(); y++) {
                            if (trx == y && ServerColcFor.equals("S")) {
                                saveTrx = saveTrxItems.get(y).concat(transno);
                                Transno = saveTrx;
                            }
                        }
                    }
                }


                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRANS_TRAIL + "(OrgNo, OrgMemNo, Projectcode, " +
                                "LoanNo, Tranamount, Colcdate, Transno, TrxType, ColcFor, UpdatedAt) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.getString("OrgNo"),
                                c.getString("OrgMemNo"),
                                c.getString("ProjectCode"),
                                c.getString("LoanNo"),
                                c.getString("Tranamount"),
                                // P8.FormatDate(ColcDate, "yyyy-MM-dd"),
                                ColcDate,
                                Transno,
                                c.getString("TrxType"),
                                c.getString("ColcFor"),
                                c.getString("UpdatedAt")});
                // Toast.makeText(App.getContext(), "Processing", Toast.LENGTH_LONG).show();
            }

            Log.d("Transtrail_1", "Data updating Done !");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ReadTransTrail2JSONFile() {
        DAO da = new DAO(getApplicationContext());
        da.open();

        try {
            java.io.File bmfpoFile = new java.io.File(Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS), CoNo + "transtrail2.json");
            FileInputStream stream = new FileInputStream(bmfpoFile);
            String jsonStr = null;
            try {
                FileChannel fc = stream.getChannel();
                MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                jsonStr = Charset.defaultCharset().decode(bb).toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                stream.close();
            }

            JSONObject jsonObj = new JSONObject(jsonStr);
            JSONObject jsonObj1 = jsonObj.getJSONObject("data");

            // Getting data JSON Array nodes Transtrail
            JSONArray getTranstrail = jsonObj1.getJSONArray("transtrail2");

            // looping through All nodes
            for (int i = 0; i < getTranstrail.length(); i++) {
                if (getTranstrail.isNull(i))
                    continue;

                JSONObject c = getTranstrail.getJSONObject(i);

                String Transno = null;
                String loanTrx;
                String saveTrx;
                String transno;
                int trx = 0;

                // ColcDate = P8.ConvertStringToDate(da.GetTargetDate(c.getString("OrgNo")));
                ColcDate = da.GetTargetDate(c.getString("OrgNo"));

                ServerColcFor = c.getString("ColcFor");
                transno = P8.padLeft(c.getString("TransNo"), 11);
                if (c.has("TrxType") && !c.isNull("TrxType")) {
                    trx = c.getInt("TrxType");
                }

                Log.d("Transtrail_2", "Data updating...");
                Log.d("TrxType ", "Position:" + i + ":" + trx);

                for (int x = 1; x <= loantrxItems.size(); x++) {
                    if (trx == x && ServerColcFor.equals("L")) {
                        loanTrx = loantrxItems.get(x).concat(transno);
                        Transno = loanTrx;
                    } else {
                        for (int y = 1; y <= saveTrxItems.size(); y++) {
                            if (trx == y && ServerColcFor.equals("S")) {
                                saveTrx = saveTrxItems.get(y).concat(transno);
                                Transno = saveTrx;
                            }
                        }
                    }
                }


                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRANS_TRAIL + "(OrgNo, OrgMemNo, Projectcode, " +
                                "LoanNo, Tranamount, Colcdate, Transno, TrxType, ColcFor, UpdatedAt) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.getString("OrgNo"),
                                c.getString("OrgMemNo"),
                                c.getString("ProjectCode"),
                                c.getString("LoanNo"),
                                c.getString("Tranamount"),
                                // P8.FormatDate(ColcDate, "yyyy-MM-dd"),
                                ColcDate,
                                Transno,
                                c.getString("TrxType"),
                                c.getString("ColcFor"),
                                c.getString("UpdatedAt")});
                // Toast.makeText(App.getContext(), "Processing", Toast.LENGTH_LONG).show();
            }
            Log.d("Transtrail_2", "Data updating Done !");

            DeleteDownloadJsonFile();
            DeleteTransTraiFile();
            DeleteDownloadZipFile();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // permission granted
        }
    }

    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }


    public void DeleteDownloadZipFile() {
        Uri uriZip = Uri.parse("/storage/emulated/0/Download/" + CoNo + "-bmfpoerp.zip");
        File zipfdelete = new File(uriZip.getPath());
        if (zipfdelete.exists()) {
            if (zipfdelete.delete()) {
                Log.d("BMFPO Zip File:", "Deleted");
                //Toast.makeText(getApplicationContext(), "Previous File Deleted" + uriZip.getPath(), Toast.LENGTH_LONG).show();
            } else {
                Log.d("BMFPO Zip File:", "Not Found ");
                //Toast.makeText(getApplicationContext(), "Previous File not Deleted" + uriZip.getPath(), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void DeleteDownloadJsonFile() {
        Uri uriJson = Uri.parse("/storage/emulated/0/Download/" + CoNo + "results.json");
        Uri uriCmembersJson = Uri.parse("/storage/emulated/0/Download/" + CoNo + "members.json");
        Uri uriCloansJson = Uri.parse("/storage/emulated/0/Download/" + CoNo + "cloans.json");

        File jsonfdelete = new File(uriJson.getPath());
        File jsonCmembersfdelete = new File(uriCmembersJson.getPath());
        File jsonCloansfdelete = new File(uriCloansJson.getPath());

        if (jsonfdelete.exists()) {
            if (jsonfdelete.delete()) {
                Log.d("BMFPO Json File:", "Deleted");
            } else {
                Log.d("Json File:", "Not Found for Deleting");
            }
        } else if (jsonCmembersfdelete.exists()) {
            if (jsonCmembersfdelete.delete()) {
                Log.d("Cmembers File:", "Deleted");
            } else {
                Log.d("Cmembers File:", "Not Found for Deleting");
            }
        } else if (jsonCloansfdelete.exists()) {
            if (jsonCloansfdelete.delete()) {
                Log.d("Cloans File:", "Deleted");
                //Toast.makeText(getApplicationContext(), "Previous File Deleted" + uriZip.getPath(), Toast.LENGTH_LONG).show();
            } else {
                Log.d("Cloans File:", "Not Found for Deleting");
                //Toast.makeText(getApplicationContext(), "Previous File not Deleted" + uriZip.getPath(), Toast.LENGTH_LONG).show();
            }
        } else {
            Log.d(" Vo_Cm_CL_GL File:", "Not Found ");
        }
    }

    public void DeleteTransTraiFile() {
        Uri uriTrans1Json = Uri.parse("/storage/emulated/0/Download/" + CoNo + "transtrail1.json");
        Uri uriTrans2Json = Uri.parse("/storage/emulated/0/Download/" + CoNo + "transtrail2.json");

        File jsonTrans1fdelete = new File(uriTrans1Json.getPath());
        File jsonTrans2fdelete = new File(uriTrans2Json.getPath());

        if (jsonTrans1fdelete.exists()) {
            if (jsonTrans1fdelete.delete()) {
                Log.d("TransTrail File:", "Deleted");
            } else {
                Log.d("TransTrail File:", "Not Found for Deleting");
            }
        } else if (jsonTrans2fdelete.exists()) {
            if (jsonTrans2fdelete.delete()) {
                Log.d("TransTrail_2 File:", "Deleted");
            } else {
                Log.d("TransTrail_2 File:", "Not Found for Deleting");
            }
        } else {
            Log.d("TransTrail File:", "Not Found ");
        }
    }

    public String LastDownloadDateTime() {
        // Log.d("LastDownloadDateTime:", "LastDownloadDateTime: " + LastDownloadDateTime());
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.getDefault());
        Date date = new Date();
        return dt.format(date);
    }

    public void DownloadManagerInitialize() {
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }


    private class DownLoadAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {

            final Thread thread = new Thread() {
                @Override
                public void run() {
                    Log.d("Load ServerData Thread ", "Started..");
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog = new ProgressDialog(SyncDataFileInputStream.this);
                            progressDialog.setCancelable(false);
                            progressDialog.setTitle(" Loading Data");
                            progressDialog.setMessage("Please wait...");
                            progressDialog.show();
                        }
                    });
                    GetDataFromServer();
                }
            };
            thread.start();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private class ReadJSONAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SyncDataFileInputStream.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle(" Updating Data");
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {
            Log.d("ReadJSONFile:  ", "Started..");
            ReadJSONFile();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) progressDialog.dismiss();
            progressDialog = null;
            new TransTrailAsyncTask().execute();
            Intent intent = new Intent(SyncDataFileInputStream.this, MainActivity.class);
            startActivity(intent);
        }
    }


    private class TransTrailAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {

            final Thread thread = new Thread() {
                @Override
                public void run() {
                    Log.d("TransTrail_1 Thread ", "Started..");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                        }
                    }, 1000);


                    ReadTransTrailJSONFile();
                }
            };
            thread.start();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
             new TransTrail2AsyncTask().execute();
        }
    }

    private class TransTrail2AsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {

            final Thread thread = new Thread() {
                @Override
                public void run() {
                    Log.d("TransTrail_2 Thread ", "Started..");
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //Do something after 100ms
                        }
                    }, 1000);

                    ReadTransTrail2JSONFile();
                }
            };
            thread.start();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initializeViews() {

        handler = new Handler();

        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();
        Log.v("onCreate", "maxMemory:" + Long.toString(maxMemory));
        //Call Downloading function----------------------
        DownloadManagerInitialize();
        // TrxType data loading
        TrxTypedata();
        //init progress dilog
        progressDialog = new ProgressDialog(SyncDataFileInputStream.this);
        //Database instance------------------------------
        DAO da = new DAO(getApplicationContext());
        da.open();
        PO po = da.getPO();
        CoNo = po.get_CONo();
        BranchCode = po.get_BranchCode();
        ProjectCode = po.get_ProjectCode();
        da.close();
        new DownLoadAsyncTask().execute();

    }

    public void onCancel(View view) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
      /*  if (progressDialog != null)
            progressDialog.dismiss(); */
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    private boolean unpackZip(String path, String zipname) {
        InputStream is;
        ZipInputStream zis;
        try {
            is = new FileInputStream(path + zipname);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;

            while ((ze = zis.getNextEntry()) != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;

                String filename = ze.getName();
                FileOutputStream fout = new FileOutputStream(path + filename);

                // reading and writing
                while ((count = zis.read(buffer)) != -1) {
                    baos.write(buffer, 0, count);
                    byte[] bytes = baos.toByteArray();
                    fout.write(bytes);
                    baos.reset();
                }

                fout.close();
                zis.closeEntry();
            }

            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    public void POExistsALert(final String cono, final String coName, final String brachCode,
                              final String branchName, final String branchOpenDate, final String SysDate, final String projectCode, final int sl) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention !")
                .setMessage("Are you sure you want to assign as new PO? If you want to assign this PO your previous data will be lost!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        DAO da = new DAO(App.getContext());
                        da.open();
                        try {
                            da.AssignCollector(cono, coName, brachCode, branchName, branchOpenDate, SysDate, projectCode, sl);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            da.close();
                        }

                        Toast.makeText(getApplicationContext(), "PO Data Updated!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(getApplicationContext(), "POST Transact Data!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                }).show();
    }


    public void AlertDialog(int drawable, String title, String message) {
        new AlertDialog.Builder(this)
                .setIcon(drawable)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                }).show();
    }


    public void TrxTypedata() {
        loantrxItems = new HashMap<Integer, String>();
        saveTrxItems = new HashMap<Integer, String>();
        //Saving TrxType Used
        saveTrxItems.put(1, "SC");
        saveTrxItems.put(2, "SD");
        saveTrxItems.put(3, "ST");
        saveTrxItems.put(4, "YI");
        saveTrxItems.put(6, "SD");
        saveTrxItems.put(7, "EC");
        saveTrxItems.put(8, "SC");
        saveTrxItems.put(9, "ST");
        //Saving TrxType probable used
        saveTrxItems.put(5, "IC");//Info change
        saveTrxItems.put(10, "TT");//Transferred
        // loan TrxType Used
        loantrxItems.put(3, "LC");
        loantrxItems.put(7, "ST");
        loantrxItems.put(13, "LC");
        //Loan TrxType  Not Allowed yet
        loantrxItems.put(1, "LD");
        loantrxItems.put(2, "LP");
        loantrxItems.put(4, "LR");
        loantrxItems.put(5, "RS");
        loantrxItems.put(6, "LS");
        loantrxItems.put(8, "GA");
        //Loan TrxType Probable use for future
        loantrxItems.put(9, "WP");
        loantrxItems.put(10, "WD");
        loantrxItems.put(11, "CS");
        loantrxItems.put(12, "NS");
        loantrxItems.put(14, "WI");
        loantrxItems.put(15, "WF");
        loantrxItems.put(16, "TT");

        for (int i = 1; i <= loantrxItems.size(); i++) {
            String loanTrx = loantrxItems.get(i);
            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRX_MAPPING + "(TrxShortType, TrxTypeValue, ColcFor) " +
                            "VALUES(?, ?, ?)",
                    new String[]{loanTrx, String.valueOf(i), "L"});
        }
        for (int i = 1; i <= saveTrxItems.size(); i++) {
            String saveTrx = saveTrxItems.get(i);
            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRX_MAPPING + "(TrxShortType, TrxTypeValue, ColcFor) " +
                            "VALUES(?, ?, ?)",
                    new String[]{saveTrx, String.valueOf(i), "S"});
        }
    }

    class DataLoadThread implements Runnable {
        String messaage;

        public DataLoadThread() {
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        public void run() {
            Log.d("Thread_0 ", "Started..");
            Looper.prepare();
            Looper.loop();
            Message message = Message.obtain();
            GetDataFromServer();
        }
    }

    private void CheckDwnloadStatus() {

        // TODO Auto-generated method stub
        DownloadManager.Query query = new DownloadManager.Query();
        //long id = preferenceManager.getLong(strPref_Download_ID, 0);
        query.setFilterById(referenceId);
        Cursor cursor = downloadManager.query(query);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(DownloadManager.COLUMN_STATUS);
            int status = cursor.getInt(columnIndex);
            int columnReason = cursor.getColumnIndex(DownloadManager.COLUMN_REASON);
            int reason = cursor.getInt(columnReason);

            switch (status) {
                case DownloadManager.STATUS_FAILED:
                    String failedReason = "";
                    switch (reason) {
                        case DownloadManager.ERROR_CANNOT_RESUME:
                            failedReason = "ERROR_CANNOT_RESUME";
                            break;
                        case DownloadManager.ERROR_DEVICE_NOT_FOUND:
                            failedReason = "ERROR_DEVICE_NOT_FOUND";
                            break;
                        case DownloadManager.ERROR_FILE_ALREADY_EXISTS:
                            failedReason = "ERROR_FILE_ALREADY_EXISTS";
                            break;
                        case DownloadManager.ERROR_FILE_ERROR:
                            failedReason = "ERROR_FILE_ERROR";
                            break;
                        case DownloadManager.ERROR_HTTP_DATA_ERROR:
                            failedReason = "ERROR_HTTP_DATA_ERROR";
                            break;
                        case DownloadManager.ERROR_INSUFFICIENT_SPACE:
                            failedReason = "ERROR_INSUFFICIENT_SPACE";
                            break;
                        case DownloadManager.ERROR_TOO_MANY_REDIRECTS:
                            failedReason = "ERROR_TOO_MANY_REDIRECTS";
                            break;
                        case DownloadManager.ERROR_UNHANDLED_HTTP_CODE:
                            failedReason = "ERROR_UNHANDLED_HTTP_CODE";
                            break;
                        case DownloadManager.ERROR_UNKNOWN:
                            failedReason = "ERROR_UNKNOWN";
                            break;
                    }

                    Toast.makeText(SyncDataFileInputStream.this,
                            "FAILED: " + failedReason,
                            Toast.LENGTH_LONG).show();
                    break;
                case DownloadManager.STATUS_PAUSED:
                    String pausedReason = "";

                    switch (reason) {
                        case DownloadManager.PAUSED_QUEUED_FOR_WIFI:
                            pausedReason = "PAUSED_QUEUED_FOR_WIFI";
                            break;
                        case DownloadManager.PAUSED_UNKNOWN:
                            pausedReason = "PAUSED_UNKNOWN";
                            break;
                        case DownloadManager.PAUSED_WAITING_FOR_NETWORK:
                            pausedReason = "PAUSED_WAITING_FOR_NETWORK";
                            break;
                        case DownloadManager.PAUSED_WAITING_TO_RETRY:
                            pausedReason = "PAUSED_WAITING_TO_RETRY";
                            break;
                    }

                    Toast.makeText(SyncDataFileInputStream.this,
                            "PAUSED: " + pausedReason,
                            Toast.LENGTH_LONG).show();
                    break;
                case DownloadManager.STATUS_PENDING:
                    Toast.makeText(SyncDataFileInputStream.this,
                            "PENDING",
                            Toast.LENGTH_LONG).show();
                    break;
                case DownloadManager.STATUS_RUNNING:
                    Toast.makeText(SyncDataFileInputStream.this,
                            "RUNNING",
                            Toast.LENGTH_LONG).show();
                    break;
                case DownloadManager.STATUS_SUCCESSFUL:

                    Toast.makeText(SyncDataFileInputStream.this,
                            "SUCCESSFUL",
                            Toast.LENGTH_LONG).show();
                    DownloadJsonFile();
                    break;
            }
        }
    }
}
