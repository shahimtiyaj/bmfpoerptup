package net.qsoft.brac.bmfpoerptup;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import net.qsoft.brac.bmfpoerptup.data.DAO;

public class WPGRepayActivity extends GRepayActivity {
	private static final String TAG = WPGRepayActivity.class.getSimpleName();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setTitle(getString(R.string.description_repay_wp));
	}

	/* (non-Javadoc)
	 * @see net.qsoft.brac.bmfpo.GRepayActivity#UpdateTransactions()
	 */
	@Override
	protected boolean UpdateTransactions() {
		// TODO Auto-generated method stub
		//return super.UpdateTransactions();
		DAO db=null;
		boolean ret=false;

		Log.d(TAG, "In update transaction");

		try {
			db = new DAO(this);
			db.open();
			db.addWPTransaction(cl, TType, mAmount);

			P8.OkSound(this);
			ret=true;

		} catch (Exception e) {
			P8.ShowError(this, e.toString());
		}
		finally {
			if(db!=null)
				db.close();
		}

		Intent intent = new Intent(this,WPTransActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(intent);
		
		finish();
		return ret;
		
	}
}
