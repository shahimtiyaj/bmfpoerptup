package net.qsoft.brac.bmfpoerptup.sls;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import net.qsoft.brac.bmfpoerptup.R;
import net.qsoft.brac.bmfpoerptup.data.SurveySL;

import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.getRGGetValue;
import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.setRGClearAndSet;



/**
 * A simple {@link Fragment} subclass.
 */
public class Pg2 extends android.app.Fragment {


    public Pg2() {
        // Required empty public constructor
    }

    RadioGroup rAge, rEducation, rHome;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.sls_fragment_pg2, container, false);

        rAge = (RadioGroup) v.findViewById(R.id.radioAge);
        rEducation = (RadioGroup) v.findViewById(R.id.radioEducation);
        rHome = (RadioGroup) v.findViewById(R.id.radioHome);


        return v;
    }
    public void onResume(){
        super.onResume();
        SurveySL ssl = SurveySL.getLastSurveySL();

        setRGClearAndSet(rAge, ssl.getAge());
        setRGClearAndSet(rEducation, ssl.getEducation());
        setRGClearAndSet(rHome, ssl.getHome());
        //for checkbox


       // editDate.setText(ssl.getSDateFormated());
        //editBranch.setText(ssl.);
       // editVo.setText(ssl.getOrgNo());
       // editMemNo.setText(ssl.getOrgMemNo());

    }

   // @Override
    public void onPause() {
        super.onPause();
        SurveySL ssl = SurveySL.getLastSurveySL();
        ssl.setAge(getRGGetValue(rAge));
        ssl.setEducation(getRGGetValue(rEducation));
        ssl.setHome(getRGGetValue(rHome));
    //For Checkbox



    }

}



