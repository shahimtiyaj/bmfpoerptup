package net.qsoft.brac.bmfpoerptup.data;

public class Branch {

	String _BranchCode;
	String _BranchName;
	Integer _BranchOpeningStatus;
	String _BranchOpenDate;
	String _BranchCloseDate;
	String _OpeningDayType;

	/**
	 * 
	 */
	public Branch() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param _BranchCode
	 * @param _BranchName
	 * @param _BranchOpeningStatus
	 * @param _BranchOpenDate
	 * @param _BranchCloseDate
	 * @param _OpeningDayType
	 */
	public Branch(String _BranchCode, String _BranchName,
			Integer _BranchOpeningStatus, String _BranchOpenDate,
			String _BranchCloseDate, String _OpeningDayType) {
		super();
		this._BranchCode = _BranchCode;
		this._BranchName = _BranchName;
		this._BranchOpeningStatus = _BranchOpeningStatus;
		this._BranchOpenDate = _BranchOpenDate;
		this._BranchCloseDate = _BranchCloseDate;
		this._OpeningDayType = _OpeningDayType;
	}

	/**
	 * @return the _BranchCode
	 */
	public final String get_BranchCode() {
		return _BranchCode;
	}
	/**
	 * @return the _BranchName
	 */
	public final String get_BranchName() {
		return _BranchName;
	}
	/**
	 * @return the _BranchOpeningStatus
	 */
	public final Integer get_BranchOpeningStatus() {
		return _BranchOpeningStatus;
	}
	/**
	 * @return the _BranchOpenDate
	 */
	public final String get_BranchOpenDate() {
		return _BranchOpenDate;
	}
	/**
	 * @return the _BranchCloseDate
	 */
	public final String get_BranchCloseDate() {
		return _BranchCloseDate;
	}
	/**
	 * @return the _OpeningDayType
	 */
	public final String get_OpeningDayType() {
		return _OpeningDayType;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return get_BranchCode()
				+ " - " + get_BranchName();
	}
	
}
