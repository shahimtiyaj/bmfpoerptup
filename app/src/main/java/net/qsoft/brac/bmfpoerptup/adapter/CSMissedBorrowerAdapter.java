package net.qsoft.brac.bmfpoerptup.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.R;

import java.util.HashMap;
import java.util.List;

public class CSMissedBorrowerAdapter extends ArrayAdapter<HashMap<String, String>> {
    // View lookup cache
    private static class ViewHolder {
        TextView si_no,st_name,vo_code,member_code,member_name,loan_number,
                 target_date,coll_date,target_ammount,coll_amount,mobile_no;
    }

    public CSMissedBorrowerAdapter(Context context, int resource, List<HashMap<String, String>> rows) {
        super(context, resource, rows);
        // TODO Auto-generated constructor stub
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        HashMap<String, String> row = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        CSMissedBorrowerAdapter.ViewHolder viewHolder; // view lookup cache stored in tag
        if (convertView == null) {
            viewHolder = new CSMissedBorrowerAdapter.ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.cs_missed_borrower_row, parent, false);
            viewHolder.si_no = (TextView) convertView.findViewById(R.id.textSIno);
            viewHolder.st_name = (TextView) convertView.findViewById(R.id.textSTName);
            viewHolder.vo_code = (TextView) convertView.findViewById(R.id.textVOCode);
            viewHolder.member_code = (TextView) convertView.findViewById(R.id.textMemberCode);
            viewHolder.member_name = (TextView) convertView.findViewById(R.id.textMemberName);
            viewHolder.loan_number = (TextView) convertView.findViewById(R.id.textLoanNumber);
            viewHolder.target_date = (TextView) convertView.findViewById(R.id.textTargetdate);
            viewHolder.coll_date = (TextView) convertView.findViewById(R.id.textColldate);
            viewHolder.target_ammount = (TextView) convertView.findViewById(R.id.textTargetAmount);
            viewHolder.coll_amount = (TextView) convertView.findViewById(R.id.textCollAmount);
            viewHolder.mobile_no = (TextView) convertView.findViewById(R.id.textMobileNo);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (CSMissedBorrowerAdapter.ViewHolder) convertView.getTag();
        }

        // Populate the data into the template view using the data object
        viewHolder.si_no.setText(row.get("SI No."));
        viewHolder.st_name.setText(row.get("ST Name"));
        viewHolder.vo_code.setText(row.get("VO Code"));
        viewHolder.member_code.setText(row.get("Member Code"));
        viewHolder.member_name.setText(row.get("Member Name"));
        viewHolder.loan_number.setText(row.get("Loan Number"));
        viewHolder.target_date.setText(row.get("Target Date"));
        viewHolder.coll_date.setText(row.get("Coll. Date"));
        viewHolder.target_ammount.setText(row.get("Target Amount"));
        viewHolder.coll_amount.setText(row.get("Coll. Amount"));
        viewHolder.mobile_no.setText(row.get("Mobile No."));

        // Return the completed view to render on screen
        return convertView;
    }
}
