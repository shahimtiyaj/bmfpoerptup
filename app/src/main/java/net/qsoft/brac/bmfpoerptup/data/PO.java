package net.qsoft.brac.bmfpoerptup.data;

import net.qsoft.brac.bmfpoerptup.P8;

import java.util.Date;

public class PO {
	public static final int EM_DEVICE=1;
	public static final int EM_DESKTOP=0;
	
	String _CONo;
	String _COName;
	Integer _SessionNo;
	String _OpenDate;
	Integer _OpeningBal;
	String _Password;
	Integer _EMethod;
	Integer _CashInHand;
	String _EnteredBy;	/* CO or Alternat CO number */
	String _SYSDATE;
	String _SYSTIME;
	String _AltCOName;
	String _BranchCode;
	String _BranchName;
	Date _BODate;
	Integer _BOStatus;
	Integer _SMSSupport;
	Integer _SurveySL;
	Integer _ProjectCode;
	Integer _SL;


	/**
	 * @param _CONo
	 * @param _COName
	 * @param _SessionNo
	 * @param _OpenDate
	 * @param _OpeningBal
	 * @param _Password
	 * @param _EMethod
	 * @param _CashInHand
	 * @param _EnteredBy
	 * @param _SYSDATE
	 * @param _SYSTIME
	 * @param _AltCOName
	 */
	public PO(String _CONo, String _COName, Integer _SessionNo,
			String _OpenDate, Integer _OpeningBal, String _Password,
			Integer _EMethod, Integer _CashInHand, String _EnteredBy,
			String _SYSDATE, String _SYSTIME, String _AltCOName,
			String _BranchCode, String _BranchName, String _BODate, Integer _BOStatus,
			Integer _SMSSupport, Integer _SurveySL, Integer _ProjectCode, Integer _SL) {
		super();
		this._CONo = _CONo;
		this._COName = _COName;
		this._SessionNo = _SessionNo;
		this._OpenDate = _OpenDate;
		this._OpeningBal = _OpeningBal;
		this._Password = _Password;
		this._EMethod = _EMethod;
		this._CashInHand = _CashInHand;
		this._EnteredBy = _EnteredBy;
		this._SYSDATE = _SYSDATE;
		this._SYSTIME = _SYSTIME;
		this._AltCOName = _AltCOName;
		this._BranchCode = _BranchCode;
		this._BranchName = _BranchName;
		this._BODate = P8.ConvertStringToDate(_BODate, "yyyy-MM-dd");
		this._BOStatus = _BOStatus;
		this._SMSSupport = _SMSSupport;
		this._SurveySL = _SurveySL;
		this._ProjectCode=_ProjectCode;
        this._SL=_SL;

    }


	public Integer get_SurveySL() {
		return _SurveySL;
	}

	public void set_SurveySL(Integer _SurveySL) {
		this._SurveySL = _SurveySL;
	}

	public Integer get_SMSSupport() {
		return _SMSSupport;
	}

	public void set_SMSSupport(Integer _SMSSupport) {
		this._SMSSupport = _SMSSupport;
	}

	/**
	 * @return the _Password
	 */
	public final String get_Password() {
		return _Password;
	}
	/**
	 * @param _Password the _Password to set
	 */
	public final void set_Password(String _Password) {
		this._Password = _Password;
	}
	/**
	 * @return the _EMethod
	 */
	public final Integer get_EMethod() {
		return _EMethod;
	}
	/**
	 * @param _EMethod the _EMethod to set
	 */
	public final void set_EMethod(Integer _EMethod) {
		this._EMethod = _EMethod;
	}
	/**
	 * @return the _CashInHand
	 */
	public final Integer get_CashInHand() {
		return _CashInHand;
	}
	/**
	 * @param _CashInHand the _CashInHand to set
	 */
	public final void set_CashInHand(Integer _CashInHand) {
		this._CashInHand = _CashInHand;
	}
	/**
	 * @return the _CONo
	 */
	public final String get_CONo() {
		return _CONo;
	}
	/**
	 * @return the _COName
	 */
	public final String get_COName() {
		return _COName;
	}
	/**
	 * @return the _SessionNo
	 */
	public final Integer get_SessionNo() {
		return _SessionNo;
	}
	/**
	 * @return the _OpenDate
	 */
	public final String get_OpenDate() {
		return _OpenDate;
	}
	/**
	 * @return the _OpeningBal
	 */
	public final Integer get_OpeningBal() {
		return _OpeningBal;
	}
	/**
	 * @return the _EnteredBy
	 */
	public final String get_EnteredBy() {
		return _EnteredBy;
	}
	/**
	 * @return the _SYSDATE
	 */
	public final String get_SYSDATE() {
		return _SYSDATE;
	}
	/**
	 * @return the _SYSTIME
	 */
	public final String get_SYSTIME() {
		return _SYSTIME;
	}
	/**
	 * @return the _AltCOName
	 */
	public final String get_AltCOName() {
		return _AltCOName;
	}
	
	/**
	 * @return the _BranchCode
	 */
	public final String get_BranchCode() {
		return _BranchCode;
	}
	/**
	 * @return the _BranchName
	 */
	public final String get_BranchName() {
		return _BranchName;
	}

	public final Date get_BranchOpenDate() {
		return _BODate;
	}

	public final Integer get_BranchOpenStatus() {
		return _BOStatus;
	}
	public Integer get_ProjectCode() {
		return _ProjectCode;
	}

	public void set_ProjectCode(Integer _ProjectCode) {
		this._ProjectCode = _ProjectCode;
	}


    public Integer get_SL() {
        return _SL;
    }

    public void set_SL(Integer _SL) {
        this._SL = _SL;
    }




    public final boolean isPO() {
		return !(_CONo.length() == 0 || _CONo.contentEquals("00000000"));
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return (get_EnteredBy() != null ? get_EnteredBy() : "") + " - "
				+ (get_AltCOName() != null ? get_AltCOName() : "");
	}

}
