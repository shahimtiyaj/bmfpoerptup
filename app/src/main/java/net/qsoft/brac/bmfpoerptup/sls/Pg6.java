package net.qsoft.brac.bmfpoerptup.sls;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import net.qsoft.brac.bmfpoerptup.R;
import net.qsoft.brac.bmfpoerptup.data.SurveySL;

import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.getCheckedValue;
import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.setCheckedValue;

/**
 * A simple {@link Fragment} subclass.
 */
public class Pg6 extends android.app.Fragment {


    CheckBox cOALand;
    CheckBox cOATV ;
    CheckBox cOAMC;
    CheckBox cOAFridge ;
    CheckBox cPINGO ;
    CheckBox cPIBank ;
    CheckBox cPIRel;
    CheckBox cPIWS;
    CheckBox cPIOS;

    public Pg6() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.sls_fragment_pg6, container, false);


        cOALand = (CheckBox) v.findViewById(R.id.chkOALand);
        cOATV = (CheckBox) v.findViewById(R.id.chkOATV);
        cOAMC = (CheckBox) v.findViewById(R.id.chkOAMC);
        cOAFridge = (CheckBox) v.findViewById(R.id.chkOAFridge);

        cPINGO = (CheckBox) v.findViewById(R.id.chkPINGO);
        cPIBank = (CheckBox) v.findViewById(R.id.chkPIBank);
        cPIRel = (CheckBox) v.findViewById(R.id.chkPIRel);
        cPIWS = (CheckBox) v.findViewById(R.id.chkPIWS);
        cPIOS = (CheckBox) v.findViewById(R.id.chkPIOS);

        return v;

    }

    public void onResume(){
        super.onResume();
        SurveySL ssl = SurveySL.getLastSurveySL();

        setCheckedValue (cOALand, ssl.getOA_LAND());
        setCheckedValue(cOATV, ssl.getOA_TV());
        setCheckedValue(cOAMC, ssl.getOA_MC());
        setCheckedValue(cOAFridge, ssl.getOA_FR());
        setCheckedValue( cPINGO , ssl.getPI_NGO());
        setCheckedValue(cPIBank, ssl.getPI_BANK());
        setCheckedValue(cPIRel, ssl.getPI_REL());
        setCheckedValue (cPIWS, ssl.getPI_WS());
        setCheckedValue(cPIOS, ssl.getPI_OS());
    }

    // @Override
    public void onPause() {
        super.onPause();
        SurveySL ssl = SurveySL.getLastSurveySL();

        ssl.setOA_LAND(getCheckedValue(cOALand));
        ssl.setOA_TV(getCheckedValue(cOATV));
        ssl.setOA_MC(getCheckedValue(cOAMC));
        ssl.setOA_FR(getCheckedValue(cOAFridge));
        ssl.setPI_NGO(getCheckedValue( cPINGO));
        ssl.setPI_BANK(getCheckedValue(cPIBank));
        ssl.setPI_REL(getCheckedValue(cPIRel));
        ssl.setPI_WS(getCheckedValue(cPIWS));
        ssl.setPI_OS(getCheckedValue(cPIOS));
    }
}
