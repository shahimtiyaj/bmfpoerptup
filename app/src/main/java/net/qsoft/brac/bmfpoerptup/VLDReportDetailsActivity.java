package net.qsoft.brac.bmfpoerptup;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.DBHelper;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.data.VO;

import java.util.ArrayList;
import java.util.HashMap;

public class VLDReportDetailsActivity extends SSActivity {
	private static final String TAG = VLDReportActivity.class.getSimpleName();

	TextView branchName;
	TextView voName;
	ListView lv;
	Button okButton;

	ArrayList<HashMap<String,String>> items = null;
	String vono;
	String ddate;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vldreport_details);

		branchName = (TextView) findViewById(R.id.textBranchName);
		voName = (TextView) findViewById(R.id.textVOName);
		lv = (ListView)findViewById(R.id.listVLDRD);
		okButton = (Button) findViewById(R.id.okButton);

		okButton.setVisibility(View.GONE);

		if(getIntent().hasExtra(P8.VONO)) {
			vono = getIntent().getExtras().getString(P8.VONO);
			Log.d(TAG, "VO = " + vono);
		}
		if(getIntent().hasExtra(P8.DDATE)) {
			ddate = getIntent().getExtras().getString(P8.DDATE);
			Log.d(TAG, "DDate = " + ddate);
		}
	
		if(VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}			
		
		createVLDReportDetails();
	}

	private void createVLDReportDetails() {
		DAO da = new DAO(this);
		da.open();
		PO po = da.getPO();
		VO vo = da.getVO(vono);
		items = da.getVODisbursementDetails(vono, ddate);
		da.close();
		
		branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());
		voName.setText(vo.get_OrgNo() + " - " + vo.get_OrgName());
		
        String[] from = new String[] {DBHelper.FLD_ORG_MEM_NAME, DBHelper.FLD_LOAN_NO, DBHelper.FLD_LOAN_SL_NO,
        								DBHelper.FLD_DISBURSEMENT_DATE, DBHelper.FLD_PRINCIPAL_AMT};
        int[] to = { R.id.textMem, R.id.textLN, R.id.textLNSl, R.id.textDD, R.id.textDA };

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), items, R.layout.vldreport_details_row, from, to);
        lv.setAdapter(adapter);
	}

	public void onCancel(View view) {
		// Back
		finish();
	}	
}
