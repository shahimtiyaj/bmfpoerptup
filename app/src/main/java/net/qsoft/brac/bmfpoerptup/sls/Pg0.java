package net.qsoft.brac.bmfpoerptup.sls;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.R;
import net.qsoft.brac.bmfpoerptup.data.DBHelper;

import java.util.HashMap;

import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.indexVONo;
import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.volist;

public class Pg0 extends  android.app.Fragment {

    SimpleAdapter adapter;
    EditText inputSearch=null;
    ListView lv=null;
    Button btnShowData=null;
    private OnFragmentInteractionListener mListener;

    public Pg0() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.sls_fragment_pg0, container, false);
        btnShowData = (Button) v.findViewById(R.id.btnShowData);
        inputSearch = (EditText) v.findViewById(R.id.inputSearch);
        lv = (ListView) v.findViewById(R.id.lstView);
        String[] from = new String[] {"[OrgNo]", "[OrgName]"};
        int[] to = { R.id.textVONo, R.id.textMemName };

        adapter = new SimpleAdapter(getActivity(), volist,
                R.layout.member_row, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                TextView mOrgMemNo = (TextView) v.findViewById(R.id.textMemNo);
                mOrgMemNo.setText("");
                return v;
            }
        };

        lv.setAdapter(adapter);
        lv.setItemChecked(indexVONo, true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HashMap<String, String> x = (HashMap<String, String>) lv.getItemAtPosition(position);
                String vono = x.get(DBHelper.FLD_ORG_NO);

                mListener.onFragmentInteraction(position, vono, "", "");

//                Toast.makeText(getBaseContext(), vono + "-" + memno, Toast.LENGTH_LONG).show();
            }
        });


        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                adapter.getFilter().filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub

            }
        });

        btnShowData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onFragmentInteraction(0, "", "", "");
            }
        });

        mListener = (OnFragmentInteractionListener) getActivity();
        return v;
    }

//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }
//
//    @Override
//    public void onDetach() {
//        super.onDetach();
//        mListener = null;
//    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */

}
