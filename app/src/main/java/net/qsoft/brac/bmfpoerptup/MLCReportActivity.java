package net.qsoft.brac.bmfpoerptup;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.CLoan;
import net.qsoft.brac.bmfpoerptup.data.CMember;
import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.DBHelper;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.data.VO;

import java.util.ArrayList;
import java.util.HashMap;

public class MLCReportActivity extends SSActivity {
	private static final String TAG = MLCReportActivity.class.getSimpleName();

	TextView branchName;
	TextView voName;
	TextView memName;
	TextView loanNum;
	TextView disbDate;
	TextView disbAmt;
	ListView lv;
	Button okButton;

	ArrayList<HashMap<String,String>> items = null;
	int intLoanNumber;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mem_loan_colc_rep);

		branchName = (TextView) findViewById(R.id.textBranchName);
		voName = (TextView) findViewById(R.id.textVOName);
		memName = (TextView) findViewById(R.id.textMem);
		loanNum = (TextView) findViewById(R.id.textLoanNo);
		disbDate = (TextView) findViewById(R.id.textDisbDate);
		disbAmt  = (TextView) findViewById(R.id.textDisbAmt);
		lv = (ListView)findViewById(R.id.listViewMLR);
		okButton = (Button) findViewById(R.id.okButton);

		okButton.setVisibility(View.GONE);

		if(getIntent().hasExtra(P8.LOANNO)) {
			intLoanNumber = getIntent().getExtras().getInt(P8.LOANNO);
			Log.d(TAG, "In has extra " + intLoanNumber);
		} 
		if(VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}			

		createMLReport(intLoanNumber);
	}
	
	private void createMLReport(int ln) {
		DAO da = new DAO(this);
		da.open();
		PO po = da.getPO();
		CLoan cl = da.getCloan(ln);
		VO vo = da.getVO(cl.get_OrgNo());
		CMember cm = da.getCMember(cl.get_OrgNo(), cl.get_OrgMemNo());
		items = da.getMemWiseLoanRep(ln);
//		Log.d(TAG, "Items: " + items );
		da.close();
		
		branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());
		voName.setText(vo.get_OrgNo() + " - " + vo.get_OrgName());
		memName.setText("Member: " + cm.get_OrgMemNo() + " - " + cm.get_MemberName());
		loanNum.setText("Loan#: " + Integer.toString(ln));
		disbDate.setText("Date: " + P8.FormatDate(cl.get_DisbDate(), "MMM dd, yyyy" ));
		disbAmt.setText("Disb. Amount: " + cl.get_PrincipalAmt().toString());
		
        String[] from = new String[] {DBHelper.FLD_COLC_DATE, DBHelper.FLD_TARGET_LOAN_AMT, DBHelper.FLD_COLC_AMT};
        int[] to = { R.id.textColcDate, R.id.textTargetAmt, R.id.textColcAmt };

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), items, R.layout.mlcreport_row, from, to);
        lv.setAdapter(adapter);
	}

	public void onCancel(View view) {
		// Back
		finish();
	}	
}
