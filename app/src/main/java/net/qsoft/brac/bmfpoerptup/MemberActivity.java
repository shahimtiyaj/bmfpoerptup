package net.qsoft.brac.bmfpoerptup;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.DBHelper;

import java.util.ArrayList;
import java.util.HashMap;

public class MemberActivity extends SSActivity {
	private static final String TAG = MemberActivity.class.getSimpleName();

	ArrayList<HashMap<String, String>> memlist=null;
	SimpleAdapter adapter;
	EditText inputSearch=null;
	ListView lv=null;
	DAO.DBoardItem itm=null;
	String vono=null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_member);

		inputSearch = (EditText) findViewById(R.id.inputSearch);
		lv = (ListView) findViewById(R.id.lstView);

		setupList();

		String[] from = new String[] {"[OrgNo]", "[OrgMemNo]", "[MemberName]"};
        int[] to = { R.id.textVONo, R.id.textMemNo, R.id.textMemName };

	    adapter = new SimpleAdapter(getApplicationContext(), memlist,
	                R.layout.member_row, from, to);
	    lv.setAdapter(adapter);

	    lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	HashMap<String, String> x = (HashMap<String, String>) lv.getItemAtPosition(position);
            	String vono = x.get(DBHelper.FLD_ORG_NO);
            	String memno = x.get(DBHelper.FLD_ORG_MEM_NO);
            	Intent it = new Intent(getApplicationContext(), MempfActivity.class);
            	it.putExtra(P8.VONO, vono);
            	it.putExtra(P8.MEMNO, memno);
            	startActivity(it);

//                Toast.makeText(getBaseContext(), vono + "-" + memno, Toast.LENGTH_LONG).show();
            }
        });

        /**
         * Enabling Search Filter
         * */
        inputSearch.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				// TODO Auto-generated method stub
				MemberActivity.this.adapter.getFilter().filter(s);
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// TODO Auto-generated method stub

			}

			@Override
			public void afterTextChanged(Editable s) {
				// TODO Auto-generated method stub

			}
		});

	}

	protected void setupList() {

		Intent it = getIntent();
		if(it.hasExtra(P8.DBITEMS)) {
			itm = DAO.DBoardItem.valueOf(it.getStringExtra(P8.DBITEMS));
		}
		if(it.hasExtra(P8.VONO))
			vono = it.getStringExtra(P8.VONO);
//		Log.d(TAG, "DBItem: " + itm.toString());
//		Log.d(TAG, "VONo.: " + vono);
		
		DAO da = new DAO(this);
		da.open();
		memlist = da.getMemberList(itm, vono);
		da.close();

	}

	public void onCancel(View view) {
		// Back
		finish();
	}		
}
