package net.qsoft.brac.bmfpoerptup.sls;

/**
 * Created by zferdaus on 10/25/2017.
 */

public interface OnFragmentInteractionListener {
    void onFragmentInteraction(Integer index, String vno, String mno, String mname);
}
