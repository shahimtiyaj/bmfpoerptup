package net.qsoft.brac.bmfpoerptup;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class App extends Application {

    private static final String TAG = App.class.getSimpleName();
    public static final String BaseUrl = "http://35.229.211.156";
    public static final String SyncUrl = "/sc/Sync";
    public static final String LastDownloadStatusUrl = "/sc/DownloadStatus";
    public static final String DownloadDataUrl = "/json/";
    public static final String SMSSendUrl = "/sms/send.php";

    private static Context mContext;
    private static Date datBranchOPenDate = null;
    private static Date datLastSyncDate = null;
    private static int versionCode = 0;
    private static String versionName = "";
    private static Activity mSMSActivity = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;

        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            versionName = packageInfo.versionName;
            versionCode = packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, e.getMessage());
        }
    }

    public static Context getContext() {
        return mContext;
    }

    public static final Integer getVersionCode() {
        return versionCode;
    }

    public static final String getVersionName() {
        return versionName;
    }

    public static final void setSMSActivity(Activity x) {
        mSMSActivity = x;
    }

    public static final Activity getSMSActivity() {
        return mSMSActivity;
    }

    public static final boolean hasSMSSupport() {
        return mSMSActivity != null;
    }

    /**
     * @return the datBranchOPenDate
     */
    public static final Date getBranchOPenDate() {
        return datBranchOPenDate;
    }

    /**
     * @param datBranchOPenDate the datBranchOPenDate to set
     */
    public static final void setBranchOPenDate(Date datBranchOPenDate) {
        App.datBranchOPenDate = datBranchOPenDate;
    }

    /**
     * @return the datLastSyncDate
     */
    public static final Date getLastSyncDate() {
        return datLastSyncDate;
    }

    /**
     * @param datLastSyncDate the datLastSyncDate to set
     */
    public static final void setLastSyncDate(Date datLastSyncDate) {
        App.datLastSyncDate = datLastSyncDate;
    }

    public static final String getLastSyncGaps() {
        String ret = "";
        Date c = new Date();
        if (getLastSyncDate() != null) {
            if (getLastSyncDate().after(P8.ConvertStringToDate("2000-01-01", "yyyy-MM-dd"))) {
                long different = c.getTime() - getLastSyncDate().getTime();

                long secondsInMilli = 1000;
                long minutesInMilli = secondsInMilli * 60;
                long hoursInMilli = minutesInMilli * 60;
                long daysInMilli = hoursInMilli * 24;

                long elapsedDays = different / daysInMilli;
                different = different % daysInMilli;

                long elapsedHours = different / hoursInMilli;
                different = different % hoursInMilli;

                long elapsedMinutes = different / minutesInMilli;
                different = different % minutesInMilli;

                long elapsedSeconds = different / secondsInMilli;

                ret = String.format("Last download since\n %d days, %d hours, %d minutes",
                        elapsedDays, elapsedHours, elapsedMinutes);    //, %d seconds%n, , elapsedSeconds
            }
        }

        return ret;
    }

    public static final String getDeviceId() {
        //retrieve a reference to an instance of TelephonyManager
        TelephonyManager telephonyManager = (TelephonyManager) getContext().getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
        }
        return telephonyManager.getDeviceId();
    }

    public static final String getSyncUrl() {
        return BaseUrl + SyncUrl;
    }

    public static final String getSMSSendURL() {
        return BaseUrl + SMSSendUrl;
    }

    public static final String getLastDownloadStatusURL() {
        return BaseUrl + LastDownloadStatusUrl;
    }

    public static final String getDownloadDataURL() {
        return BaseUrl + DownloadDataUrl;
    }


    public static void showDebugDBAddressLogToast(Context context) {
        if (BuildConfig.DEBUG) {
            try {
                Class<?> debugDB = Class.forName("com.amitshekhar.DebugDB");
                Method getAddressLog = debugDB.getMethod("getAddressLog");
                Object value = getAddressLog.invoke(null);
                Toast.makeText(context, (String) value, Toast.LENGTH_LONG).show();
            } catch (Exception ignore) {

            }
        }
    }


    public static void unzip(File zipFile, File targetDirectory) throws IOException {
        ZipInputStream zis = new ZipInputStream(
                new BufferedInputStream(new FileInputStream(zipFile)));
        try {
            ZipEntry ze;
            int count;
            byte[] buffer = new byte[8192];
            while ((ze = zis.getNextEntry()) != null) {
                File file = new File(targetDirectory, ze.getName());
                File dir = ze.isDirectory() ? file : file.getParentFile();
                if (!dir.isDirectory() && !dir.mkdirs())
                    throw new FileNotFoundException("Failed to ensure directory: " +
                            dir.getAbsolutePath());
                if (ze.isDirectory())
                    continue;
                FileOutputStream fout = new FileOutputStream(file);
                try {
                    while ((count = zis.read(buffer)) != -1)
                        fout.write(buffer, 0, count);
                } finally {
                    fout.close();
                }
            /* if time should be restored as well
            long time = ze.getTime();
            if (time > 0)
                file.setLastModified(time);
            */
            }
        } finally {
            zis.close();
        }
    }
}
