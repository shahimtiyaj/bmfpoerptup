package net.qsoft.brac.bmfpoerptup.util;

import java.util.BitSet;

import android.app.ActionBar.LayoutParams;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.Log;
import android.view.View;

public class GraphicsPrint {
	private static final String TAG = GraphicsPrint.class.getSimpleName();

	private BluetoothService mService;
	private BitSet dots;

	Bitmap mbitmap = null;
	Canvas mcanvas = null;
	Paint paint = null;
	int mbaseline = 24;

	public GraphicsPrint(BluetoothService mS) {
		// TODO Auto-generated constructor stub
		mService = mS;

		paint = new Paint();
		Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
		paint.setTypeface(font);
		paint.setTextSize(24);
		paint.setAntiAlias(true);

		RefreshSurface(0, 0);
	}

	public static Bitmap loadBitmapFromView(View v) {
		if (v.getMeasuredHeight() <= 0) {
			v.measure(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		}
		Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(),
				v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
		v.draw(c);
		return b;

		// Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width,
		// v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
		// Canvas c = new Canvas(b);
		// Log.d(TAG, "Left: " + v.getLeft() + "\nTop: " + v.getTop() +
		// "\nRight: " + v.getRight() + "\nBottom: " + v.getBottom());
		// v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
		// v.draw(c);
		// return b;
	}

	public static Bitmap getViewBitmap(View view) {
		// Get the dimensions of the view so we can re-layout the view at its
		// current size
		// and create a bitmap of the same size
		int width = view.getWidth();
		int height = view.getHeight();

		int measuredWidth = View.MeasureSpec.makeMeasureSpec(width,
				View.MeasureSpec.EXACTLY);
		int measuredHeight = View.MeasureSpec.makeMeasureSpec(height,
				View.MeasureSpec.EXACTLY);

		// Cause the view to re-layout
		view.measure(measuredWidth, measuredHeight);
		view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

		// Create a bitmap backed Canvas to draw the view into
		Log.d(TAG,
				"Height: " + view.getHeight() + "\nWidth: " + view.getWidth());

		Bitmap b = Bitmap.createBitmap(view.getWidth(), view.getHeight(),
				Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);

		// Now that the view is laid out and we have a canvas, ask the view to
		// draw itself into the canvas
		view.draw(c);

		Bitmap nb = Bitmap.createScaledBitmap(b, 384, 30, false);
		return nb;
	}

	public static int getScale(int originalWidth, int originalHeight,
			final int requiredWidth, final int requiredHeight) {
		// a scale of 1 means the original dimensions
		// of the image are maintained
		int scale = 1;

		// calculate scale only if the height or width of
		// the image exceeds the required value.
		if ((originalWidth > requiredWidth)
				|| (originalHeight > requiredHeight)) {
			// calculate scale with respect to
			// the smaller dimension
			if (originalWidth < originalHeight)
				scale = Math.round((float) originalWidth / requiredWidth);
			else
				scale = Math.round((float) originalHeight / requiredHeight);

		}

		return scale;
	}

	// public Bitmap ScaleImage(Bitmap image, double scale)
	// {
	// int newWidth = (int)(image.Width * scale);
	// int newHeight = (int)(image.Height * scale);
	//
	// Bitmap result = new Bitmap(newWidth, newHeight,
	// PixelFormat.Format24bppRgb);
	// result.SetResolution(image.HorizontalResolution,
	// image.VerticalResolution);
	//
	// using (Graphics g = Graphics.FromImage(result))
	// {
	// g.InterpolationMode = InterpolationMode.HighQualityBicubic;
	// g.CompositingQuality = CompositingQuality.HighQuality;
	// g.SmoothingMode = SmoothingMode.HighQuality;
	// g.PixelOffsetMode = PixelOffsetMode.HighQuality;
	//
	// g.DrawImage(image, 0, 0, result.Width, result.Height);
	// }
	// return result;
	// }

	public void RefreshSurface(int nWidth, int nHeight) {
		if (nWidth == 0)
			nWidth = 384;
		if (nHeight == 0)
			nHeight = 24;

		if (mbitmap != null)
			mbitmap.recycle();

		mbitmap = Bitmap.createBitmap(nWidth, nHeight, Bitmap.Config.ARGB_8888);
		mcanvas = new Canvas(mbitmap);
		mcanvas.drawRGB(0xFF, 0xFF, 0xFF); // Backgroud with white
	}

	public void Erase() {
		mcanvas.drawRGB(0xFF, 0xFF, 0xFF); // Backgroud with white
	}

	public void SetTypeFace(Typeface face) {
		paint.setTypeface(face);
	}

	public void SetTextSize(int size) {
		paint.setTextSize(size);
	}

	public void SetText(String txt, int nStart, Paint.Align aln) {
		paint.setTextAlign(aln);
		mcanvas.drawText(txt, nStart, mbaseline, paint);
	}

	public void BottomText(String txt) {
		 TextPaint paint = new TextPaint();
		 Typeface font = Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD);
		 paint.setTypeface(font);
		 paint.setTextSize(20);
		 paint.setAntiAlias(true);

		// Rect textBounds = new Rect();
		// paint.getTextBounds( txt, 0, txt.length(), textBounds );

		Bitmap mybitmap = Bitmap.createBitmap(384, 24*5,
		Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(mybitmap);
		c.drawRGB(0xFF, 0xFF, 0xFF); // Backgroud with white

		// int x = (c.getWidth() - (textBounds.right-textBounds.left)) / 2;
		// int y = (c.getHeight() - (textBounds.bottom-textBounds.top)) / 2;
		// c.drawText( txt, 0, 24 , paint );

		StaticLayout sl = new StaticLayout(txt, paint,
				mybitmap.getWidth(), Layout.Alignment.ALIGN_NORMAL, 1, 0, false);
		sl.draw(c);

		// paint.setStyle(Paint.Style.FILL);
		// paint.setColor(Color.RED);
		// paint.setTypeface(Typeface.MONOSPACE);

		mybitmap.getWidth();
		mybitmap.getHeight();

		print_image(mybitmap);
		// print_bitmap(mybitmap);
	}

	public void print() {
		print_image(mbitmap);
		//Erase();
		RefreshSurface(0, 0);
	}

	public void draw_line() {
		draw_line(0, 0, mcanvas.getWidth());
	}

	public void draw_underline() {
		draw_line(0, mcanvas.getHeight() - 2, mcanvas.getWidth());
	}

	public void draw_line(float x, float y, float lenght) {
		Paint np = new Paint();
		np.setStyle(Paint.Style.STROKE);
		np.setStrokeWidth(2);
		np.setColor(Color.BLACK);
		mcanvas.drawLine(x, y, x + lenght - 1, y, np);
//		Log.d(TAG, "width: " + lenght);
	}

	public void print_image(Bitmap bmp) {
		int datawidth = bmp.getWidth();
		byte[] width = new byte[] { (byte) datawidth, (byte) (datawidth >> 8) };
		convertBitmap(bmp);

		// mService.write(PrinterCommands.SET_LINE_SPACING_24);
		mService.PrinterInit();
		mService.LineSpace(30);

		int offset = 0;
		while (offset < bmp.getHeight()) {
			mService.SetBitImageMode(); // .write(PrinterCommands.SELECT_BIT_IMAGE_MODE);
			mService.write(width);
			for (int x = 0; x < bmp.getWidth(); ++x) {

				for (int k = 0; k < 3; ++k) {

					byte slice = 0;
					for (int b = 0; b < 8; ++b) {
						int y = (((offset / 8) + k) * 8) + b;
						int i = (y * bmp.getWidth()) + x;
						boolean v = false;
						if (i < dots.length()) {
							v = dots.get(i);
						}
						slice |= (byte) ((v ? 1 : 0) << (7 - b));
					}
					mService.write(slice);
				}
			}
			offset += 24;
			// mService.write(PrinterCommands.FEED_LINE);
			// mService.write(PrinterCommands.FEED_LINE);
			// mService.write(PrinterCommands.FEED_LINE);
			// mService.write(PrinterCommands.FEED_LINE);
			// mService.write(PrinterCommands.FEED_LINE);
			// mService.write(PrinterCommands.FEED_LINE);
			// mService.LineFeed(6);
			mService.write(new byte[] { 10 });
		}
		// mService.write(PrinterCommands.SET_LINE_SPACING_30);
		//mService.LineSpace(30);
	}

	public String convertBitmap(Bitmap inputBitmap) {

		int mWidth = inputBitmap.getWidth();
		int mHeight = inputBitmap.getHeight();

		convertArgbToGrayscale(inputBitmap, mWidth, mHeight);
		String mStatus = "ok";
		return mStatus;

	}

	private void convertArgbToGrayscale(Bitmap bmpOriginal, int width,
			int height) {
		int pixel;
		int k = 0;
		int B = 0, G = 0, R = 0;
		dots = new BitSet(width * height);
		try {

			for (int x = 0; x < height; x++) {
				for (int y = 0; y < width; y++) {
					// get one pixel color
					pixel = bmpOriginal.getPixel(y, x);

					// retrieve color of all channels
					R = Color.red(pixel);
					G = Color.green(pixel);
					B = Color.blue(pixel);
					// take conversion up to one single value by calculating
					// pixel intensity.
					R = G = B = (int) (0.299 * R + 0.587 * G + 0.114 * B);
					// set bit into bitset, by calculating the pixel's luma
					if (R < 55) {
						dots.set(k);// this is the bitset that i'm printing
					}
					k++;
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			Log.e(TAG, e.toString());
		}
	}

	public void print_bitmap(Bitmap bm) {
		BitmapData data = GetBitmapData(bm);
		BitSet dots = data.Dots;
		byte[] width = new byte[] { (byte) data.Width, (byte) (data.Width >> 8) }; // BitConverter.GetBytes(data.Width);

		int offset = 0;
		// MemoryStream stream = new MemoryStream();
		// BinaryWriter bw = new BinaryWriter(stream);

		mService.write((char) 0x1B);
		mService.write('@');

		mService.write((char) 0x1B);
		mService.write('3');
		mService.write((byte) 24);

		while (offset < data.Height) {
			mService.write((char) 0x1B);
			mService.write('*'); // bit-image mode
			mService.write((byte) 33); // 24-dot double-density
			mService.write(width[0]); // width low byte
			mService.write(width[1]); // width high byte

			for (int x = 0; x < data.Width; ++x) {
				for (int k = 0; k < 3; ++k) {
					byte slice = 0;
					for (int b = 0; b < 8; ++b) {
						int y = (((offset / 8) + k) * 8) + b;
						// Calculate the location of the pixel we want in the
						// bit array.
						// It'll be at (y * width) + x.
						int i = (y * data.Width) + x;

						// If the image is shorter than 24 dots, pad with zero.
						boolean v = false;
						if (i < dots.length()) {
							v = dots.get(i);
						}
						slice |= (byte) ((v ? 1 : 0) << (7 - b));
					}

					mService.write(slice);
				}
			}
			offset += 24;
			mService.write((char) 0x0A);
		}
		// Restore the line spacing to the default of 30 dots.
		mService.write((char) 0x1B);
		mService.write('3');
		mService.write((byte) 30);

		// mService.Flush();
	}

	public BitmapData GetBitmapData(Bitmap bitmap) {
		int threshold = 127;
		int index = 0;
		double multiplier = 570; // this depends on your printer model. for
									// Beiyang you should use 1000
		double scale = (double) (multiplier / (double) bitmap.getWidth());
		int xheight = (int) (bitmap.getHeight() * scale);
		int xwidth = (int) (bitmap.getWidth() * scale);
		int dimensions = xwidth * xheight;
		BitSet dots = new BitSet(dimensions);

		for (int y = 0; y < xheight; y++) {
			for (int x = 0; x < xwidth; x++) {
				int _x = (int) (x / scale);
				int _y = (int) (y / scale);
				int color = bitmap.getPixel(_x, _y);
				double luminance = (int) (Color.red(color) * 0.3
						+ Color.green(color) * 0.59 + Color.blue(color) * 0.11);
				dots.set(index, luminance < threshold);
				index++;
			}
		}

		BitmapData bm = new BitmapData();
		bm.Dots = dots;
		bm.Height = (int) (bitmap.getHeight() * scale);
		bm.Width = (int) (bitmap.getWidth() * scale);

		return bm;
	}

	public class BitmapData {
		public BitSet Dots;
		public int Height;
		public int Width;
	}
}
