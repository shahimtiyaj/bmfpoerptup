package net.qsoft.brac.bmfpoerptup.report;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.ODRListAdapter;
import net.qsoft.brac.bmfpoerptup.R;
import net.qsoft.brac.bmfpoerptup.SSActivity;
import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.data.VO;

import java.util.ArrayList;
import java.util.HashMap;

@SuppressLint("Registered")
public class CSMissedBorrowerActivity extends SSActivity {
    private static final String TAG = CSMissedBorrowerActivity.class.getSimpleName();

    ListView lv = null;
    ArrayList<HashMap<String, String>> items = null;
    String vono = null;
    Button cmdOK;
    TextView branchName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_current_schedul_missed_borrower);

        cmdOK = (Button) findViewById(R.id.okButton);
        cmdOK.setVisibility(View.GONE);
        branchName = (TextView) findViewById(R.id.textBranchName);
        lv = (ListView) findViewById(R.id.listViewCSMissedBorrower);

    }

    private void createODReportList(String von) {
        DAO da = new DAO(this);
        da.open();
        PO po = da.getPO();
        VO vo = da.getVO(von);
        items = da.getOverDueByVO(von);
        da.close();

        branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());

        ODRListAdapter adapter = new ODRListAdapter(this, R.layout.ordreport_row, items);
        Log.d(TAG, "Array list addapter: " + adapter);
        lv.setAdapter(adapter);
    }


    public void onCancel(View view) {
        finish();
    }
}
