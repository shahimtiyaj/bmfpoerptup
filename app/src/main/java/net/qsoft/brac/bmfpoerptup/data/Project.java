package net.qsoft.brac.bmfpoerptup.data;

public class Project {
	String _ProjectCode;
	String _ProjectName;
	String _OrgCategory;
	Integer _Status;

	/**
	 * @param _ProjectCode
	 * @param _ProjectName
	 * @param _OrgCategory
	 * @param _Status
	 */
	public Project(String _ProjectCode, String _ProjectName,
			String _OrgCategory, Integer _Status) {
		super();
		this._ProjectCode = _ProjectCode;
		this._ProjectName = _ProjectName;
		this._OrgCategory = _OrgCategory;
		this._Status = _Status;
	}

	/**
	 * @return the _ProjectCode
	 */
	public final String get_ProjectCode() {
		return _ProjectCode;
	}
	/**
	 * @return the _ProjectName
	 */
	public final String get_ProjectName() {
		return _ProjectName;
	}
	/**
	 * @return the _OrgCategory
	 */
	public final String get_OrgCategory() {
		return _OrgCategory;
	}
	/**
	 * @return the _Status
	 */
	public final Integer get_Status() {
		return _Status;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return get_ProjectCode() + " - " + get_ProjectName();
	}
	

}
