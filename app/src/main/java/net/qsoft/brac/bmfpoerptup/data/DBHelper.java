package net.qsoft.brac.bmfpoerptup.data;


import android.content.Context;
import android.content.res.AssetManager;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class DBHelper extends SQLiteOpenHelper {
    private static final String TAG = SQLiteOpenHelper.class.getSimpleName();

    final static int DB_VERSION = 3;
    final static String DB_NAME = "bmfpo.s3db";
    final static String TBL_COLL = "coll";
    final static String TBL_CLIENT = "client";
    final static String TBL_CLTRANS = "cltrans";

    final static String TBL_BRANCH = "branch";
    final static String TBL_PROJECTS = "Projects";
    final static String TBL_LOAN_PRODUCTS = "LoanProducts";
    public final static String TBL_PO = "POList";
    public final static String TBL_VOLIST = "VOList";
    public final static String TBL_CMEMBERS = "CMembers";
    public final static String TBL_CLOANS = "CLoans";
    final static String TBL_LOANSTATUS = "LoanStatus";
    final static String TBL_WPTRANSACT = "WPTransact";
    public final static String TBL_GOOD_LOANS = "GoodLoans";
    final static String TBL_TRANSACT = "Transact";
    public final static String TBL_TRANS_TRAIL = "TransTrail";
    final static String TBL_SURVEYSL = "SurveySL";
    final static String TBL_SLSVOLIST = "SLSVOList";
    final static String TBL_SLSMEMLIST = "SLSMemList";
    public final static String TBL_TRX_MAPPING = "TrxMapping";


    // TBL_POList-----------------------------------------------------------------------------------
    private final static String FLD_POLIST_CO_NO = "CONo";// Means Pin number
    private final static String FLD_POLIST_CO_NAME = "COName";
    private final static String FLD_POLIST_SESSION_NO = "SessionNo";
    private final static String FLD_POLIST_OPEN_DATE = "OpenDate";
    private final static String FLD_POLIST_OPENING_BALANCE = "OpeningBal";
    private final static String FLD_POLIST_PASSWORD = "Password";//
    private final static String FLD_POLIST_E_METHOD = "EMethod";
    private final static String FLD_POLIST_CASH_IN_HAND = "CashInHand";
    private final static String FLD_POLIST_ENTERED_BY = "EnteredBy";//
    private final static String FLD_POLIST_DEVICE_ID = "DeviceId";
    private final static String FLD_POLIST_BRANCH_OPENING_STATUS = "BOStatus";
    private final static String FLD_POLIST_BRANCH_CODE = "BranchCode";
    private final static String FLD_POLIST_BRANCH_NAME = "BranchName";//
    private final static String FLD_POLIST_PROJECT_CODE = "ProjectCode";
    private final static String FLD_POLIST_DESIG = "Desig";
    private final static String FLD_POLIST_LAST_SYNC_TIME = "Time";

    // TBL_PROJECTS field names
    final static String FLD_PROJECT_CODE = "[ProjectCode]";
    final static String FLD_PROJECT_NAME = "[ProjectName]";

    // TBL_LOAN_PRODUCTS field names
    final static String FLD_PRODUCT_NO = "[ProductNo]";
    final static String FLD_PRODUCT_NAME = "[ProductName]";
    final static String FLD_INTEREST_RATE = "[IntRate]";
    final static String FLD_INTEREST_CALC_METHOD = "[IntCalcMethod]";
    final static String FLD_INT_RATE_INTERVAL_DAYS = "[IntRateIntrvlDays]";
    final static String FLD_LOAN_INT_CALC_FACTOR = "[IntrFactorLoan]";

    // TBL_VOLIST field names-----------------------------------------------------------------------
    final public static String FLD_ORG_NO = "[OrgNo]";
    final public static String FLD_ORG_NAME = "[OrgName]";
    final public static String FLD_ORG_CATEGORY = "[OrgCategory]";
    final public static String FLD_MEM_SEX_ID = "[MemSexId]";
    final public static String FLD_SAV_COLC_OPTION = "[SavColcOption]";
    final public static String FLD_LOAN_COLC_OPTION = "[LoanColcOption]";
    final public static String FLD_SAV_COLC_DATE = "[SavColcDate]";
    final public static String FLD_LOAN_COLC__DATE = "[LoanColcDate]";
    final public static String FLD_TARGET_DATE = "[TargetDate]";
    final public static String FLD_CO_NO = "[CONo]";
    final public static String FLD_BRANCH_CODE = "[BranchCode]";

    // TBL_CMEMBERS field names
    final public static String FLD_ORG_MEM_NO = "[OrgMemNo]";
    final public static String FLD_ORG_MEM_NAME = "[MemberName]";
    final public static String FLD_TARGET_SAVINGS_AMT = "[TargetAmtSav]";
    final public static String FLD_ORG_MEM_PHONE = "[PhoneNo]";
    final public static String FLD_SMS_SENT_STATUS = "[SMSStatus]";

    // TBL_CLOANS field names
    public final static String FLD_LOAN_NO = "[LoanNo]";
    public final static String FLD_LOAN_SL_NO = "[LoanSlNo]";
    public final static String FLD_PRINCIPAL_AMT = "[PrincipalAmt]";
    public final static String FLD_LOAN_INSTL_AMT = "[InstlAmtLoan]";
    public final static String FLD_DISBURSEMENT_DATE = "[DisbDate]";
    final static String FLD_LOAN_STATUS = "[LnStatus]";
    final static String FLD_PRINCIAL_DUE = "[PrincipalDue]";
    final static String FLD_INTEREST_DUE = "[InterestDue]";
    final static String FLD_TOTAL_DUE = "[TotalDue]";
    public final static String FLD_TARGET_LOAN_AMT = "[TargetAmtLoan]";
    final static String FLD_TOTAL_REALISED_AMT = "[TotalReld]";
    final static String FLD_OVERDUE_AMT = "[Overdue]";
    final static String FLD_BUFFER_INT_AMT = "[BufferIntrAmt]";
    final static String FLD_RECEIVED_AMT = "[ReceAmt]";
    final static String FLD_TARGET_AMT_LOAN_DUE = "[TALB]";
    final static String FLD_OVERDUE_BAL = "[ODB]";

    // TBL_LOANSTATUS
    public final static String FLD_LOAN_STATUS_NAME = "[StName]";
    // TBL_TRASACT
    final static String FLD_COLC_ID = "[ColcID]";
    public final static String FLD_COLC_AMT = "[ColcAmt]";
    public final static String FLD_COLC_DATE = "[ColcDate]";
    public final static String FLD_COLC_FOR = "[ColcFor]";

    // TBL_TRANS_TRAIL
    final static String FLD_TRAN_AMT = "[Tranamount]";
    final static String FLD_TRANS_NO = "[Transno]";

    Context context;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        // Store the context for later use
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // TODO Auto-generated method stub
        try {
            executeSQLScript(db, "create.sql");
        } catch (SQLException e) {
        } catch (IOException e) {
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        try {
            if (newVersion > oldVersion) {
                switch (oldVersion) {
                    case 1:
    		            executeSQLScript(db, "update_v2.sql");
                    case 2:
                        executeSQLScript(db, "update_v3.sql");
                }
            }
        } catch (SQLException e) {
        } catch (IOException e) {
        }
    }

    private void executeSQLScript(SQLiteDatabase database, String dbname) throws IOException, SQLException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte buf[] = new byte[1024];
        int len;
        AssetManager assetManager = context.getAssets();
        InputStream inputStream = null;

        try {
            inputStream = assetManager.open(dbname);
            while ((len = inputStream.read(buf)) != -1) {
                outputStream.write(buf, 0, len);
            }
            outputStream.close();
            inputStream.close();

            String[] createScript = outputStream.toString().split(";");
            for (int i = 0; i < createScript.length; i++) {
                String sqlStatement = createScript[i].trim();
                // TODO You may want to parse out comments here
                if (sqlStatement.length() > 0) {
                    try {
                        database.execSQL(sqlStatement + ";");
                    } catch (SQLException se) {
                        Log.e(TAG, se.toString(), se);
                    }
                }
            }
        } catch (IOException e) {
            // TODO Handle Script Failed to Load
            Log.e(TAG, e.toString(), e);
            throw e;
        } catch (SQLException e) {
            // TODO Handle Script Failed to Execute
            Log.e(TAG, e.toString(), e);
            throw e;
        }
    }

}
