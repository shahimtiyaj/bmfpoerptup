package net.qsoft.brac.bmfpoerptup;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.PO;

import java.util.ArrayList;
import java.util.HashMap;

public class LRTAReportActivity extends SSActivity {
	private static final String TAG = LRTAReportActivity.class.getSimpleName();

	ListView lv = null;
	ArrayList<HashMap<String, String>> items=null;
	String vono=null;
	Button cmdOK;
	TextView branchName;
	TextView voName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_lrtareport);

		cmdOK = (Button) findViewById(R.id.okButton);
		cmdOK.setVisibility(View.GONE);
		branchName = (TextView) findViewById(R.id.textBranchName);

		lv = (ListView) findViewById(R.id.listViewLRTA);

		if(VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		createLRTAReportList(vono);

	}

	private void createLRTAReportList(String von) {
//		Log.d(TAG, "VO No.: " + (von==null ? "": von));
		DAO da = new DAO(this);
		da.open();
		PO po = da.getPO();
		items = da.getLRTAReport();
//		Log.d(TAG, "Items: " + items );
		da.close();

		branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());

		LRTARListAdapter adapter = new LRTARListAdapter(this, R.layout.ordreport_row, items);
		Log.d(TAG, "Array list addapter: " + adapter);
		lv.setAdapter(adapter);
	}

	public void onCancel(View view) {
		// Back
		finish();
	}	
}
