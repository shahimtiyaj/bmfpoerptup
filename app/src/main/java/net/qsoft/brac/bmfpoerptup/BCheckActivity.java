package net.qsoft.brac.bmfpoerptup;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.CLoan;

import java.util.Calendar;

public class BCheckActivity extends SSActivity {
	private static final String TAG = BCheckActivity.class.getSimpleName();

	int intOption;
	int intBalance;
	String strLToDate;

	Button signButton;
	EditText editBalance;

	Button futureButton;
	TextView balLabel;

	TextView LoanInfo;

	Button okButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bcheck);

		TextView dispLabel = (TextView) findViewById(R.id.dispLabel);
		TextView captionLabel = (TextView) findViewById(R.id.captionLabel);
		balLabel = (TextView) findViewById(R.id.balanceLabel);
		editBalance = (EditText) findViewById(R.id.editBalance);
		signButton = (Button) findViewById(R.id.signButton);
		futureButton = (Button) findViewById(R.id.futureButton);
		LoanInfo = (TextView) findViewById(R.id.textLoanInfo);
		okButton = (Button) findViewById(R.id.okButton);

		intOption = getIntent().getIntExtra(P8.TRANS_OPTION, 0);
		intBalance = getIntent().getIntExtra(P8.BCHECK_BALANCE, 0);
		Log.d(TAG, "Balance: " + Integer.toString(intBalance));
		switch(intOption) {
			case P8.TRANS_OPTION_SAVINGS:
				// Savings balance check
				setTitle(getString(R.string.title_bcheck_verify_savings_balance));
				dispLabel.setText("");
				editBalance.setHint(R.string.prompt_enter_savings_balance);
				LoanInfo.setVisibility(View.GONE);
				futureButton.setVisibility(View.GONE);
				balLabel.setVisibility(View.GONE);
				break;

			case P8.TRANS_OPTION_REPAYMENT:
				// Loan repayment - total realized balance check
				setTitle(getString(R.string.title_bcheck_verify_loan_repay_balance));
				dispLabel.setVisibility(View.GONE);
				editBalance.setHint(R.string.prompt_enter_loan_realized_balance);
				LoanInfo.setVisibility(View.VISIBLE);
				futureButton.setVisibility(View.VISIBLE);
				balLabel.setVisibility(View.VISIBLE);

				CLoan ln = CLoan.get_lastCL();

				LoanInfo.setText(String.format("Disbursed:  %1$tB %1$td, %1$tY\nAmount: %2$d\nLoan Serial No.: %3$d",
						ln.get_DisbDate(), ln.get_PrincipalAmt(), ln.get_LoanSlNo()));

				if((P8.daysBetweenDates(ln.get_DisbDate(), P8.ToDay()) + 1) <= 15) {
					okButton.setVisibility(View.GONE);
					editBalance.setFocusable(false);
					editBalance.setEnabled(false);
					editBalance.setHintTextColor(android.graphics.Color.rgb(0xFF, 0, 0));
					editBalance.setHint("LOAN IN GRACE PERIOD");
				}
				break;
		}
	}

	public void onSignClick(View view) {
        if (signButton.getText().toString().equals("+"))
            signButton.setText("-");
        else if (signButton.getText().toString().equals("-"))
            signButton.setText("+");
	}

	public void calcBalance(View view) {
		showDialog(999);
	}

	@Override
    protected Dialog onCreateDialog(int id) {
	      // TODO Auto-generated method stub
	      if (id == 999) {

	    	  Calendar calendar = Calendar.getInstance();
	          int year = calendar.get(Calendar.YEAR);

	          int month = calendar.get(Calendar.MONTH);
	          int day = calendar.get(Calendar.DAY_OF_MONTH);

	         return new DatePickerDialog(this, myDateListener, year, month, day);
	      }
	      return null;
	}

	private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
	      @Override
	      public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
	         // TODO Auto-generated method stub
	         // arg1 = year
	         // arg2 = month
	         // arg3 = day
	    	  CLoan cl = CLoan.get_lastCL();
	    	  Calendar c = Calendar.getInstance();
	    	  c.set(arg1, arg2, arg3, 0, 0, 0);
	    	  Integer bal = cl.get_LastLB(c.getTime());

	    	  balLabel.setText("Loan balance on " + P8.FormatDate((c.getTime()), "MMM dd, yyyy") + ": " +  bal.toString());
	      }
	};

	public void onOk(View view) {
		if(P8.IsValidAmount(this, editBalance)) {
            int amtBalance = Integer.parseInt(editBalance.getText().toString());
            if (signButton.getText().toString().equals("-"))
            	amtBalance=-amtBalance;
            if(intBalance==amtBalance) {
            	// Balance matched, start transaction activity
            	Intent ri;
            	if(intOption== P8.TRANS_OPTION_SAVINGS)
            		ri = new Intent(this, GSaveActivity.class);
            	else
            		ri = new Intent(this, GRepayActivity.class);

        		startActivity(ri);
        		finish();
            }
            else {
            	// Balance not matched
            	P8.ErrorSound(this);
            	editBalance.setError(getString(R.string.error_bcheck_balance_mismatch));
            	editBalance.requestFocus();
            }
		}
	}
	
	public void onCancel(View view) {
		finish();
	}
}
