package net.qsoft.brac.bmfpoerptup.util;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Base64InputStream;
import android.util.Log;

import net.qsoft.brac.bmfpoerptup.App;
import net.qsoft.brac.bmfpoerptup.P8;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;

public class FileUtils {

	public FileUtils() {
		// TODO Auto-generated constructor stub
	}

	 /**
     * Creates the specified <code>toFile</code> as a byte for byte copy of the
     * <code>fromFile</code>. If <code>toFile</code> already exists, then it
     * will be replaced with a copy of <code>fromFile</code>. The name and path
     * of <code>toFile</code> will be that of <code>toFile</code>.<br/>
     * <br/>
     * <i> Note: <code>fromFile</code> and <code>toFile</code> will be closed by
     * this function.</i>
     * 
     * @param fromFile
     *            - FileInputStream for the file to copy from.
     * @param toFile
     *            - FileInputStream for the file to copy to.
     */
    public static void copyFile(FileInputStream fromFile, FileOutputStream toFile) throws IOException {
        FileChannel fromChannel = null;
        FileChannel toChannel = null;
        try {
            fromChannel = fromFile.getChannel();
            toChannel = toFile.getChannel();
            fromChannel.transferTo(0, fromChannel.size(), toChannel);
        } finally {
            try {
                if (fromChannel != null) {
                    fromChannel.close();
                }
            } finally {
                if (toChannel != null) {
                    toChannel.close();
                }
            }
        }
    }

    public static boolean copyBase64ToFile(String fileName, String outFileName)
    {
        FileOutputStream fos = null;

        try {
//            mTempFile = File.createTempFile("decoded", "mediadata");
            File outputFile = new File(outFileName); //File(outFileName, filename);
            fos = new FileOutputStream(outputFile);
            InputStream stream = new FileInputStream((fileName));
            Base64InputStream decoder = new Base64InputStream(stream, Base64.NO_WRAP);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = decoder.read(buffer)) != -1) {
                fos.write(buffer, 0, len);
            }
            decoder.close();
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            try {
                if (fos != null) fos.close();
            } catch (IOException e) {
                // Can't do anything.
            }
        }
    }

    public static boolean UpdateSelf(String fileName) {
        boolean ret = false;
        File dd = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String x = P8.PathCombine(dd.getAbsolutePath(), "BMFPO.apk");

        if(copyBase64ToFile(fileName, x)) {
            Intent i = new Intent();
            i.setAction(Intent.ACTION_VIEW);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.setDataAndType(Uri.fromFile(new File(x)), "application/vnd.android.package-archive" );
            Log.d("Lofting", "About to install new .apk");
            App.getContext().startActivity(i);
            ret=true;
        }

        return ret;
    }
}
