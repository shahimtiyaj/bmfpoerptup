package net.qsoft.brac.bmfpoerptup.data;

import net.qsoft.brac.bmfpoerptup.App;
import net.qsoft.brac.bmfpoerptup.P8;

import java.util.Date;

public class CLoan {
	private static final String TAG = CLoan.class.getSimpleName();
	private static CLoan _lastCL = null;

	public static CLoan get_lastCL() {
		return _lastCL;
	}
	public static void set_lastCL(CLoan _lastCL) {
		CLoan._lastCL = _lastCL;
	}
	
	String _OrgNo;
	String _OrgMemNo;
	String _ProjectCode;
	Integer _LoanNo;
	Integer _LoanSlNo;
	String _ProductNo;
	Double _IntrFactorLoan;
	Integer _PrincipalAmt;
	String _SchmCode;
	Integer _InstlAmtLoan;
	Date _DisbDate;
	Integer _LnStatus;
	Integer _PrincipalDue;
	Integer _InterestDue;		
	Integer _TotalDue;
	Integer _TargetAmtLoan;
	Integer _TotalReld;
	Integer _Overdue;
	Double _BufferIntrAmt;
	Integer _ReceAmt;
	Integer _IAB;
	Integer _ODB;
	Integer _TRB;
	Integer _LB;
	Integer _PDB;
	Integer _IDB;
	Integer _InstlPassed;
	Double _OldInterestDue;
	String _ProductSymbol;
	Integer _LoanRealized;
	

	/**
	 * @param _OrgNo
	 * @param _OrgMemNo
	 * @param _ProjectCode
	 * @param _LoanNo
	 * @param _LoanSlNo
	 * @param _ProductNo
	 * @param _IntrFactorLoan
	 * @param _PrincipalAmt
	 * @param _SchmCode
	 * @param _InstlAmtLoan
	 * @param _DisbDate
	 * @param _LnStatus
	 * @param _PrincipalDue
	 * @param _InterestDue
	 * @param _TotalDue
	 * @param _TargetAmtLoan
	 * @param _TotalReld
	 * @param _Overdue
	 * @param _BufferIntrAmt
	 * @param _ReceAmt
	 * @param _IAB
	 * @param _ODB
	 * @param _TRB
	 * @param _LB
	 * @param _PDB
	 * @param _IDB
	 */
	public CLoan(String _OrgNo, String _OrgMemNo, String _ProjectCode,
			Integer _LoanNo, Integer _LoanSlNo, String _ProductNo,
			Double _IntrFactorLoan, Integer _PrincipalAmt, String _SchmCode,
			Integer _InstlAmtLoan, Date _DisbDate, Integer _LnStatus,
			Integer _PrincipalDue, Integer _InterestDue, Integer _TotalDue,
			Integer _TargetAmtLoan, Integer _TotalReld, Integer _Overdue,
			Double _BufferIntrAmt, Integer _ReceAmt, Integer _IAB,
			Integer _ODB, Integer _TRB, Integer _LB, Integer _PDB, Integer _IDB,
			Integer _InstlPassed, Double _OldInterestDue, String _ProductSymbol, Integer _LoanRealized) {
		super();
		this._OrgNo = _OrgNo;
		this._OrgMemNo = _OrgMemNo;
		this._ProjectCode = _ProjectCode;
		this._LoanNo = _LoanNo;
		this._LoanSlNo = _LoanSlNo;
		this._ProductNo = _ProductNo;
		this._IntrFactorLoan = _IntrFactorLoan;
		this._PrincipalAmt = _PrincipalAmt;
		this._SchmCode = _SchmCode;
		this._InstlAmtLoan = _InstlAmtLoan;
		this._DisbDate = _DisbDate;
		this._LnStatus = _LnStatus;
		this._PrincipalDue = _PrincipalDue;
		this._InterestDue = _InterestDue;
		this._TotalDue = _TotalDue;
		this._TargetAmtLoan = _TargetAmtLoan;
		this._TotalReld = _TotalReld;
		this._Overdue = _Overdue;
		this._BufferIntrAmt = _BufferIntrAmt;
		this._ReceAmt = _ReceAmt;
		this._IAB = _IAB;
		this._ODB = _ODB;
		this._TRB = _TRB;
		this._LB = _LB;
		this._PDB = _PDB;
		this._IDB = _IDB;
		this._InstlPassed=_InstlPassed;
		this._OldInterestDue=_OldInterestDue;
		this._ProductSymbol=_ProductSymbol;
		this._LoanRealized=_LoanRealized;
		
	}

	/**
	 * @return the _InstlPassed
	 */
	public final Integer get_InstlPassed() {
		return _InstlPassed;
	}
	/**
	 * @return the _OldInterestDue
	 */
	public final Double get_OldInterestDue() {
		return _OldInterestDue;
	}
	/**
	 * @return the _ProductSymbol
	 */
	public final String get_ProductSymbol() {
		return _ProductSymbol;
	}
	/**
	 * @return the _LoanRealized
	 */
	public final Integer get_LoanRealized() {
		return _LoanRealized;
	}

	/**
	 * @return the _OrgNo
	 */
	public final String get_OrgNo() {
		return _OrgNo;
	}
	/**
	 * @return the _OrgMemNo
	 */
	public final String get_OrgMemNo() {
		return _OrgMemNo;
	}
	/**
	 * @return the _ProjectCode
	 */
	public final String get_ProjectCode() {
		return _ProjectCode;
	}
	/**
	 * @return the _LoanNo
	 */
	public final Integer get_LoanNo() {
		return _LoanNo;
	}
	/**
	 * @return the _LoanSlNo
	 */
	public final Integer get_LoanSlNo() {
		return _LoanSlNo;
	}
	/**
	 * @return the _ProductNo
	 */
	public final String get_ProductNo() {
		return _ProductNo;
	}
	/**
	 * @return the _IntrFactorLoan
	 */
	public final Double get_IntrFactorLoan() {
		return _IntrFactorLoan;
	}
	/**
	 * @return the _PrincipalAmt
	 */
	public final Integer get_PrincipalAmt() {
		return _PrincipalAmt;
	}
	/**
	 * @return the _SchmCode
	 */
	public final String get_SchmCode() {
		return _SchmCode;
	}
	/**
	 * @return the _InstlAmtLoan
	 */
	public final Integer get_InstlAmtLoan() {
		return _InstlAmtLoan;
	}
	/**
	 * @return the _DisbDate
	 */
	public final Date get_DisbDate() {
		return _DisbDate;
	}
	/**
	 * @return the _LnStatus
	 */
	public final Integer get_LnStatus() {
		return _LnStatus;
	}
	/**
	 * @return the _PrincipalDue
	 */
	public final Integer get_PrincipalDue() {
		return _PrincipalDue;
	}
	/**
	 * @return the _InterestDue
	 */
	public final Integer get_InterestDue() {
		return _InterestDue;
	}
	/**
	 * @return the _TotalDue
	 */
	public final Integer get_TotalDue() {
		return _TotalDue;
	}
	/**
	 * @return the _TargetAmtLoan
	 */
	public final Integer get_TargetAmtLoan() {
		return _TargetAmtLoan;
	}
	/**
	 * @return the _TotalReld
	 */
	public final Integer get_TotalReld() {
		return _TotalReld;
	}
	/**
	 * @return the _Overdue
	 */
	public final Integer get_Overdue() {
		return _Overdue;
	}
	/**
	 * @return the _BufferIntrAmt
	 */
	public final Double get_BufferIntrAmt() {
		return _BufferIntrAmt;
	}
	/**
	 * @return the _ReceAmt
	 */
	public final Integer get_ReceAmt() {
		return _ReceAmt;
	}
	
	/**
	 * @return the _IAB
	 */
	public final Integer get_IAB() {
		return _IAB;
	}
	/**
	 * @return the _ODB
	 */
	public final Integer get_ODB() {
		return _ODB;
	}
	/**
	 * @return the _TRB
	 */
	public final Integer get_TRB() {
		return _TRB;
	}
	/**
	 * @return the _LB
	 */
	public final Integer get_LB() {
		return _LB;
	}
	
	public final Integer get_LastLB(Date currdt) {
		if (get_LnStatus() > DAO.LS_LATE2) 
			return get_LB();
		
		// Find branch open date
		
		//Date td = CMember.get_lastCM().get_ColcDate();
		Date td = App.getBranchOPenDate();
		int diff = P8.daysBetweenDates(td, currdt);
		double bufAmt = get_BufferIntrAmt();
//		Log.d(TAG, "Buffer Amt: " + Double.toString(bufAmt));
//		Log.d(TAG, "Factor    : " + String.format("%f10", get_IntrFactorLoan()));
		if(diff > 0) {
			bufAmt += (get_PrincipalDue() * get_IntrFactorLoan() * diff);
		}
//		Log.d(TAG, "Open Date: " + td.toString());
//		Log.d(TAG, "Curr Date: " + currdt.toString());
//		Log.d(TAG, "Date diff: " + Integer.toString(diff));
//		Log.d(TAG, "Amt diff : " + Double.toString((get_PrincipalDue() * get_IntrFactorLoan() * diff)));
//		Log.d(TAG, "Loan Bal: " + get_LB().toString());
//		Log.d(TAG, "PDB: " + get_PDB().toString());
//		Log.d(TAG, "IDB: " + get_IDB().toString());
		
		return get_PDB() + get_IDB() + ((int) bufAmt);
	}
	
	/**
	 * @return the _PDB
	 */
	public final Integer get_PDB() {
		return _PDB;
	}
	/**
	 * @return the _IDB
	 */
	public final Integer get_IDB() {
		return _IDB;
	}

	public final void Repay(Integer amt) {
		_ReceAmt += amt;
		_TRB += amt;
		_LB -= amt;
		
		// Handle overdue
		if(_LnStatus != DAO.LS_CURRENT) {
			_ODB -= amt;
			if((_IAB - amt) >= 0)
				_IAB-=amt;
			else
				_IAB=0;
		}
		else {
			if((_IAB - amt) >= 0)
				_IAB -= amt;
			else {
				_ODB -= (amt - _IAB); 
				_IAB=0;
			}
		}
		
		// Principal and Interest 
		if(amt >= _IDB) {
			_PDB -= (amt - _IDB);
			_IDB = 0;
		}
		else {
			_IDB -= amt;
		}
	}
}
