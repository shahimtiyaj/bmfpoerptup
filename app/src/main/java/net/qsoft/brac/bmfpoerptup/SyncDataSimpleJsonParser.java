package net.qsoft.brac.bmfpoerptup;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;

import net.qsoft.brac.bmfpoerptup.Volley.VolleyCustomRequest;
import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.util.CloudRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static android.os.Environment.DIRECTORY_DOWNLOADS;
import static com.android.volley.VolleyLog.d;
import static com.android.volley.VolleyLog.v;
import static net.qsoft.brac.bmfpoerptup.MainActivity.VolleyErrorResponse;
import static net.qsoft.brac.bmfpoerptup.P8.getDateTime;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_CLOANS;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_CMEMBERS;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_GOOD_LOANS;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_TRANS_TRAIL;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_TRX_MAPPING;
import static net.qsoft.brac.bmfpoerptup.data.DBHelper.TBL_VOLIST;

public class SyncDataSimpleJsonParser extends Activity {
    private static final String TAG = SyncDataSimpleJsonParser.class.getSimpleName();
    private Handler handler;
    private String req = null;
    private Context context;
    private DownloadManager downloadManager;
    private long refid;
    private long referenceId;
    private ArrayList<Long> list = new ArrayList<>();
    private HashMap<String, String> po;
    private HashMap<Integer, String> loantrxItems;
    private HashMap<Integer, String> saveTrxItems;
    private String CoNo = "", BranchCode = "", syncTime, vono, ServerColcFor;
    private int ProjectCode = 0;
    private ProgressDialog progressDialog;
    private Button retry;
    //private Date ColcDate;
    private String ColcDate;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sync_data_layout);
        initializeViews();
    }

    //JSON Post Request--------------------------------------------
    public void GetDataFromServer() {
        final DAO da = new DAO(getApplicationContext());
        da.open();
        syncTime = getDateTime();
        Log.d("SyncTime:", "SyncTime: " + syncTime);

        String hitURL = net.qsoft.brac.bmfpoerptup.App.getSyncUrl();
        HashMap<String, String> params = new HashMap<>();
        try {
            req = da.PrepareDataForSending().toString();
            params.put("req", req);

        } catch (IOException e) {
            e.printStackTrace();
        }

        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, null,
                new Response.Listener<org.json.JSONObject>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(org.json.JSONObject response) {
                        Log.d(TAG, "Server Response: " + response.toString());
                        DAO da = new DAO(getApplicationContext());
                        da.open();

                        try {
                            v("Response:%n %s", response.toString(4));
                            String status = response.getString("status");
                            Log.d("Message", status);

                            switch (status) {
                                case "A":
                                    try {
                                        JSONArray data1 = response.getJSONArray("data");
                                        org.json.JSONObject c = data1.getJSONObject(0);
                                        PO po = da.getPO();
                                        if (po.isPO()) {
                                            POExistsALert(c.getString("cono"), c.getString("coname"),
                                                    c.getString("branchcode"), c.getString("branchname"),
                                                    c.getString("opendate"), " ",
                                                    c.getString("projectcode"), c.getInt("SLno"));

                                        } else {
                                            da.AssignCollector(c.getString("cono"), c.getString("coname"),
                                                    c.getString("branchcode"), c.getString("branchname"),
                                                    c.getString("opendate"), " ",
                                                    c.getString("projectcode"), c.getInt("SLno"));
                                            Intent intent = new Intent(getApplicationContext(), MainActivity1.class);
                                            startActivity(intent);
                                        }

                                    } catch (Exception e) {
                                        Log.d(TAG, e.getMessage());
                                    }
                                    break;
                                case "PEND":
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Pending Data !", "Still data is pending in sync cloud.Please try later.");
                                    break;
                                case "IAA": //IAA= In appropriate application
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Inappropiate app", "This is not appropiate application");
                                    break;
                                case "CT": //CT==current time
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Time Mismatch!", "Please Update your Device time.");
                                    break;
                                case "VEN": //VEN==Vendor
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Vendor Server error ", "Unable to process request. Please contact with your vendor ");
                                    break;
                                case "HP":
                                    AlertDialog(R.drawable.ic_open_in_browser_black_24dp, "Pending Server Data !", "Still data is pending in server.Please try later.");
                                    break;

                                case "UL":
                                    try {
                                        da.ClearTransactions();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                case "D":
                                    DownloadJsonFile();
                                    break;
                                case "INA": //INA = In Active Device
                                    AlertDialog(R.drawable.ic_perm_device_information_black_24dp, "Deactivate Device !", "Your Device is Deactivate.Please Active your device first then try.");
                                    progressDialog.dismiss();
                                    break;
                                case "ASG": //ASG = Already Assign Device
                                    String message = response.getString("message");
                                    AlertDialog(R.drawable.ic_perm_device_information_black_24dp, "Already Assigned!", message);
                                    progressDialog.dismiss();
                                    break;
                                case "CL"://CL = Branch Closed
                                    AlertDialog(R.drawable.ic_do_not_disturb_off_black_24dp, "Branch Closed !", "The Branch you have requested to get data is closed at this moment, Please try again later.Thank you.");
                                    progressDialog.dismiss();
                                    break;
                                case "BCL"://CL = Branch not found
                                    AlertDialog(R.drawable.ic_do_not_disturb_off_black_24dp, "Branch Not Found!", "The Branch you have requested to get data is not found, Please try again later.Thank you.");
                                    progressDialog.dismiss();
                                    break;
                                case "F"://F = Device not found
                                    AlertDialog(R.drawable.ic_do_not_disturb_off_black_24dp, "Device Not Found!", "This Device is not found !!");
                                    progressDialog.dismiss();
                                    break;
                                default:
                                    AlertDialog(R.drawable.ic_perm_device_information_black_24dp, "Device Not Assign !", "Your Device is not Assign yet.Please Assign your device first then try.Thank You !");
                                    progressDialog.dismiss();
                                    break;
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            da.close();
                        }
                    }
                },

                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //progressDialog.dismiss();
                        volleyError.printStackTrace();
                        d(TAG, "Error: " + volleyError.getMessage());
                        P8.ShowError(SyncDataSimpleJsonParser.this, VolleyErrorResponse(volleyError));
                    }
                }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                int AppVersionCode = BuildConfig.VERSION_CODE;
                String AppVersionName = BuildConfig.VERSION_NAME;
                String AppId = BuildConfig.APPLICATION_ID.substring(15);

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("ProjectCode", String.valueOf(ProjectCode));
                headers.put("PIN", CoNo);
                headers.put("BranchCode", BranchCode);
                headers.put("Content-Type", "application/json");
                headers.put("ApiKey", "7f30f4491cb4435984616d1913e88389");
                headers.put("ImeiNo", net.qsoft.brac.bmfpoerptup.App.getDeviceId());
                headers.put("LastSyncTime", da.GetSyncTime());
                headers.put("CurrentTime", CurrentDateTime());
                headers.put("AppVersionCode", String.valueOf(AppVersionCode));
                headers.put("AppVersionName", AppVersionName);
                headers.put("AppId", AppId);
                headers.put("req", req);
                return headers;
            }
        };

        int socketTimeout = 1800000;//18 Minutes  - change to what you want// 18,0,0,000 miliseconds=18 minutes
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        postRequest.setRetryPolicy(policy);
        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
    }

    //JSON Post Request--------------------------------------------
    public void SendLastDownloadStatus() {
        String hitURL = App.getLastDownloadStatusURL();
        VolleyCustomRequest postRequest = new VolleyCustomRequest(Request.Method.POST, hitURL, null,
                new Response.Listener<org.json.JSONObject>() {
                    @Override
                    public void onResponse(org.json.JSONObject response) {
                        Log.d("response", "Size: " + response.length());
                        try {
                            v("Response:%n %s", response.toString(4));
                            String status = response.getString("message");
                            Log.d("Message", status);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        d(TAG, "Error: " + volleyError.getMessage());
                        P8.ShowError(SyncDataSimpleJsonParser.this, VolleyErrorResponse(volleyError));
                    }
                }) {

            /**
             * Passing some request headers
             */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                final int AppVersionCode = BuildConfig.VERSION_CODE;
                final String AppVersionName = BuildConfig.VERSION_NAME;
                final String AppId = BuildConfig.APPLICATION_ID.substring(15);
                final String DownloadStatus = "completed";

                DAO dao = new DAO(getApplicationContext());
                dao.open();
                dao.close();

                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("ProjectCode", String.valueOf(ProjectCode));
                headers.put("BranchCode", BranchCode);
                headers.put("ApiKey", "7f30f4491cb4435984616d1913e88389");
                headers.put("ImeiNo", net.qsoft.brac.bmfpoerptup.App.getDeviceId());
                headers.put("PIN", CoNo);
                headers.put("LastSyncTime", dao.GetSyncTime());
                headers.put("LastDownloadStatus", DownloadStatus);
                headers.put("CurrentTime", CurrentDateTime());
                headers.put("AppVersionCode", String.valueOf(AppVersionCode));
                headers.put("AppVersionName", AppVersionName);
                headers.put("AppId", AppId);

                return headers;
            }
        };
        // Add the request to the RequestQueue.
        CloudRequest.getInstance(this).addToRequestQueue(postRequest);
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ReadJSONFile() {
        DAO da = new DAO(getApplicationContext());
        da.open();
        unpackZip("/storage/emulated/0/Download/", CoNo + "-bmfpoerp.zip");
        org.json.simple.JSONObject jsonObject = null;
        JSONParser parser = new JSONParser();
        try {

            Object obj = parser.parse(new FileReader("/storage/emulated/0/Download/" + CoNo + "results.json"));

            jsonObject = (org.json.simple.JSONObject) obj;

            org.json.simple.JSONObject jsonObj1 = (org.json.simple.JSONObject) jsonObject.get("data");
            // Getting data JSON Array nodes Transtrail

            org.json.simple.JSONArray getVolist = (org.json.simple.JSONArray) jsonObj1.get("volist");

            for (int i = 0; i < getVolist.size(); i++) {

                org.json.simple.JSONObject c = (JSONObject) getVolist.get(i);

                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_VOLIST + "(OrgNo, OrgName, OrgCategory, MemSexID, " +
                                "SavColcOption, LoanColcOption, SavColcDate, LoanColcDate, CONo, TargetDate, StartDate, " +
                                "EndDate, NextTargetDate, Territory, UpdatedAt, ProjectCode, BranchCode) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.get("OrgNo").toString(),
                                c.get("OrgName").toString(),
                                "null",// c.getString("OrgCategory"),
                                c.get("MemSexId").toString(),
                                c.get("ColcOption").toString(),
                                c.get("ColcOption").toString(),
                                c.get("TargetDate").toString(),
                                c.get("TargetDate").toString(),
                                c.get("PIN").toString(),
                                c.get("TargetDate").toString(),
                                c.get("StartDate").toString(),
                                "null",//c.getString("EndDate"),
                                c.get("NextTargetDate").toString(),
                                "null",//c.getString("Territory"),
                                c.get("UpdatedAt").toString(),
                                c.get("ProjectCode").toString()
                                //BranchCode //c.getString("BranchCode")
                        });
                Log.d("VOLIST ", "Position: " + i + "Inserting...");
            }


            // Getting data JSON Array nodes CMEMBERS
            org.json.simple.JSONArray getCmembers = (org.json.simple.JSONArray) jsonObj1.get("cmembers");


            // looping through All nodes
            for (int i = 0; i < getCmembers.size(); i++) {

                try {
                    org.json.simple.JSONObject c = (JSONObject) getCmembers.get(i);

                    // ColcDate = P8.ConvertStringToDate(da.GetTargetDate(c.getString("OrgNo")));
                    ColcDate = da.GetTargetDate(c.get("OrgNo").toString());


                    DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_CMEMBERS + "(ProjectCode, OrgNo, OrgMemNo, " +
                                    "MemberName, SavBalan, SavPayable, CalcIntrAmt, TargetAmtSav, ReceAmt, " +
                                    "ColcDate, AdjAmt, SB, SIA, SavingsRealized, " +
                                    "MemSexID, NationalIDNo, PhoneNo, Walletno, AdmissionDate) " +
                                    "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                            new String[]{c.get("ProjectCode").toString(),
                                    c.get("OrgNo").toString(),
                                    c.get("OrgMemNo").toString(),
                                    c.get("MemberName").toString(),
                                    c.get("SavBalan").toString(),
                                    c.get("SavPayable").toString(),
                                    c.get("CalcIntrAmt").toString(),
                                    c.get("TargetAmtSav").toString(),
                                    c.get("ReceAmt").toString(),
                                    //P8.FormatDate(ColcDate, "yyyy-MM-dd"),
                                    ColcDate,
                                    "null", //c.getString("AdjAmt"),
                                    c.get("SavBalan").toString(),
                                    c.get("TargetAmtSav").toString(),
                                    "null", //c.getString("SavingsRealized"),
                                    c.get("MemSexId").toString(),

                                    String.valueOf(c.get("NationalId")),
                                    String.valueOf(c.get("ContactNo")),
                                    String.valueOf(c.get("BkashWalletNo")),

                                    c.get("ApplicationDate").toString()
                            });
                    Log.d("CMEMBERS ", "Position: " + i + "Inserting...");

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


            // Getting data JSON Array nodes CLOANS
            org.json.simple.JSONArray getCLoans = (org.json.simple.JSONArray) jsonObj1.get("cloans");


            // looping through All nodes
            for (int i = 0; i < getCLoans.size(); i++) {

                if (getCLoans.get(i) == null)
                    continue;

                org.json.simple.JSONObject c = (JSONObject) getCLoans.get(i);

                int LnSts = Integer.parseInt(c.get("LnStatus").toString());
                int LnStatus = LnSts - 1;
                // ColcDate = P8.ConvertStringToDate(da.GetTargetDate(c.getString("OrgNo")));
                ColcDate = da.GetTargetDate(c.get("OrgNo").toString());

                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_CLOANS + "(OrgNo, OrgMemNo, ProjectCode, LoanNo, LoanSlNo, " +
                                "ProductNo, IntrFactorLoan, PrincipalAmt, SchmCode, InstlAmtLoan, DisbDate, LnStatus, " +
                                "PrincipalDue, InterestDue, TotalDue, TargetAmtLoan, TotalReld, Overdue, BufferIntrAmt, " +
                                "ReceAmt, IAB, ODB, TRB, LB, PDB, IDB, " +
                                "InstlPassed, OldInterestDue, ProductSymbol, LoanRealized, ColcDate) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                                "?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.get("OrgNo").toString(),
                                c.get("OrgMemNo").toString(),
                                c.get("ProjectCode").toString(),
                                c.get("LoanNo").toString(),
                                c.get("LoanSlNo").toString(),
                                c.get("ProductNo").toString(),
                                c.get("IntrFactorLoan").toString(),
                                c.get("PrincipalAmount").toString(),
                                "null", //c.getString("SchmCode"),
                                c.get("InstlAmtLoan").toString(),//c.getString("InstallmentAmount"), //change parameter new name
                                c.get("DisbDate").toString(),
                                String.valueOf(LnStatus),
                                c.get("PrincipalDue").toString(),
                                c.get("InterestDue").toString(),
                                c.get("TotalDue").toString(),
                                c.get("TargetAmtLoan").toString(),
                                c.get("TotalReld").toString(),
                                c.get("Overdue").toString(),
                                c.get("BufferIntrAmt").toString(),
                                "null",//c.getString("ReceAmt"),
                                c.get("TargetAmtLoan").toString(),//IAB
                                c.get("Overdue").toString(),
                                c.get("TotalReld").toString(),
                                c.get("TotalDue").toString(),//LB=PrincipalDue+InterestDue
                                c.get("PrincipalDue").toString(),
                                c.get("InterestDue").toString(),
                                c.get("InstlPassed").toString(),
                                "0", //c.getString("OldInterestDue"),
                                c.get("ProductSymbol").toString(),
                                "null",// c.getString("LoanRealized"),
                                //P8.FormatDate(ColcDate, "yyyy-MM-dd")
                                ColcDate
                        });
                Log.d("CLOANS ", "Position: " + i + " : Inserting...");
            }


            // Getting data JSON Array nodes GoodLoans
            org.json.simple.JSONArray goodloans = (org.json.simple.JSONArray) jsonObj1.get("goodloans");

            // looping through All nodes
            for (int i = 0; i < goodloans.size(); i++) {
                if (goodloans.get(i) == null)
                    continue;

                org.json.simple.JSONObject c = (JSONObject) goodloans.get(i);

                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_GOOD_LOANS + "(OrgNo, OrgMemNo, MaxAmt," +
                                " Taken, UpdatedAt, _Month, _Year) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.get("OrgNo").toString(),
                                c.get("OrgMemNo").toString(),
                                c.get("MaxAmt").toString(),
                                c.get("Taken").toString(),
                                c.get("UpdatedAt").toString(),
                                c.get("Month").toString(),
                                c.get("Year").toString()
                        });
                Log.d("GOOD LOANS ", "Position: " + i + ": Inserting...");
            }

            Log.d("GOOD LOANS ", "Updated Successfully !");

            if (progressDialog.isShowing()) progressDialog.dismiss();

            da.WriteSyncTime(syncTime);
            da.UpdateEMethod(1);
            da.close();


        } catch (Exception e) {
            e.printStackTrace();
        }

        DeletePOjsonFile();
        DeletePOzipFile();

        Intent intent = new Intent(SyncDataSimpleJsonParser.this, MainActivity1.class);
        startActivity(intent);
    }

    public void DeletePOjsonFile() {
        Uri uriPOJson = Uri.parse("/storage/emulated/0/Download/" + CoNo + "results.json");
        File jsonPOfdelete = new File(uriPOJson.getPath());
        if (jsonPOfdelete.exists()) {
            if (jsonPOfdelete.delete()) {
                Log.d("BMFPO Json File:", "Deleted !");
            } else {
                Log.d("BMFPO Json File:", "Not Found !");
            }
        }
    }

    public void DeletePOzipFile() {
        Uri uriPOZip = Uri.parse("/storage/emulated/0/Download/" + CoNo + "-bmfpoerp.zip");

        File zipPOfdelete = new File(uriPOZip.getPath());

        if (zipPOfdelete.exists()) {
            if (zipPOfdelete.delete()) {
                Log.d("BMFPO Zip File:", "Deleted !");
                // Toast.makeText(getApplicationContext(), "Previous File Deleted" + uriZip.getPath(), Toast.LENGTH_LONG).show();
            } else {
                // Toast.makeText(getApplicationContext(), "Previous File not Deleted" + uriZip.getPath(), Toast.LENGTH_LONG).show();
                Log.d("BMFPO Zip File:", "Not Found !");
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void ReadTransTrailJSONFile() {
        DAO da = new DAO(getApplicationContext());
        da.open();

        org.json.simple.JSONObject jsonObject;
        JSONParser parser = new JSONParser();
        try {

            Object obj = parser.parse(new FileReader("/storage/emulated/0/Download/" + CoNo + "transtrail.json"));

            jsonObject = (org.json.simple.JSONObject) obj;

            // JSONObject json = (JSONObject) parser.parse(String.valueOf(obj));

            org.json.simple.JSONObject jsonObj1 = (org.json.simple.JSONObject) jsonObject.get("data");
            // Getting data JSON Array nodes Transtrail

            //JSONArray getTranstrail = jsonObj1.getJSONArray("transtrail");
            org.json.simple.JSONArray getTranstrail = (org.json.simple.JSONArray) jsonObj1.get("transtrail");


            // looping through All nodes
            for (int i = 0; i < getTranstrail.size(); i++) {

                org.json.simple.JSONObject c = (JSONObject) getTranstrail.get(i);

                String Transno = null;
                String loanTrx;
                String saveTrx;
                String transno;
                int trx = 0;


                // ColcDate = P8.ConvertStringToDate(da.GetTargetDate(c.getString("OrgNo")));
                String ColcDate = da.GetTargetDate(String.valueOf(c.get("OrgNo")));

                String ServerColcFor = c.get("ColcFor").toString();
                transno = P8.padLeft(c.get("TransNo").toString(), 11);

                try {
                    trx = Integer.parseInt((c.get("TrxType")).toString());
                } catch (NullPointerException num) {
                    num.printStackTrace();
                    continue;
                }

                //  Log.d("Transtrail ", "Data updating...");
                Log.d("TrxType ", "Position:" + i + " :" + trx);

                for (int x = 1; x <= loantrxItems.size(); x++) {
                    if (trx == x && ServerColcFor.equals("L")) {
                        loanTrx = loantrxItems.get(x).concat(transno);
                        Transno = loanTrx;
                    } else {
                        for (int y = 1; y <= saveTrxItems.size(); y++) {
                            if (trx == y && ServerColcFor.equals("S")) {
                                saveTrx = saveTrxItems.get(y).concat(transno);
                                Transno = saveTrx;
                            }
                        }
                    }
                }


                DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRANS_TRAIL + "(OrgNo, OrgMemNo, Projectcode, " +
                                "LoanNo, Tranamount, Colcdate, Transno, TrxType, ColcFor, UpdatedAt) " +
                                "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",

                        new String[]{c.get("OrgNo").toString(),
                                c.get("OrgMemNo").toString(),
                                c.get("ProjectCode").toString(),
                                c.get("LoanNo").toString(),
                                c.get("Tranamount").toString(),
                                // P8.FormatDate(ColcDate, "yyyy-MM-dd"),
                                ColcDate,
                                Transno,
                                c.get("TrxType").toString(),
                                c.get("ColcFor").toString(),
                                c.get("UpdatedAt").toString()});

                Log.d("TransTrail ", "Inserting..");

            }

            Log.d("TransTrail ", "Updating Successfully !!");

        } catch (NumberFormatException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        DeleteTransJsonFile();
    }

    public void DeleteTransJsonFile() {
        Uri uriTransJson = Uri.parse("/storage/emulated/0/Download/" + CoNo + "transtrail.json");
        File jsonPOTransFdelete = new File(uriTransJson.getPath());
        if (jsonPOTransFdelete.exists()) {
            if (jsonPOTransFdelete.delete()) {
                Log.d("BMFPO TransTrail File:", "Deleted !");
            } else {
                Log.d("BMFPO TransTrail File:", "Not Found !");
            }
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void initializeViews() {
        handler = new Handler();
        setTitle("Sync Data");
        Runtime rt = Runtime.getRuntime();
        long maxMemory = rt.maxMemory();
        Log.v("onCreate", "maxMemory:" + Long.toString(maxMemory));
        //Call Downloading function----------------------
        DownloadManagerInitialize();
        // TrxType data loading
        TrxTypedata();
        //init progress dilog
        progressDialog = new ProgressDialog(SyncDataSimpleJsonParser.this);
        //Database instance------------------------------
        DAO da = new DAO(getApplicationContext());
        da.open();
        PO po = da.getPO();
        CoNo = po.get_CONo();
        BranchCode = po.get_BranchCode();
        ProjectCode = po.get_ProjectCode();
        da.close();
        new DownLoadAsyncTask().execute();
    }

    private class DownLoadAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {

            final Thread thread = new Thread() {
                @Override
                public void run() {
                    Log.d("Load ServerData Thread ", "Started..");
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog = new ProgressDialog(SyncDataSimpleJsonParser.this);
                            progressDialog.setCancelable(false);
                            progressDialog.setTitle(" Loading Data");
                            progressDialog.setMessage("Please wait...");
                            progressDialog.show();
                        }
                    });
                    GetDataFromServer();
                }
            };
            thread.start();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if (progressDialog.isShowing()) progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private class ReadJSONAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(SyncDataSimpleJsonParser.this);
            progressDialog.setCancelable(false);
            progressDialog.setTitle(" Updating Data");
            progressDialog.setMessage("Please wait...");
            progressDialog.show();
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {
            Log.d("ReadJSONFile:  ", "Started..");
            ReadJSONFile();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            // if (progressDialog.isShowing()) progressDialog.dismiss();
            // progressDialog = null;
            new TransTrailAsyncTask().execute();
        }
    }


    private class TransTrailAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected Void doInBackground(Void... params) {

            final Thread thread = new Thread() {
                @Override
                public void run() {
                    Log.d("TransTrail_1 Thread ", "Started..");
                    handler.post(new Runnable() {
                        @Override
                        public void run() {

                        }
                    });

                    ReadTransTrailJSONFile();
                }
            };
            thread.start();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
        }
    }


    public String CurrentDateTime() {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dt.format(date);
    }

    public void DownloadManagerInitialize() {
        downloadManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void DownloadJsonFile() {
        list.clear();
        Uri uri = Uri.parse(App.getDownloadDataURL() + CoNo + ".zip");
        DownloadManager.Request request = new DownloadManager.Request(uri);
        if (DownloadManager.ERROR_HTTP_DATA_ERROR == 404) {
            Toast.makeText(SyncDataSimpleJsonParser.this, "Error", Toast.LENGTH_LONG).show();
        }
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle("BMFPO-ERP" + CoNo + "-bmfpoerp.zip");
        request.setDescription("BMFPO Data Downloading" + CoNo + "-bmfpoerp.zip");
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(DIRECTORY_DOWNLOADS, CoNo + "-bmfpoerp.zip");
        request.allowScanningByMediaScanner();
        refid = downloadManager.enqueue(request);
        Log.e("OUTNM", "" + refid);
        list.add(refid);
    }

    BroadcastReceiver onComplete = new BroadcastReceiver() {

        public void onReceive(Context ctxt, Intent intent) {

            long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

            String action = intent.getAction();

            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {

                DownloadManager.Query query = new DownloadManager.Query();

                query.setFilterById(referenceId);

                Cursor c = downloadManager.query(query);

                if (c.moveToFirst()) {
                    int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);

                    if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                        Log.e("IN", "" + referenceId);
                        list.remove(referenceId);

                        if (list.isEmpty()) {
                            Log.e("INSIDE", "" + referenceId);
                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(SyncDataSimpleJsonParser.this, "channelId")
                                            .setSmallIcon(R.drawable.ic_launcher)
                                            .setContentTitle("BMFPO Data").setContentText("PO Data Download completed!");
                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.notify(455, mBuilder.build());

                            SendLastDownloadStatus();
                            progressDialog.dismiss();
                            new ReadJSONAsyncTask().execute();
                        }

                    } else if (DownloadManager.STATUS_FAILED == c.getInt(columnIndex)) {

                        Log.i("handleData()", "Reason: " + c.getInt(c.getColumnIndex(DownloadManager.COLUMN_REASON)));

                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(SyncDataSimpleJsonParser.this, "channelId")
                                .setSmallIcon(R.drawable.ic_launcher).setContentTitle("PO Data Download Failed!!");
                        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                        notificationManager.notify(455, mBuilder.build());
                        progressDialog.dismiss();
                        AlertDialog(R.drawable.ic_error_black_24dp, "Download Failed !!", "Please , Try to download the file later.");

                    }
                }
            }

            registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        }
    };


    private boolean unpackZip(String path, String zipname) {
        InputStream is;
        ZipInputStream zis;
        try {
            is = new FileInputStream(path + zipname);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze;

            while ((ze = zis.getNextEntry()) != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                byte[] buffer = new byte[1024];
                int count;

                String filename = ze.getName();
                FileOutputStream fout = new FileOutputStream(path + filename);

                // reading and writing
                while ((count = zis.read(buffer)) != -1) {
                    baos.write(buffer, 0, count);
                    byte[] bytes = baos.toByteArray();
                    fout.write(bytes);
                    baos.reset();
                }

                fout.close();
                zis.closeEntry();
            }

            zis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    public void POExistsALert(final String cono, final String coName, final String brachCode,
                              final String branchName, final String branchOpenDate, final String SysDate, final String projectCode, final int sl) {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Attention !")
                .setMessage("Are you sure you want to assign as new PO? If you want to assign this PO your previous data will be lost!")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        DAO da = new DAO(App.getContext());
                        da.open();
                        try {
                            da.AssignCollector(cono, coName, brachCode, branchName, branchOpenDate, SysDate, projectCode, sl);
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            da.close();
                        }

                        Toast.makeText(getApplicationContext(), "PO Data Updated!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Toast.makeText(getApplicationContext(), "POST Transact Data!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                    }
                }).show();
    }


    public void AlertDialog(int drawable, String title, String message) {
        new AlertDialog.Builder(this)
                .setIcon(drawable)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(getApplicationContext(), MainActivity1.class);
                        startActivity(intent);
                    }
                }).show();
    }


    public void TrxTypedata() {
        loantrxItems = new HashMap<Integer, String>();
        saveTrxItems = new HashMap<Integer, String>();
        //Saving TrxType Used
        saveTrxItems.put(1, "SC");
        saveTrxItems.put(2, "SD");
        saveTrxItems.put(3, "ST");
        saveTrxItems.put(4, "YI");
        saveTrxItems.put(6, "SD");
        saveTrxItems.put(7, "EC");
        saveTrxItems.put(8, "SC");
        saveTrxItems.put(9, "ST");
        //Saving TrxType probable used
        saveTrxItems.put(5, "IC");//Info change
        saveTrxItems.put(10, "TT");//Transferred
        // loan TrxType Used
        loantrxItems.put(3, "LC");
        loantrxItems.put(7, "ST");
        loantrxItems.put(13, "LC");
        //Loan TrxType  Not Allowed yet
        loantrxItems.put(1, "LD");
        loantrxItems.put(2, "LP");
        loantrxItems.put(4, "LR");
        loantrxItems.put(5, "RS");
        loantrxItems.put(6, "LS");
        loantrxItems.put(8, "GA");
        //Loan TrxType Probable use for future
        loantrxItems.put(9, "WP");
        loantrxItems.put(10, "WD");
        loantrxItems.put(11, "CS");
        loantrxItems.put(12, "NS");
        loantrxItems.put(14, "WI");
        loantrxItems.put(15, "WF");
        loantrxItems.put(16, "TT");

        for (int i = 1; i <= loantrxItems.size(); i++) {
            String loanTrx = loantrxItems.get(i);
            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRX_MAPPING + "(TrxShortType, TrxTypeValue, ColcFor) " +
                            "VALUES(?, ?, ?)",
                    new String[]{loanTrx, String.valueOf(i), "L"});
        }
        for (int i = 1; i <= saveTrxItems.size(); i++) {
            String saveTrx = saveTrxItems.get(i);
            DAO.executeSQL("INSERT OR REPLACE INTO " + TBL_TRX_MAPPING + "(TrxShortType, TrxTypeValue, ColcFor) " +
                            "VALUES(?, ?, ?)",
                    new String[]{saveTrx, String.valueOf(i), "S"});
        }
    }

    @Override
    public void onPause() {
        super.onPause();
      /*  if (progressDialog != null)
            progressDialog.dismiss(); */
        //finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        //finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onRestart() {
        super.onRestart();
    }

    public void onCancel(View view) {
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(onComplete);
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

}
