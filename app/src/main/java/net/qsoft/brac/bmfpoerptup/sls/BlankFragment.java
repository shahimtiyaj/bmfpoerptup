package net.qsoft.brac.bmfpoerptup.sls;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.qsoft.brac.bmfpoerptup.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BlankFragment extends android.app.Fragment {


    public BlankFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.sls_fragment_blank, container, false);
    }

}
