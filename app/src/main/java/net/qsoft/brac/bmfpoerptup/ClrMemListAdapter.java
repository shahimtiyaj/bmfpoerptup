package net.qsoft.brac.bmfpoerptup;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.CLoan;
import net.qsoft.brac.bmfpoerptup.data.CMember;
import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.DBHelper;
import net.qsoft.brac.bmfpoerptup.data.VO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ClrMemListAdapter extends ArrayAdapter<HashMap<String, String>> implements Filterable {
	private static final String TAG = ClrMemListAdapter.class.getSimpleName();
	
    DAO da;
    ArrayList<HashMap<String, String>> items = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> objFiltered;
    
    public ClrMemListAdapter(Context context, ArrayList<HashMap<String, String>> objects) {
//        super(context, R.layout.member_row, objects);
        super(context, R.layout.member_row);
       items = objects;
       objFiltered = objects;
       da= new DAO(context);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public HashMap<String, String> getItem(int position) {
//        return super.getItem(position);
    	return objFiltered.get(position);
    }


    /* (non-Javadoc)
	 * @see android.widget.ArrayAdapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return objFiltered.size();
	}

	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
    	ArrayList<CLoan> cls= null;
    	HashMap<String, String>  cmem = (HashMap<String, String>) getItem(position);

        da.open();

//        Log.d(TAG, "Org: " + cmem.get(DBHelper.FLD_ORG_NO) + "\nMember: " + cmem.get(DBHelper.FLD_ORG_MEM_NO));
        if(!cmem.get(DBHelper.FLD_LOAN_NO).isEmpty()) {
        	// Loan number
        	cls = new ArrayList<CLoan>();
        	CLoan cl = da.getCloan(Integer.parseInt(cmem.get(DBHelper.FLD_LOAN_NO)));
        	cls.add(cl);
        } 
        else if(cmem.get(DBHelper.FLD_LOAN_STATUS_NAME).isEmpty())
        	cls = da.getAllCLoansByMember(cmem.get(DBHelper.FLD_ORG_NO), cmem.get(DBHelper.FLD_ORG_MEM_NO));
        
        da.close();
        
        ViewHolder holder;
        View v=convertView;
        if(v==null){
            LayoutInflater inflater = LayoutInflater.from(getContext());
            v = inflater.inflate(R.layout.member_row, parent, false);
            holder=new ViewHolder();
            holder.txtorgNo=(TextView)v.findViewById(R.id.textVONo);
            holder.txtorgMemno=(TextView)v.findViewById(R.id.textMemNo);
            holder.txtorgMemName=(TextView)v.findViewById(R.id.textMemName);
            //holder.txtorgMemName.setGravity())
            holder.txtHDR = (TextView)v.findViewById(R.id.textHDR);
            
            v.setTag(holder);

        }else{
            holder = (ViewHolder)v.getTag();
        }

        if(cmem!=null){
            holder.txtorgNo.setText(cmem.get(DBHelper.FLD_ORG_NO));
            holder.txtorgMemno.setText(cmem.get(DBHelper.FLD_ORG_MEM_NO));
            holder.txtorgMemName.setText(cmem.get(DBHelper.FLD_ORG_MEM_NAME));
            if(!cmem.get(DBHelper.FLD_LOAN_STATUS_NAME).isEmpty()) {
            	holder.txtHDR.setVisibility(View.VISIBLE);
            	holder.txtHDR.setText(cmem.get(DBHelper.FLD_LOAN_STATUS_NAME));
            	v.setOnClickListener(null);
            }
            else
            	holder.txtHDR.setVisibility(View.GONE);
        }
        if(cls != null) {
	        int clr = 9;		//Color.WHITE;
	        for(CLoan cl:cls) 
	        {
//	        	clr = checkTarget(cl.get_ODB(), cl.get_ReceAmt(), cl.get_TargetAmtLoan(),  clr);
	        	clr = checkTarget(cl, clr);
	        }
	        v.setBackgroundColor(getColor(clr));
        }
        
        return v;
    }
    
    private int checkTarget(CLoan cl, int lastColor) {
    	int ret;
    	
    	if(cl.get_ODB() > 0)
    		ret = 1;
    	else {
    		if(cl.get_ReceAmt() >= cl.get_TargetAmtLoan())
    			ret = 4;
    		else if (cl.get_ReceAmt() > 0 && cl.get_ReceAmt() < cl.get_TargetAmtLoan())
    			ret = 3;
    		else {
    			da.open();
    			VO vo = da.getVO(cl.get_OrgNo());
    			CMember cm = da.getCMember(cl.get_OrgNo(), cl.get_OrgMemNo());
    			da.close();		
    			
    			if(vo.get_LoanColcOption() == 2) {
    				// Monthly 
        			Calendar cal = Calendar.getInstance();
        			cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
        			cal.set(Calendar.HOUR_OF_DAY, 0);
        			cal.set(Calendar.MINUTE, 0);
        			cal.set(Calendar.SECOND, 0);
        			
        			Date lastDayOfMonth = cal.getTime();
        			
//        			Log.d(TAG, "Last date of month: " + lastDayOfMonth);
//        			Log.d(TAG, "Target date: " + cm.get_ColcDate());
        			
        			if(lastDayOfMonth.before(cm.get_ColcDate())) 
        				ret = 4;
        			else if((-cl.get_ODB()) >= cl.get_TargetAmtLoan() )
        				ret = 4;
        			else if((-cl.get_ODB()) > 0 && (-cl.get_ODB()) < cl.get_TargetAmtLoan() )
        				ret = 3;
        			else 
        				ret = 2;
    			}
    			else if((-cl.get_ODB()) >= cl.get_TargetAmtLoan() )
    				ret = 4;
    			else if((-cl.get_ODB()) > 0 && (-cl.get_ODB()) < cl.get_TargetAmtLoan() )
    				ret = 3;
    			else 
    				ret = 2;
    		}
    	}
    	return ret;
    }
/*    private int checkTarget(int od, int rece, int target, int lastColor) {
    	int ret;
    	
    	if(od > 0 )
    		ret = 1;	//Color.RED;
    	else if(rece >= target)
    		ret = 4;	//Color.GREEN;
    	else if(rece > 0 && rece < target)
    		ret = 3;	// Color.YELLOW;
    	else
    		ret = 2;
    	
    	if (lastColor < ret)
    		ret = lastColor;
    	return ret;
    }
*/   
    private int getColor(int cc) {
    	//int ret = Color.rgb(127, 255, 0);				//R.color.fully_paid_bg;
    	int ret = Color.rgb(178, 255, 102);
    	
    	if(cc==9)		//Savings only 
    		ret = Color.WHITE;
    	
    	if(cc==1)
    		ret = Color.rgb(255, 102, 102); 				//R.color.overdue_bd;
    	else if(cc==2)
    		//R.color.fully_unpaid_bg;
    		ret = Color.rgb(255, 191, 0);    		
    	else if(cc==3)
    		// ret = Color.rgb(255, 255, 102);	//R.color.part_paid_bd;
    		ret = Color.rgb(255,218,185);
    		
    	return ret;
    }
    
    static class ViewHolder {
        public TextView txtorgNo;
        public TextView txtorgMemno;
        public TextView txtorgMemName;
        public TextView txtHDR;
    }
    
    public Filter getFilter() {
        return new Filter() {

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                final FilterResults oReturn = new FilterResults();
                
                Log.d(TAG, "Char: " + constraint);
                if (constraint == null || constraint.length()==0) {
                	oReturn.values = items;
                	oReturn.count = items.size();
                }
                else {
                	final ArrayList<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();    
                    for (final HashMap<String, String> map : items) {
                        if (map.get(DBHelper.FLD_ORG_MEM_NAME).toLowerCase().contains(constraint.toString()) ||
                        		map.get(DBHelper.FLD_ORG_NO).toLowerCase().contains(constraint.toString()) ||
                        		map.get(DBHelper.FLD_ORG_MEM_NO).toLowerCase().contains(constraint.toString())) {
                            results.add(map);
                        }
                    }
                    oReturn.values = results;
                    oReturn.count = results.size();
                }
                return oReturn;
            }

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
            	objFiltered = (ArrayList<HashMap<String, String>>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
