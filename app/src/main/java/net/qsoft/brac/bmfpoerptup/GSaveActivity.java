package net.qsoft.brac.bmfpoerptup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class GSaveActivity extends TBaseActivity {

	/* (non-Javadoc)
	 * @see org.safesave.p8acm.TBaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mLHCode = "S+";
		TType="S";
		setTitle(getString(R.string.description_save));
//		imageIcon.setImageResource(R.drawable.save);
//		imageIcon.setContentDescription(getString(R.string.description_save));

		accLabel.setVisibility(View.GONE);
		repoButton.setVisibility(View.VISIBLE);
	}

	/* (non-Javadoc)
	 * @see org.safesave.p8acm.TBaseActivity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		dispLabel.setText("Savings bal: " + cm.get_SB().toString());
		dispLabel2.setText("Target: " + cm.get_SIA().toString());

//		dispLabel.setText("");
	}

	public void onOk(View view) {
		if (P8.IsValidAmount(this, editAmount)) {
			mAmount = Integer.parseInt(editAmount.getText().toString());
			if(mAmount == 0) {
				P8.ErrorSound(this);
				editAmount.setError(getString(R.string.error_zero_number));
				editAmount.requestFocus();
			}
			else {
				if(!BalanceEqual(cm.get_SB(), getString(R.string.description_save), null))
					VerifyAmount(getString(R.string.description_save), null);
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.safesave.p8acm.TBaseActivity#UpdateTransactions()
	 */
	@Override
	protected boolean UpdateTransactions() {
		// TODO Auto-generated method stub
		if(super.UpdateTransactions()) {
            ShowBanglaBalance("এখন জমা", mAmount, "বর্তমান মোট সঞ্চয়", cm.get_SB().intValue());
//			Toast.makeText(this, "Savings update successful.", Toast.LENGTH_SHORT).show();
			finish();
		}
		return false;
	}

	public void onReport(View view) {
		  Intent it = new Intent(this, MSCReportActivity.class);
		  it.putExtra(P8.VONO, cm.get_OrgNo());
		  it.putExtra(P8.MEMNO, cm.get_OrgMemNo());
		  startActivity(it);
		}
}
