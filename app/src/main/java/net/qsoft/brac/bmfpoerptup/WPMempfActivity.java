package net.qsoft.brac.bmfpoerptup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import net.qsoft.brac.bmfpoerptup.data.CLoan;
import net.qsoft.brac.bmfpoerptup.data.DAO;

import java.util.HashMap;

public class WPMempfActivity extends MempfActivity {

	protected void onCreate(Bundle savedInstanceState) {
		HasPrintModule = false;
		super.onCreate(savedInstanceState);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		String sln = ((HashMap<String, String>) lv.getItemAtPosition(position)).get("[LoanNo]").toString();

		if(sln.equals("Savings")) {
			startActivity(new Intent(this, WPGSaveActivity.class));
		}
		else {
			DAO da = new DAO(this);
			da.open();
			CLoan cl = da.getCloan(Integer.parseInt(sln));
			CLoan.set_lastCL(cl);
			da.close();
			
			startActivity(new Intent(this, WPGRepayActivity.class));
		}
		
//		Toast.makeText(getBaseContext(), "Loan#/Savings: " + sln , Toast.LENGTH_LONG).show();
		
	}		
}
