package net.qsoft.brac.bmfpoerptup;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.CMember;

public class BanglaActivity extends SSActivity {

	public static String TRANS_LABEL="org.safesave.p8acm.BanglaActivity.TRANS_LABEL";
	public static String TRANS_VALUE="org.safesave.p8acm.BanglaActivity.TRANS_VALUE";
	public static String BAL_LABEL="org.safesave.p8acm.BanglaActivity.BAL_LABEL";
	public static String BAL_VALUE="org.safesave.p8acm.BanglaActivity.BAL_VALUE";
	public static String IMAGE_ID="org.safesave.p8acm.BanglaActivity.IMAGE_ID";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_bangla);
//		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		
		TextView transLabel = (TextView) findViewById(R.id.transLabel);
		TextView transValueLable = (TextView) findViewById(R.id.transValueLable);
		TextView balLabel =  (TextView) findViewById(R.id.balLabel);
		TextView balValueLabel = (TextView) findViewById(R.id.balValueLabel);
		ImageView imageIcon = (ImageView) findViewById(R.id.imageIcon);
		TextView nameLabel = (TextView) findViewById(R.id.nameLabel);
		
//		Typeface face=Typeface.createFromAsset(getAssets(), "fonts/kalpurushANSI.ttf");
//		transLabel.setTypeface(face);
//		transValueLable.setTypeface(face);
//		balLabel.setTypeface(face);
//		balValueLabel.setTypeface(face);
		
		((Button) findViewById(R.id.cancelButton)).setVisibility(View.GONE);
		
		Intent it = getIntent();
		transLabel.setText(it.getStringExtra(BanglaActivity.TRANS_LABEL));
		//transValueLable.setText(Integer.toString(it.getIntExtra(BanglaActivity.TRANS_VALUE, 0)));
		transValueLable.setText(P8.toBanglaNumber(it.getIntExtra(BanglaActivity.TRANS_VALUE, 0)));
		balLabel.setText(it.getStringExtra(BanglaActivity.BAL_LABEL));
//		balValueLabel.setText(Integer.toString(it.getIntExtra(BanglaActivity.BAL_VALUE, 0)));
		balValueLabel.setText(P8.toBanglaNumber(it.getIntExtra(BanglaActivity.BAL_VALUE, 0)));
//		imageIcon.setImageResource(it.getIntExtra(BanglaActivity.IMAGE_ID, android.R.drawable.btn_star_big_on));
		nameLabel.setText(CMember.get_lastCM().get_OrgNo() + "  " + 
				CMember.get_lastCM().get_OrgMemNo() + "  " + 
				CMember.get_lastCM().get_MemberName());
	}

	public void onOk(View view) {
		finish();
	}
}
