package net.qsoft.brac.bmfpoerptup;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends SSActivity {

	/**
	 * The Intent extra
	 */
	public static final String EXTRA_PASS = "net.qsoft.brac.bmfpo.PASSWORD";

	// Values for password at the time of the login attempt.
	private String mPasswordToVerify;
	private String mPassword;

	// UI references.
	private EditText mPasswordView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.login);

		// Retrieve passed password
		mPasswordToVerify = getIntent().getStringExtra(EXTRA_PASS);

		mPasswordView = (EditText) findViewById(R.id.password);
		mPasswordView
				.setOnEditorActionListener(new TextView.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView textView, int id,
							KeyEvent keyEvent) {
						if (id == R.id.login || id == EditorInfo.IME_NULL) {
							attemptLogin();
							return true;
						}
						return false;
					}
				});
	}

	public void onOk(View view) {
		attemptLogin();
	}

	public void onCancel(View view) {
		finish();
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {
		// Reset errors.
		mPasswordView.setError(null);

		// Store values at the time of the login attempt.
		mPassword = mPasswordView.getText().toString();

		// Check for a valid password.
		if (TextUtils.isEmpty(mPassword)) {
			P8.ErrorSound(this);
			mPasswordView.setError(getString(R.string.error_field_required));
		} else if (!mPassword.equals(mPasswordToVerify)) {
			P8.ErrorSound(this);
			mPasswordView.setError(getString(R.string.error_incorrect_password));
		} else {
			// matched
			Intent resultIntent = new Intent();
			setResult(Activity.RESULT_OK, resultIntent);
			finish();
		}

	}

}
