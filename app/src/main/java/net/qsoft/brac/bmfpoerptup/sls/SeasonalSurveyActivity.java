package net.qsoft.brac.bmfpoerptup.sls;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.P8;
import net.qsoft.brac.bmfpoerptup.R;
import net.qsoft.brac.bmfpoerptup.SSActivity;
import net.qsoft.brac.bmfpoerptup.data.SurveySL;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

//import android.support.v7.app.AppCompatActivity;

public class SeasonalSurveyActivity extends SSActivity implements OnFragmentInteractionListener {

    private Button btnBack, btnNext;
    private TextView txtMember;

    private CheckBox chk1,chk2, chk3;
    private RadioGroup radio1,radio2,radio3;

   ArrayList<Integer>rbValue;


    Fragment fragment;

    Integer iPage = 0;

    Date datQuery = null;
    String vono = null;
    String memno = null;
    String memName = null;
    static ArrayList<HashMap<String, String>> memlist=null;
    static ArrayList<HashMap<String, String>> volist=null;
    static Integer indexVONo = 0;
    static Integer indexMemNo=0;

    TextView tPage;

    static HashMap<Integer, Integer> bv = new HashMap<>();
    RadioButton MSI,USI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sls);

        btnBack = (Button) findViewById(R.id.bPg2);
        btnNext = (Button) findViewById(R.id.bPg1);
        txtMember = (TextView) findViewById(R.id.txtMember);
        tPage = (TextView ) findViewById(R.id.txtPage);

        btnNext.setEnabled(false);
//vono = "2010";
//memno = "000020";

        // Loan member last survey data if any

        setTitle(R.string.product_seasonal_loan);

        // Lookup table create for button values
        bv.put(R.id.rbAge1, 1);
        bv.put(R.id.rbAge2, 2);
        bv.put(R.id.rbAge3, 3);
        bv.put(R.id.rbEdu1, 1);
        bv.put(R.id.rbEdu2, 2);
        bv.put(R.id.rbEdu3, 3);
        bv.put(R.id.rbH1, 1);
        bv.put(R.id.rbH2, 2);
        bv.put(R.id.rbH3, 3);
//        bv.put(R.id.rbDL1, 1);
//        bv.put(R.id.rbDL2, 2);
//        bv.put(R.id.rbOB1, 1);
//        bv.put(R.id.rbOB2, 2);
//        bv.put(R.id.rbPB1,1);
//        bv.put(R.id.rbPB2, 2);
//        bv.put(R.id.rbOS1, 1);
//        bv.put(R.id.rbOS2, 2);
        bv.put(R.id.rbCAge0_51, 1);
        bv.put(R.id.rbCAge0_52, 2);
        bv.put(R.id.rbCAge0_53, 3);
        bv.put(R.id.rbCAge5_101, 1);
        bv.put(R.id.rbCAge5_102, 2);
        bv.put(R.id.rbCAge5_103, 3);
        bv.put(R.id.rbCAge10Up1, 1);
        bv.put(R.id.rbCAge10Up2, 2);
        bv.put(R.id.rbCAge10Up3, 3);
        bv.put(R.id.rbPLOO1, 1);
        bv.put(R.id.rbPLOO2, 2);
        bv.put(R.id.rbPLOP1, 1);
        bv.put(R.id.rbPLOP2, 2);
        bv.put(R.id.rbPLLO1, 1);
        bv.put(R.id.rbPLLO2, 2);
        bv.put(R.id.rbPLLP1, 1);
        bv.put(R.id.rbPLLP2, 2);
        bv.put(R.id.rbUSI, 1);
        bv.put(R.id.rbMSI, 2);

//        DAO da = new DAO(this);
//        da.open();
        volist = SurveySL.getVOList();
//        memlist = da.getMemberList("SELECT C.[OrgNo], C.[OrgMemNo], C.[MemberName] " +
//                "FROM CMembers C INNER JOIN CLoans L ON C.[OrgNo]=L.[OrgNo] AND C.[OrgMemNo]=L.[OrgMemNo] " +
//                "WHERE L.[ProductNo]='002' " +
//                "ORDER BY  C.[OrgNo], C.[OrgMemNo]", null);  //   INNER JOIN CLoans L ON C.[OrgNo]=L.[OrgNo] AND C.[OrgMemNo]=L.[OrgMemNo] " + "WHERE L.[ProductNo]='174' "
//        da.close();

        ShowPage();
    }

    public void onFragmentInteraction(Integer postion, final String vno, final String mno, String mname) {
        vono = vno;
        memno = mno;
        memName = mname;

        if(memno.trim().length() > 0) {
            indexMemNo = postion;
            SurveySL ssl = new SurveySL(vono, memno);
            SurveySL.setLastSurveySL(ssl);
            txtMember.setText("Member: " + vono + " - " + memno + " " + memName + "\nDate: " + ssl.getSDateFormated() );

            btnNext.setEnabled(true);
            if (ssl.Exists() ) {    // && ssl.getSDate().before((P8.ToDay()))
                // Ask edit existing or add new record
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which != DialogInterface.BUTTON_POSITIVE) {
                            SurveySL ssl = new SurveySL();
                            ssl.setOrgNo(vono);
                            ssl.setOrgMemNo(memno);
                            SurveySL.setLastSurveySL(ssl);
                            txtMember.setText("Member: " + vono + " - " + memno + " " + memName + "\nDate: " + ssl.getSDateFormated() );
                        }
                        dialog.dismiss();
                        iPage++;
                        ShowPage();
                    }
                };

                String sDisp, sEdOrView;
                if(ssl.isSent()) {
                    sDisp = "Do you want to Add New or View this record?";
                    sEdOrView = "View";

                }
                else {
                    sDisp = "Do you want to Add New or Edit this record?";
                    sEdOrView = "Edit";
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Record Exists")
                        .setMessage(sDisp)
                        .setIcon(R.drawable.ic_launcher)
                        .setCancelable(true)
                        .setPositiveButton(sEdOrView, dialogClickListener)
                        .setNegativeButton("Add New", dialogClickListener).show();

            } else {
                iPage++;
                ShowPage();
            }
        }
        else if(vono.length() > 0) {
            indexVONo = postion;
            vono = vno;
            txtMember.setText("VO: " +  vono);
            memlist = SurveySL.getMemberList(vono);
            iPage++;
            ShowPage();
        }
        else {
            // Called from btnShowData
            vono = "";
            memlist = SurveySL.getSurveyDataMemberList();
            txtMember.setText("Survey data count: " +  memlist.size() );
            iPage++;
            ShowPage();
        }
    }

    public static void setRGClearAndSet(RadioGroup rg, int value) {
        for(int i = 0; i < rg.getChildCount(); i++){
            RadioButton rb = ((RadioButton)rg.getChildAt(i));
            if(bv.get(rb.getId()) == value)
                rb.setChecked(true);
            else
                rb.setChecked(false);
        }
    }

    public static Integer getRGGetValue(RadioGroup rg) {
        Integer ret = 0;
        Integer iBId = rg.getCheckedRadioButtonId();
        if(iBId != -1 )
            ret = bv.get(iBId);

        return ret;
    }

    public  static Integer getCheckedValue(CheckBox ckbox) {
        Integer ret = 0;
        if(ckbox.isChecked())
            ret = 1;

        return ret;
    }

    public  static void setCheckedValue(CheckBox ckBox, Integer value) {
        ckBox.setChecked(value==1);
    }

    public void ChangeFragment(View view) {

        if (view.getId() == R.id.bPg1) {

            if (iPage < 6) {
                iPage++;
                ShowPage();
            }
            else {
                // Save
                DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(which==DialogInterface.BUTTON_POSITIVE) {
                            SaveRecord();
                            iPage = 1;
                        }
                        else {
                            iPage++;
                        }
                        dialog.dismiss();
                        ShowPage();
                    }
                };

                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Save Data")
                        .setMessage("Do you want to save?")
                        .setIcon(R.drawable.ic_launcher)
                        .setCancelable(false)
                        .setPositiveButton(R.string.yes, dialogClickListener)
                        .setNegativeButton(R.string.no, dialogClickListener).show();

                iPage--;
                ShowPage();
            }
        }
        else if (view.getId() == R.id.bPg2) {

            if (iPage > 0) {
                iPage--;
                ShowPage();
            }
            else
                finish();
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if(iPage > 0) {
            iPage--;
            ShowPage();
        }
        else
            finish();
    }

    void ShowPage() {

        if(iPage==0) {
//            btnBack.setEnabled(false);
            btnNext.setEnabled(false);
            vono="";
            memno = "";
            memName = "";
            indexVONo=0;
            indexMemNo=0;

            txtMember.setText("Select a VO");

            fragment = new Pg0();
            tPage.setText("");
        }
        else if(iPage==1) {
//            btnBack.setEnabled(false);
            fragment = new Pg1();
            tPage.setText("");
        }
        else if(iPage==2 ) {
//            btnBack.setEnabled(true);
            fragment = new Pg2();
            tPage.setText("Page 1/5");
        }
        else if(iPage == 3) {
//            btnNext.setEnabled(true);
            fragment = new Pg4();
            tPage.setText("Page 2/5");
        }
        else if(iPage == 4) {
 //           btnBack.setEnabled(true);
  //          btnNext.setText("Next");
            fragment = new Pg3();
            tPage.setText("Page 3/5");
        }
        else if(iPage == 5) {
//            btnBack.setEnabled(true);
            btnNext.setText("Next");
            btnNext.setEnabled(true);
            fragment = new Pg5();
            tPage.setText("Page 4/5");
        }
        else if(iPage == 6) {
            if(SurveySL.getLastSurveySL().isSent())
                btnNext.setEnabled(false);
            else
                btnNext.setText("Save");
            fragment = new Pg6();
            tPage.setText("Page 5/5");
        }

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.FArea, fragment);
        ft.commit();

    }

    public void pg3checkBoxListener(View v) {
//        CheckBox chk1 = (CheckBox) v3;
        CheckBox chk1 = (CheckBox) v;    //.findViewById(R.id.chkOSI);
        CheckBox chkDL = (CheckBox) findViewById(R.id.chkDL);
        CheckBox chkOB = (CheckBox) findViewById(R.id.chkOB);
        CheckBox chkPB = (CheckBox) findViewById(R.id.chkPB);
        CheckBox chkOS = (CheckBox) findViewById(R.id.chkOS);

        MSI=(RadioButton) findViewById(R.id.rbMSI);
        USI=(RadioButton) findViewById(R.id.rbUSI);

//        setRGEnabled(radio1, !chk1.isChecked());
//        setRGEnabled(radio2, !chk1.isChecked());
//        setRGEnabled(radio3, !chk1.isChecked());
//        setRGEnabled(radio4, !chk1.isChecked());

        if (chk1.isChecked()) {

            USI.setChecked(true);
            MSI.setChecked(false);

            chkDL.setChecked(false);
            chkDL.setEnabled(false);

            chkOB.setChecked(false);
            chkOB.setEnabled(false);

            chkPB.setChecked(false);
            chkPB.setEnabled(false);

            chkOS.setChecked(false);
            chkOS.setEnabled(false);
        }
       else {
            USI.setChecked(false);
            MSI.setChecked(true);
            chkDL.setEnabled(true);
            chkOB.setEnabled(true);
            chkPB.setEnabled(true);
            chkOS.setEnabled(true);
        }
    }

    public void pg4checkBoxListener(View v4){
//        CheckBox chkp4 = (CheckBox) v4;
        CheckBox chk4= (CheckBox) v4;

        RadioGroup radio1=(RadioGroup) findViewById(R.id.radioCAge0_5);
        RadioGroup radio2=(RadioGroup) findViewById(R.id.radioCAge5_10);
        RadioGroup radio3=(RadioGroup) findViewById(R.id.radioCAge10Up);

        setRGEnabled(radio1, !chk4.isChecked());
        setRGEnabled(radio2, !chk4.isChecked());
        setRGEnabled(radio3, !chk4.isChecked());

        totalChilldren(null);
    }

    public void totalChilldren(View v) {
        RadioGroup radio1=(RadioGroup) findViewById(R.id.radioCAge0_5);
        RadioGroup radio2=(RadioGroup) findViewById(R.id.radioCAge5_10);
        RadioGroup radio3=(RadioGroup) findViewById(R.id.radioCAge10Up);
        ((TextView) findViewById(R.id.textTotalChildren)).setText("মোট সন্তান সংখ্যাঃ " +
                 P8.toBanglaNumber(getTotalChildren(radio1, radio2, radio3)));
    }

    Integer getTotalChildren(RadioGroup a, RadioGroup b, RadioGroup c ) {
        Integer ret = 0;
        if(a.getCheckedRadioButtonId() > 0)
            ret = ret + bv.get(a.getCheckedRadioButtonId());

        if(b.getCheckedRadioButtonId() > 0)
            ret = ret + bv.get(b.getCheckedRadioButtonId());

        if(c.getCheckedRadioButtonId() > 0)
            ret = ret + bv.get(c.getCheckedRadioButtonId());

        return ret;
    }

    public static void setRGEnabled(RadioGroup rg, boolean checked) {
        if(!checked)
            rg.clearCheck();
        for(int i = 0; i < rg.getChildCount(); i++){
            ((RadioButton)rg.getChildAt(i)).setEnabled(checked);
        }
    }

    void SaveRecord() {
      try {
            SurveySL.getLastSurveySL().Save();
        }
    catch (Exception e) {
           P8.ShowError(this, e.getMessage());
        }
    }
}

