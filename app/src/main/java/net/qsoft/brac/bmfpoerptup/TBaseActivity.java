package net.qsoft.brac.bmfpoerptup;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.CLoan;
import net.qsoft.brac.bmfpoerptup.data.CMember;
import net.qsoft.brac.bmfpoerptup.data.DAO;

public class TBaseActivity extends SSActivity {

	// Class name tab for Logging process
	private static final String TAG = TBaseActivity.class.getSimpleName();

	protected TextView dispLabel;
	protected TextView dispLabel2;
	protected TextView dateLabel;
	protected TextView accLabel;
	protected TextView instLabel;
	protected EditText editAmount;
	protected TextView promptVoucher;
	protected EditText editVoucher;
	protected ImageView imageIcon;
	protected TextView memLabel;
	protected TextView voLabel;
	protected Button repoButton;

	protected String mAccountNo="";
	protected int mAmount=0;
	protected String mVoucherNo="";
	protected String mLHCode;

	protected CMember cm = null;
	protected CLoan cl = null;
	protected String TType="S";
	protected Integer intOption=0;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_tbase);

		dispLabel = (TextView) findViewById(R.id.dispLabel);
		dispLabel2 = (TextView) findViewById(R.id.dispLabel2);
		dateLabel = (TextView) findViewById(R.id.dateLabel);
		accLabel = (TextView) findViewById(R.id.accLabel);
		instLabel = (TextView) findViewById(R.id.instLabel);
		editAmount = (EditText) findViewById(R.id.editAmount);
		promptVoucher = (TextView) findViewById(R.id.promptVoucher);
		editVoucher = (EditText) findViewById(R.id.editVoucher);
		memLabel = (TextView) findViewById(R.id.memLabel);
		voLabel = (TextView) findViewById(R.id.voLabel);
		repoButton = (Button) findViewById(R.id.repoTBaseButton);

//		imageIcon = (ImageView) findViewById(R.id.imageIcon);

//		Intent it = getIntent();
//		intOption=it.getIntExtra(P8.TRANS_OPTION, P8.TRANS_OPTION_SAVINGS);
//		if(intOption==P8.TRANS_OPTION_SAVINGS)
//			TType="S";
//		else
//			TType="L";

//		Log.d(TAG, "LHCode: " + mLHCode + "   Account#: " + mAccountNo);


	}

	/* (non-Javadoc)
	 * @see android.app.Activity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		cm = CMember.get_lastCM();
		memLabel.setText(cm.get_OrgMemNo() + " " + cm.get_MemberName());
		voLabel.setText("VO: " + cm.get_OrgNo());

		dateLabel.setText(getString(R.string.prompt_date) +" "+ P8.getDate());
	}

	public void onCancel(View view) {
		finish();
	}

	protected boolean BalanceEqual(int matchValue, final String itm, DialogInterface.OnClickListener dialogClick) {
		return BalanceEqual(matchValue, itm, dialogClick, itm);
	}

	protected boolean BalanceEqual(int matchValue, final String itm, DialogInterface.OnClickListener dialogClick, final String vfitm){
		boolean ret = (matchValue == mAmount);
		Log.d(TAG, "MatchValue=" + Integer.toString(matchValue) + " editAmount=" + Integer.toString(mAmount));
		if(dialogClick==null) {
			dialogClick = new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	            	if(which==DialogInterface.BUTTON_POSITIVE) {
	            		VerifyAmount(vfitm, null);
	            	}
	            	dialog.dismiss();
	            }
	        };
		}
		if(ret) {
			P8.ErrorSound(this);
		    AlertDialog.Builder builder = new AlertDialog.Builder(this);
	        builder.setTitle(R.string.title_warrning_dialog)
	        	.setMessage(itm +" "+ getString(R.string.prompt_balance_equal))
	            .setIcon(android.R.drawable.ic_dialog_alert)
	            .setCancelable(false)
	            .setPositiveButton(android.R.string.yes, dialogClick)
	            .setNegativeButton(android.R.string.no, dialogClick).show();
		}
		return ret;
	}

	protected void VerifyAmount(final String itm, DialogInterface.OnClickListener dialogClick) {
		if(dialogClick==null) {
			dialogClick = new DialogInterface.OnClickListener() {
	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	            	if(which==DialogInterface.BUTTON_POSITIVE) {
	            		UpdateTransactions();
	            	}
	            	dialog.dismiss();
	            }
	        };
		}

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
    	builder.setTitle(R.string.title_verify)
    	.setMessage(getString(R.string.prompt_please_verify) + "\n\n" +
            	itm + ": " + editAmount.getText().toString())
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setCancelable(false)
        .setPositiveButton(android.R.string.ok, dialogClick)
        .setNegativeButton(android.R.string.cancel, dialogClick).show();
	}

	protected boolean UpdateTransactions() {
		DAO db=null;
		boolean ret=false;

		try {
			db = new DAO(this);
			db.open();
			if(TType.equals("S")) {
				db.addTransaction(cm, TType, mAmount);
			}
			else {
				db.addTransaction(cl, TType, mAmount);
			}
			P8.OkSound(this);
			ret=true;

		} catch (Exception e) {
			P8.ShowError(this, e.toString());
		}
		finally {
			if(db!=null)
				db.close();
		}
		return ret;
	}

	protected void ShowBanglaBalance(String tranLabel, int tranValue, String balLabel, int balValue) {
    	Intent it = new Intent(this, BanglaActivity.class);
    	it.putExtra(BanglaActivity.TRANS_LABEL, tranLabel)
    	  .putExtra(BanglaActivity.TRANS_VALUE, tranValue)
    	  .putExtra(BanglaActivity.BAL_LABEL, balLabel)
    	  .putExtra(BanglaActivity.BAL_VALUE, balValue);
    	
    	startActivity(it);
    	
    }
}
