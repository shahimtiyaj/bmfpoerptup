package net.qsoft.brac.bmfpoerptup.data;

import net.qsoft.brac.bmfpoerptup.P8;

import java.util.ArrayList;
import java.util.Date;

public class Transact {

	Integer _id; 
	Integer _ColcID;
	String _OrgNo;
	String _OrgMemNo;
	String _ProjectCode;
	Integer _LoanNo;
	Integer _ColcAmt;
	Integer _SavAdj;
	Date _ColcDate;
	String _ColcFor;
	String _ProductSymbol;
	Integer _Balance;
	Integer _SMSStatus;

	public Transact() {
		super();
	}

	/**
	 * @param _id
	 * @param _ColcID
	 * @param _OrgNo
	 * @param _OrgMemNo
	 * @param _ProjectCode
	 * @param _LoanNo
	 * @param _ColcAmt
	 * @param _SavAdj
	 * @param _ColcDate
	 * @param _ColcFor
	 */
	public Transact(Integer _id, Integer _ColcID, String _OrgNo,
			String _OrgMemNo, String _ProjectCode, Integer _LoanNo,
			Integer _ColcAmt, Integer _SavAdj, Date _ColcDate, String _ColcFor,
			String _ProductSymbol, Integer _Balance, Integer _SMSStatus) {
		super();
		this._id = _id;
		this._ColcID = _ColcID;
		this._OrgNo = _OrgNo;
		this._OrgMemNo = _OrgMemNo;
		this._ProjectCode = _ProjectCode;
		this._LoanNo = _LoanNo;
		this._ColcAmt = _ColcAmt;
		this._SavAdj = _SavAdj;
		this._ColcDate = _ColcDate;
		this._ColcFor = _ColcFor;
//		if(_ProductSymbol==null)
//			this._ProductSymbol = "";
//		else
			this._ProductSymbol = _ProductSymbol;
//		if(_Balance == null)
//			this._Balance = 0;
//		else
			this._Balance = _Balance;
//		if(_SMSStatus==null)
//			this._SMSStatus = 0;
//		else
			this._SMSStatus = _SMSStatus;
	}

	public String get_ProductSymbol() {
		return _ProductSymbol;
	}

	public Integer get_Balance() {
		return _Balance;
	}

	public Integer get_SMSStatus() {
		return _SMSStatus;
	}

	/**
	 * @return the _id
	 */
	public final Integer get_id() {
		return _id;
	}

	/**
	 * @return the _ColcID
	 */
	public final Integer get_ColcID() {
		return _ColcID;
	}

	/**
	 * @return the _OrgNo
	 */
	public final String get_OrgNo() {
		return _OrgNo;
	}

	/**
	 * @return the _OrgMemNo
	 */
	public final String get_OrgMemNo() {
		return _OrgMemNo;
	}

	/**
	 * @return the _ProjectCode
	 */
	public final String get_ProjectCode() {
		return _ProjectCode;
	}

	/**
	 * @return the _LoanNo
	 */
	public final Integer get_LoanNo() {
		return _LoanNo;
	}

	/**
	 * @return the _ColcAmt
	 */
	public final Integer get_ColcAmt() {
		return _ColcAmt;
	}

	/**
	 * @return the _SavAdj
	 */
	public final Integer get_SavAdj() {
		return _SavAdj;
	}

	/**
	 * @return the _ColcDate
	 */
	public final Date get_ColcDate() {
		return _ColcDate;
	}

	/**
	 * @return the _ColcFor
	 */
	public final String get_ColcFor() {
		return _ColcFor;
	}

	public static void prepareSMSText(ArrayList<Transact> trans, StringBuilder sb, StringBuilder sbrec ) {
		//sb = new StringBuilder(P8.toBanglaNumber(P8.FormatDate(P8.ToDay(), "dd-MM-yyyy")));
        sb.append(P8.toBanglaNumber(P8.FormatDate(P8.ToDay(), "dd-MM-yyyy")));
//		sbrec = new StringBuilder();

		for (Transact ct : trans) {
			if (sb.length() > 0)
				sb.append("\n");

			if (sbrec.length() > 0)
				sbrec.append(", ");
			sbrec.append(ct.get_id());

			if (ct.get_ColcFor().equals("S")) {
				sb.append("সঞ্চয় জমা ").append(P8.toBanglaNumber(ct.get_ColcAmt()));
				sb.append(" মোট সঞ্চয় ").append(P8.toBanglaNumber(ct.get_Balance()));
			} else if (ct.get_ColcFor().equals("L")) {
				if (ct.get_ProductSymbol().equals("DGD")) {
					sb.append("গুড লোন পরিশোধ ").append(P8.toBanglaNumber(ct.get_ColcAmt()));
					sb.append(" মোট পরিশোধ ").append(P8.toBanglaNumber(ct.get_Balance()));
				} else {
					sb.append("সাধারণ ঋণ পরিশোধ ").append(P8.toBanglaNumber(ct.get_ColcAmt()));
					sb.append(" মোট পরিশোধ ").append(P8.toBanglaNumber(ct.get_Balance()));
				}
			}
		}
	}
}