package net.qsoft.brac.bmfpoerptup;

import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.CMember;
import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.DBHelper;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.data.VO;

import java.util.ArrayList;
import java.util.HashMap;

public class MSCReportActivity extends SSActivity {
    private static final String TAG = MLCReportActivity.class.getSimpleName();

    TextView branchName;
    TextView voName;
    TextView memName;
    TextView toDay;
    ListView lv;
    Button okButton;

    ArrayList<HashMap<String, String>> items = null;
    String memno;
    String vono;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mscreport);

        branchName = (TextView) findViewById(R.id.textBranchName);
        voName = (TextView) findViewById(R.id.textVOName);
        memName = (TextView) findViewById(R.id.textMem);
        toDay = (TextView) findViewById(R.id.textToday);

        lv = (ListView) findViewById(R.id.listViewMLR);
        okButton = (Button) findViewById(R.id.okButton);

        okButton.setVisibility(View.GONE);

        if (getIntent().hasExtra(P8.VONO)) {
            vono = getIntent().getExtras().getString(P8.VONO);
//			Log.d(TAG, "In has extra " + vono);
        }
        if (getIntent().hasExtra(P8.MEMNO)) {
            memno = getIntent().getExtras().getString(P8.MEMNO);
        }

        if (VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        createMSReport();
    }

    private void createMSReport() {
        DAO da = new DAO(this);
        da.open();
        PO po = da.getPO();
        VO vo = da.getVO(vono);
        CMember cm = da.getCMember(vono, memno);
        items = da.getMemWiseSavingsRep(vono, memno);
//		Log.d(TAG, "Items: " + items );
        da.close();
        if (vo != null) {
            branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());
            voName.setText(vo.get_OrgNo() + " - " + vo.get_OrgName());
            toDay.setText("As on " + P8.FormatDate(P8.ToDay(), "MMM dd, yyyy"));
            memName.setText("Member: " + cm.get_OrgMemNo() + " - " + cm.get_MemberName());

            String[] from = new String[]{DBHelper.FLD_COLC_DATE, DBHelper.FLD_TARGET_SAVINGS_AMT, DBHelper.FLD_COLC_AMT};
            int[] to = {R.id.textColcDate, R.id.textTargetAmt, R.id.textColcAmt};

            SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), items, R.layout.mlcreport_row, from, to);
            lv.setAdapter(adapter);
        } else {
            Log.d(TAG, "VO not found!");
        }
    }

    public void onCancel(View view) {
        // Back
        finish();
    }
}
