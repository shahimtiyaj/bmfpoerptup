package net.qsoft.brac.bmfpoerptup;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

public class LRTARListAdapter extends ArrayAdapter<HashMap<String, String>> {
	// View lookup cache
    private static class ViewHolder {
    	TextView gphead;
        TextView orgno;
        TextView targetnum;
        TextView achievenum;
        TextView diffnum;
        TextView targetamt;
        TextView achieveamt;
        TextView diffamt;        
    }

	public LRTARListAdapter(Context context, int resource, List<HashMap<String, String>> rows) {
		super(context, resource, rows);
		// TODO Auto-generated constructor stub
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	       // Get the data item for this position
	       HashMap<String, String> row = getItem(position);    
	       // Check if an existing view is being reused, otherwise inflate the view
	       ViewHolder viewHolder; // view lookup cache stored in tag
	       if (convertView == null) {
	          viewHolder = new ViewHolder();
	          LayoutInflater inflater = LayoutInflater.from(getContext());
	          convertView = inflater.inflate(R.layout.lrtareport_row, parent, false);
	          viewHolder.gphead = (TextView) convertView.findViewById(R.id.textGPHead);
	          viewHolder.orgno = (TextView) convertView.findViewById(R.id.textOrgNo);
	          viewHolder.targetnum = (TextView) convertView.findViewById(R.id.textTargetNum);
	          viewHolder.achievenum = (TextView) convertView.findViewById(R.id.textAchieveNum);
	          viewHolder.diffnum = (TextView) convertView.findViewById(R.id.textDiffNum);
	          viewHolder.targetamt = (TextView) convertView.findViewById(R.id.textTargetAmt);
	          viewHolder.achieveamt = (TextView) convertView.findViewById(R.id.textAchieveAmt);
	          viewHolder.diffamt = (TextView) convertView.findViewById(R.id.textDiffAmt);
	          
	          convertView.setTag(viewHolder);
	       } else {
	           viewHolder = (ViewHolder) convertView.getTag();
	       }
	       
	       // Populate the data into the template view using the data object
	       if(row.get("GPHead").isEmpty()) {
	    	   viewHolder.gphead.setVisibility(View.GONE);
	       }
	       else {
	    	   viewHolder.gphead.setVisibility(View.VISIBLE);
	    	   viewHolder.gphead.setText(row.get("GPHead"));
//	    	   viewHolder.memno.setVisibility(View.GONE);
//	    	   viewHolder.memname.setVisibility(View.GONE);
//	    	   viewHolder.disbamt.setVisibility(View.GONE);
//	    	   viewHolder.overdue.setVisibility(View.GONE);
	       }
	       
	       viewHolder.orgno.setText(row.get("OrgNo"));
	       viewHolder.targetnum.setText(row.get("TargetNum"));
	       viewHolder.achievenum.setText(row.get("AchieveNum"));
	       viewHolder.diffnum.setText(row.get("DiffNum"));
	       viewHolder.targetamt.setText(row.get("TargetAmt"));
	       viewHolder.achieveamt.setText(row.get("AchieveAmt"));
	       viewHolder.diffamt.setText(row.get("DiffAmt"));	       
	       // Return the completed view to render on screen
	       
	       if(row.get("OrgNo").equals("Total"))
	    	   convertView.setBackgroundColor(Color.LTGRAY); 
	       return convertView;
	   }	
}
