package net.qsoft.brac.bmfpoerptup;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity;
import net.qsoft.brac.bmfpoerptup.util.CloudRequest;
import net.qsoft.brac.bmfpoerptup.util.SMSListener;

import java.net.URLEncoder;
import java.util.HashMap;

public class MainActivity1 extends SSActivity implements SMSListener {
    private static final String TAG = MainActivity1.class.getSimpleName();
    TextView textSyncInfo;
    TextView collLabel;
    TextView branchLabel;
    TextView dlStatusLabel;
    TextView textVersion;
    boolean startMenuEnabled = true;
    public PO coll;
    private CardView collCard, reportCard;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main1);
        collCard = (CardView) findViewById(R.id.collcardId);
        reportCard = (CardView) findViewById(R.id.repoCardId);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(false);
        }
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(false);
        }
        actionBar.setTitle("BMFPOERP TUP");

        textSyncInfo = (TextView) findViewById(R.id.textSyncInfo);
        collLabel = (TextView) findViewById(R.id.collLabel);
        branchLabel = (TextView) findViewById(R.id.branchLabel);
        dlStatusLabel = (TextView) findViewById(R.id.dlStatuslabel);

        textVersion = (TextView) findViewById(R.id.textVersion);
        String versionName = BuildConfig.VERSION_NAME;
        textVersion.setText("BMFPO Version " + versionName);


//        startButton = (Button) findViewById(R.id.startButton);
//        repoButton = (Button) findViewById(R.id.repoButton);
//        slsButton = (Button) findViewById(R.id.slsButton);
//        int versionCode = BuildConfig.VERSION_CODE;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        invalidateOptionsMenu();
        CheckDataState();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @SuppressLint("NewApi")
    private void CheckDataState() {
        DAO data = new DAO(this);
        data.open();
        coll = data.getPO();
        App.setBranchOPenDate(coll.get_BranchOpenDate());
        App.setLastSyncDate(P8.getLastSyncTime());
        if (coll.get_SMSSupport() > 0)
            App.setSMSActivity(this);
        else
            App.setSMSActivity(null);
        if (coll.isPO()) {
            collLabel.setText(coll.toString());
            textSyncInfo.setText(App.getLastSyncGaps());
            if (coll.get_BranchCode().equals("0000"))
                branchLabel.setText("");
            else
                branchLabel.setText(coll.get_BranchCode() + " - " + coll.get_BranchName());
            branchLabel.setVisibility(View.VISIBLE);
            if (coll.get_EMethod() == PO.EM_DESKTOP) {
                textSyncInfo.setVisibility(View.INVISIBLE);
                dlStatusLabel.setVisibility(View.VISIBLE);
                collCard.setEnabled(false);
                reportCard.setEnabled(false);
            } else {
                textSyncInfo.setVisibility(View.VISIBLE);
                dlStatusLabel.setVisibility(View.INVISIBLE);
                collCard.setEnabled(true);
                reportCard.setEnabled(true);
                coll.get_OpenDate();
            }
        } else {
            branchLabel.setVisibility(View.INVISIBLE);
            dlStatusLabel.setVisibility(View.INVISIBLE);
            collCard.setEnabled(false);
            reportCard.setEnabled(false);
        }
        Log.d(TAG, "From CheckDataState - after if(isPO) block");
        data.close();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // TODO Auto-generated method stub
        super.onPrepareOptionsMenu(menu);
        menu.getItem(0).setEnabled(startMenuEnabled);
        return true;
    }

    public void ReportsCard(View v) {
        PopupMenu popup = new PopupMenu(MainActivity1.this, v);

        //Inflating the Popup using xml file
        popup.getMenuInflater().inflate(R.menu.reports_menu, popup.getMenu());
        popup.getMenu().getItem(4).setVisible(App.hasSMSSupport()); // .setEnabled(App.hasSMSSupport());

        //registering popup with OnMenuItemClickListener
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_rept_overdue) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), R.id.menu_rept_overdue);
                } else if (item.getItemId() == R.id.menu_rept_vo_loan_disb) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), R.id.menu_rept_vo_loan_disb);
                } else if (item.getItemId() == R.id.menu_rept_good_loan) {
                    startActivityForResult(new Intent(App.getContext(), VOResultActivity.class), R.id.menu_rept_good_loan);
                } else if (item.getItemId() == R.id.menu_rept_loan_target_vs_achive) {
                    startActivity(new Intent(App.getContext(), LRTAReportActivity.class));
                } else if (item.getItemId() == R.id.menu_oprtns_smsstat) {
                    startActivity(new Intent(App.getContext(), SMSReportActivity.class));
                }
                return true;
            }
        });

        popup.show();
    }

    public void SeasonalLoanSurvey(View v) {
        startActivity(new Intent(App.getContext(), SeasonalSurveyActivity.class));
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_syncronize) {

            Intent intent = new Intent(getApplicationContext(), SyncDataJacksonJsonParser.class);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.right_out);
            return true;
        } else if (item.getItemId() == R.id.menu_settings) {
            startActivity(new Intent(Settings.ACTION_APPLICATION_SETTINGS));
            return true;
            //startActivity(new Intent(this, SettingsPermActivity.class));
            //overridePendingTransition(R.anim.right_in, R.anim.right_out);
            //startActivity(new Intent(this, SyncSettingsActivity.class));
        } else if (item.getItemId() == R.id.menu_device_id) {
            deviceIdAlert();
            return true;
        } else if (item.getItemId() == R.id.menu_video) {
            startActivity(new Intent(this, ShowVideoActivity.class));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);

            return true;
        } else if (item.getItemId() == R.id.menu_datetime) {
            startActivity(new Intent(Settings.ACTION_DATE_SETTINGS));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);

            return true;
        } else if (item.getItemId() == R.id.menu_exit) {
            Exit();
            return true;
        } else if (item.getItemId() == R.id.menu_power) {
            startActivity(new Intent("android.intent.action.POWER_USAGE_SUMMARY"));
            overridePendingTransition(R.anim.right_in, R.anim.right_out);

            return true;
        }
        return false;
    }

    public void CollModule(View view) {

        collCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(MainActivity1.this, TTActivity.class);
                startActivity(it);
            }
        });

//        // Button onClick - start activity - collector password
//        if (coll.get_Password().length() > 0) {
//            Intent it = new Intent(this, LoginActivity.class);
//            it.putExtra(LoginActivity.EXTRA_PASS, coll.get_Password());
//        } else {
//            Intent it = new Intent(this, TTActivity.class);
//            startActivity(it);
//        }
    }

    public void deviceIdAlert() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.ic_perm_device_information_black_24dp)
                .setTitle("Device ID")
                .setMessage(App.getDeviceId())
                .setNegativeButton("Ok", null)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {

            case (R.id.menu_rept_overdue): {
                if (resultCode == Activity.RESULT_OK) {
                    // Overdue report activity
                    Intent it = new Intent(this, ODReportActivity.class);
                    String vn = data.getStringExtra(P8.VONO);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case (R.id.menu_rept_vo_loan_disb): {
                // VO-wise loan disbursement report
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, VLDReportActivity.class);
                    String vn = data.getStringExtra(P8.VONO);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
            break;

            case (R.id.menu_rept_good_loan): {
                // Good loan list report activity
                if (resultCode == Activity.RESULT_OK) {
                    Intent it = new Intent(this, GoodLoanListActivity.class);
                    String vn = data.getStringExtra(P8.VONO);
                    it.putExtra(P8.VONO, vn);
                    startActivity(it);
                }
            }
        }
    }

    @Override
    public void SendSMS(String branch_code, String project_code,
                        String po_no, String orgno, String orgmemno, String mobile, String message,
                        final String records) {

        String api_key = "i2yRuU6yspWf7NDsrFdkz7vlMrz35SDZ";
        //encoding message
        String encoded_message = URLEncoder.encode(message);

        //Send SMS API
        String mainUrl = App.getSMSSendURL();

        HashMap<String, String> mParams = new HashMap<String, String>();
        mParams.put("mobile", mobile.trim());
        mParams.put("branch_code", branch_code);
        mParams.put("orgno", orgno);
        mParams.put("orgmemno", orgmemno);
        mParams.put("message", message);
        mParams.put("po_no", po_no);
        mParams.put("project_code", project_code);
        mParams.put("api_key", api_key);

        final HashMap<String, String> mp = mParams;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.POST, mainUrl,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //Display the first 500 characters of the response string.
//						mTextView.setText("Response is: "+ response.substring(0,500));
                        String resp = Html.fromHtml(response).toString().trim();
                        String[] st = resp.split(",");
                        if (st[0].trim().equals("200")) {
                            if (records.trim().length() > 0) {
                                DAO da = new DAO(MainActivity1.this);
                                da.open();
                                da.ConfirmSMSSent(records);
                                da.close();
                            }
                        } else {
                            P8.ShowError(MainActivity1.this, resp);
                        }
                    }

                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //mTextView.setText("That didn't work!");
                P8.ShowError(MainActivity1.this, VolleyErrorResponse(error));
            }
        }) {
            @Override
            public HashMap<String, String> getParams() {
                return mp;
            }
        };

        // Add the request to the RequestQueue.
        CloudRequest.getInstance(this).addToRequestQueue(stringRequest);
    }

    public static String VolleyErrorResponse(VolleyError volleyError) {
        // TODO Auto-generated method stub
        String message = null;
        if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        } else {
            message = volleyError.toString();
        }

        try {
            Log.e("myerror", "deal first", volleyError);
            Log.e("myerror", "deal success" + new String(volleyError.networkResponse.data, "utf-8"));
        } catch (Throwable e) {
            Log.e("myerror", "deal fail", e);
        }

        return message;
    }

    @Override
    public void onBackPressed() {
        Exit();
    }

    public void Exit() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Closing Activity")
                .setMessage("Are you sure you want to close application")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent(Intent.ACTION_MAIN);
                        intent.addCategory(Intent.CATEGORY_HOME);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

}

