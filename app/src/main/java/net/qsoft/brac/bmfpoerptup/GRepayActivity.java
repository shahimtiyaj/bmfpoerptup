package net.qsoft.brac.bmfpoerptup;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import net.qsoft.brac.bmfpoerptup.data.CLoan;

public class GRepayActivity extends TBaseActivity {

	/* (non-Javadoc)
	 * @see org.safesave.p8acm.TBaseActivity#onCreate(android.os.Bundle)
	 */
	@Override
	@SuppressLint("NewApi")
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		TType="L";
		setTitle(getString(R.string.description_repay));
//		imageIcon.setImageResource(R.drawable.repaid);
//		imageIcon.setContentDescription(getString(R.string.description_repay));
		instLabel.setVisibility(View.VISIBLE);
		repoButton.setVisibility(View.VISIBLE);
	}

	/* (non-Javadoc)
	 * @see org.safesave.p8acm.TBaseActivity#onResume()
	 */
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();

		cl = CLoan.get_lastCL();
		dispLabel.setText("Loan bal: " + cl.get_LastLB(P8.ToDay()).toString());	//		cl.get_LB().toString());
		dispLabel2.setText("Target: " + cl.get_IAB().toString());
		accLabel.setText(getString(R.string.prompt_account_no) + cl.get_LoanNo().toString());
		instLabel.setText("Inst. passed: " + cl.get_InstlPassed().toString());
	}

	public void onOk(View view) {
		if (P8.IsValidAmount(this, editAmount)) {
			mAmount = Integer.parseInt(editAmount.getText().toString());
			if(mAmount == 0) {
				P8.ErrorSound(this);
				editAmount.setError(getString(R.string.error_zero_number));
				editAmount.requestFocus();
			}
			else {
				if(!BalanceEqual(cl.get_LB(), getString(R.string.description_loan), null, getString(R.string.description_repay)))
					VerifyAmount(getString(R.string.description_repay), null);
			}
		}
	}
	/* (non-Javadoc)
	 * @see org.safesave.p8acm.TBaseActivity#UpdateTransactions()
	 */
	@Override
	protected boolean UpdateTransactions() {
		// TODO Auto-generated method stub
		if(super.UpdateTransactions()) {

			ShowBanglaBalance("এখন ঋণ পরিশোধ", mAmount, "সর্বমোট পরিশোধিত ঋণ", cl.get_TRB());
			finish();
		}
		return false;
	}

	public void onReport(View view) {
	  Intent it = new Intent(this, MLCReportActivity.class);
	  it.putExtra(P8.LOANNO, cl.get_LoanNo());
	  startActivity(it);
	}
}
