package net.qsoft.brac.bmfpoerptup;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.brac.bmfpoerptup.data.CLoan;
import net.qsoft.brac.bmfpoerptup.data.CMember;
import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.data.Transact;
import net.qsoft.brac.bmfpoerptup.data.VO;
import net.qsoft.brac.bmfpoerptup.util.BluetoothService;
import net.qsoft.brac.bmfpoerptup.util.GraphicsPrint;
import net.qsoft.brac.bmfpoerptup.util.SMSListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MempfActivity extends SSActivity implements OnItemClickListener {
    private static final String TAG = MempfActivity.class.getSimpleName();
    private static final boolean D = false;

    final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == P8.MESSAGE_STATE_CHANGE) {
                // Device state changed
                //Toast.makeText(getApplicationContext(), "State: " + Integer.toString(msg.arg1) , Toast.LENGTH_SHORT).show();
                if (msg.arg1 == BluetoothService.STATE_CONNECTED) {
//		        		miniStatement(bts);
//		        		bts.stop();
                }
            } else if (msg.what == P8.MESSAGE_READ) {

            } else if (msg.what == P8.MESSAGE_WRITE) {

            } else if (msg.what == P8.MESSAGE_DEVICE_NAME) {

            } else if (msg.what == P8.MESSAGE_TOAST) {
                Bundle bundle = msg.getData();
                String s = bundle.getString(P8.TOAST);
                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
            }
        }
    };

    Boolean HasPrintModule = false;

    BluetoothService bts;
    boolean isPrinting = false;
    Button okButton;

    TextView tdate;
    TextView textTargetDate;
    TextView textMemNo;
    TextView textMemName;
    TextView textVO;

    ListView lv;

    String vono;
    String memno;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mempf);

        PreferenceManager.setDefaultValues(App.getContext(), R.xml.pref_data_sync, false);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        HasPrintModule = prefs.getBoolean(P8.PREF_HAS_PRINT_MODULE, false);

        okButton = (Button) findViewById(R.id.okButton);
        if (!HasPrintModule) {
            okButton.setVisibility(View.GONE);
        }
        tdate = (TextView) findViewById(R.id.tdate);
        textTargetDate = (TextView) findViewById(R.id.textTargetDate);
        textMemNo = (TextView) findViewById(R.id.textMemNo);
        textMemName = (TextView) findViewById(R.id.textMemName);
        textVO = (TextView) findViewById(R.id.textVO);
        lv = (ListView) findViewById(R.id.lstView);

        tdate.setText(P8.FormatDate(P8.ToDay(), "MMMM dd, yyyy"));

        Intent it = getIntent();

        vono = it.getStringExtra(P8.VONO);
        memno = it.getStringExtra(P8.MEMNO);

        refreshView();


        if (HasPrintModule) {
            bts = new BluetoothService(this, handler);
            bts.start();
        }

    }


    /* (non-Javadoc)
     * @see android.app.Activity#onResume()
     */
    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        refreshView();
    }

    private void refreshView() {
        DAO da = new DAO(this);
        da.open();
        CMember cm = da.getCMember(vono, memno);
        CMember.set_lastCM(cm);
        ArrayList<HashMap<String, String>> alst = da.getLoansByMember(vono, memno, cm);
        da.close();

        // Add Savings
        textTargetDate.setText(P8.FormatDate(cm.get_ColcDate(), "MMMM dd, yyyy"));
        textMemNo.setText(memno);
        textVO.setText(vono);
        textMemName.setText(cm.get_MemberName());


        String[] from = new String[]{"[ProductSymbol]", "[LoanNo]", "[TargetAmtLoan]", "[ReceAmt]", "[StName]", "[ODText]", "[ODAmt]"};
        int[] to = {R.id.textLSym, R.id.textDesc, R.id.textTarget, R.id.textYTR, R.id.textType, R.id.textODText, R.id.textODAmt};

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), alst,
                R.layout.memberpf_row, from, to);
        lv.setAdapter(adapter);
        lv.setOnItemClickListener(this);
    }

    public Boolean SendSMS() {
        Boolean ret = false;
        final CMember c = CMember.get_lastCM();
        final String phone = c.get_PhoneNo().trim();
//		final String phone="01726686188";
        if (phone.length() > 0) {
            DAO da = new DAO(this);
            da.open();
            final PO po = da.getPO();

            final ArrayList<Transact> trans = da.getTransactions(vono, memno, 0);
            da.close();

            if (trans.size() > 0) {
                ret = true;

//				DialogInterface.OnClickListener dialogClick = new DialogInterface.OnClickListener() {
//					@Override
//					public void onClick(DialogInterface dialog, int which) {
//						if(which==DialogInterface.BUTTON_POSITIVE) {
                StringBuilder sb = new StringBuilder(),
                        sbrec = new StringBuilder();
                Transact.prepareSMSText(trans, sb, sbrec);

                if (sb.length() > 0) {
                    SMSListener ss = (SMSListener) App.getSMSActivity();
                    ss.SendSMS(po.get_BranchCode(), c.get_ProjectCode(), po.get_CONo(), vono, memno, phone, sb.toString(), sbrec.toString());
                }
//						}
//						dialog.dismiss();
                MempfActivity.super.finish();
//					}
//				};
//
//				AlertDialog.Builder builder = new AlertDialog.Builder(this);
//				builder.setTitle("SEND SMS")
//						.setMessage("Do you want to send SMS?")
//						.setCancelable(false)
//						.setPositiveButton(R.string.yes, dialogClick)
//						.setNegativeButton(R.string.no, dialogClick).show();
            }
        }
        return ret;
    }

    public void onCancel(View view) {
        // Back
        finish();
    }


    /* (non-Javadoc)
     * @see android.app.Activity#finish()
     */
    @Override
    public void finish() {
        // TODO Auto-generated method stub
        if (HasPrintModule) {
            bts.stop();
        }
        if (App.hasSMSSupport()) {
            if (!SendSMS())
                super.finish();
        } else
            super.finish();
    }


    public void onOk(View view) {
//		final Handler handler = new Handler() {
//			 @Override
//		    public void handleMessage(Message msg){
//		        if(msg.what == P8.MESSAGE_STATE_CHANGE){
//		            // Device state changed
//		        	//Toast.makeText(getApplicationContext(), "State: " + Integer.toString(msg.arg1) , Toast.LENGTH_SHORT).show();
//		        	if(msg.arg1 == BluetoothService.STATE_CONNECTED) {
//		        		miniStatement(bts);
//		        		bts.stop();
//		        	}
//		        } else if(msg.what == P8.MESSAGE_READ){
//
//		        } else if(msg.what == P8.MESSAGE_WRITE) {
//
//		        } else if(msg.what == P8.MESSAGE_DEVICE_NAME) {
//
//		        } else if(msg.what == P8.MESSAGE_TOAST) {
//		        	Bundle bundle = msg.getData();
//		        	String s = bundle.getString(P8.TOAST);
//		        	Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
//		        }
//		    }
//		};
        if (bts.getState() == BluetoothService.STATE_CONNECTED && !isPrinting)
//		if(!isPrinting)
            miniStatement(bts);
//		bts = new BluetoothService(this, handler);
//		bts.start();
    }

    private void miniStatement(BluetoothService bts) {
        String s;

        isPrinting = true;
        //	bts = new BluetoothService(this, handler);
/*		bts.PrinterInit();
        GraphicsPrint gp = new GraphicsPrint(bts);

		gp.draw_line();
		gp.SetText("তারিখ", 48, Paint.Align.CENTER);
		gp.SetText("ঋণ-১", 144, Paint.Align.CENTER);
		gp.SetText("ঋণ-২", 240, Paint.Align.CENTER);
		gp.SetText("সঞ্চয়", 336, Paint.Align.CENTER);
		gp.draw_underline();
		gp.print();
*///		gp.SetText("3538252", 144, Paint.Align.CENTER);
//		gp.SetText("3538257", 240, Paint.Align.CENTER);
//		gp.print();

//		gp.print("HELO WORLD!");
//		gp.print("ব্র্যাক মাইক্রোফাইন্যান্স কর্মসূচির সাথে থাকার জন্য আপনাকে ধন্যবাদ। ভবিষ্যৎ প্রয়োজনে রশিদটি সংরক্ষণ করুন।");

//        View v =  getLayoutInflater().inflate(R.layout.print_test, null);


//		Bitmap bm = GraphicsPrint.loadBitmapFromView(v);

//		gp.print_image(bm);

        try {
            DAO da = new DAO(this);
            da.open();
            PO po = da.getPO();
            VO vo = da.getVO(vono);
//			String stm = da.GetTransForStatement(vono, textMemNo.getText()
//					.toString());
            HashMap<String, String[]> itms = da.GetTransForStmByMember(vono, textMemNo.getText().toString());
            da.close();

//			bts = new BluetoothService(this, handler);
//			bts.start();
//			wait(200);

            bts.PrinterInit();
            //		bts.DefaultFormat();
            bts.Appearance(BluetoothService.BOLD);
            //		bts.ReversePrint(1);
            bts.CenterJustify();
            bts.write("MINI STATEMENT\n".getBytes());
            //		bts.ReversePrint(0);
            bts.Appearance(BluetoothService.UNDERLINE);
            bts.SelectFont(2);
            bts.write(("As on " + P8.getDate() + "\n").getBytes());
            bts.Appearance(0);
            bts.SelectFont(2);
            bts.CenterJustify();
            bts.write(("Branch: " + po.get_BranchCode() + " - "
                    + po.get_BranchName() + "\n").getBytes());
            bts.LeftJustify();
            s = String.format("CO: %-21s %16s\n", po.get_COName(), "Mob: ");
            bts.write(s.getBytes());
            s = String.format("VO: %-4s %33s\n", vono, vo.get_OrgName());
            bts.write(s.getBytes());
            s = String.format("Member: %-6s %27s\n", textMemNo.getText(), textMemName.getText());
            bts.write(s.getBytes());

//			bts.PrinterInit();
            GraphicsPrint gp = new GraphicsPrint(bts);

            gp.draw_line();
//			gp.SetText("তারিখ", 48, Paint.Align.CENTER);
//			gp.SetText("ঋণ-১", 144, Paint.Align.CENTER);
//			gp.SetText("ঋণ-২", 240, Paint.Align.CENTER);
//			gp.SetText("সঞ্চয়", 336, Paint.Align.CENTER);
            gp.SetText("তারিখ", 48, Paint.Align.CENTER);
            gp.SetText("ঋণ-১", 188, Paint.Align.RIGHT);
            gp.SetText("ঋণ-২", 284, Paint.Align.RIGHT);
            gp.SetText("সঞ্চয়", 383, Paint.Align.RIGHT);

//			gp.draw_underline();
            gp.print();

            Typeface face = Typeface.createFromAsset(getAssets(), "fonts/kalpurushANSI.ttf");
            gp.SetTypeFace(face);

            gp.draw_line();

            Map<String, String[]> keySortedMap = new TreeMap<String, String[]>(itms);
            for (String sKey : keySortedMap.keySet()) {
                if (!(sKey.equals("H") || sKey.equals("B"))) {
                    String[] r = itms.get(sKey);
                    gp.SetText(r[0], 48, Paint.Align.CENTER);
                    gp.SetText(r[1], 188, Paint.Align.RIGHT);
                    gp.SetText(r[2], 284, Paint.Align.RIGHT);
                    gp.SetText(r[3], 383, Paint.Align.RIGHT);
                    gp.print();
                }
            }
            gp.draw_line();
            gp.print();

            gp.SetTextSize(22);
            String[] hd = itms.get("H");
            String[] bl = itms.get("B");

            if (hd[1].length() > 0) {
                gp.SetText("ঋণ-১ " + hd[1] + " মোট আদায়:", 284, Paint.Align.RIGHT);
                gp.SetText(bl[1], 383, Paint.Align.RIGHT);
                gp.print();
            }
            if (hd[2].length() > 0) {
                gp.SetText("ঋণ-২ " + hd[2] + " মোট আদায়:", 284, Paint.Align.RIGHT);
                gp.SetText(bl[2], 383, Paint.Align.RIGHT);
                gp.print();
            }

            gp.SetText("মোট সঞ্চয়:", 284, Paint.Align.RIGHT);
            gp.SetText(bl[3], 383, Paint.Align.RIGHT);
            gp.print();
            gp.draw_line();
            gp.print();

            //gp.RefreshSurface(384, 120);
            gp.BottomText("ব্র্যাক মাইক্রোফাইন্যান্স কর্মসূচির সাথে থাকার জন্য আপনাকে ধন্যবাদ। ভবিষ্যত রেফারেন্সের জন্য রশিদটি সংরক্ষন করুন।");
            //gp.print();

/*			bts.write("------------------------------------------\n".getBytes());
			bts.write("Date      Loan No.    Loan Col.  Save Col.\n".getBytes());
			bts.write("------------------------------------------\n".getBytes());
			bts.write(stm.getBytes());
			bts.write("\n".getBytes());

			bts.CenterJustify();
			//		bts.write("ব্র্যাক মাইক্রোফাইন্যান্স কর্মসূচির সাথে থাকার জন্য আপনাকে ধন্যবাদ। ভবিষ্যৎ প্রয়োজনে রশিদটি সংরক্ষণ করুন।\n\n\n".getBytes());
			bts.write("Thank you for being with BRAC Microfinance Programme. Please keep this money receipt for future reference.\n\n\n"
					.getBytes());
//			wait(100);
*/
        } catch (Exception e) {
            // TODO: handle exception
            if (D)
                Log.d(TAG, e.getMessage());
        }
//		finally {
//			if(bts != null) {
//				bts.stop();
//				bts = null;
//			}
//		}
        isPrinting = false;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        // TODO Auto-generated method stub
        String sln = ((HashMap<String, String>) lv.getItemAtPosition(position)).get("[LoanNo]").toString();

        Intent it = new Intent(this, BCheckActivity.class);
        if (sln.equals("Savings")) {
            it.putExtra(P8.TRANS_OPTION, P8.TRANS_OPTION_SAVINGS);
            it.putExtra(P8.BCHECK_BALANCE, CMember.get_lastCM().get_SB());
        } else {
            DAO da = new DAO(this);
            da.open();
            CLoan cl = da.getCloan(Integer.parseInt(sln));
            CLoan.set_lastCL(cl);
            da.close();
            it.putExtra(P8.TRANS_OPTION, P8.TRANS_OPTION_REPAYMENT);
            it.putExtra(P8.BCHECK_BALANCE, cl.get_TRB());
        }

        startActivity(it);

//		Toast.makeText(getBaseContext(), "Loan#/Savings: " + sln , Toast.LENGTH_LONG).show();

    }
}