package net.qsoft.brac.bmfpoerptup.data;

import android.database.Cursor;
import android.util.Log;

import net.qsoft.brac.bmfpoerptup.App;
import net.qsoft.brac.bmfpoerptup.P8;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by Sakeba on 10/4/2017.
 */

public class SurveySL {
    private static final String TAG = SurveySL.class.getSimpleName();

    Boolean bolExists = false;

    Integer _id=0;
    Date SDate= P8.ToDay();
    String OrgNo="";
    String OrgMemNo="";
    Integer Age=0;
    Integer Education=0;
    Integer Home=0;
    Integer OI_No=0;
    Integer OI_DL=0;
    Integer OI_OB=0;
    Integer OI_PB=0;
    Integer OI_OTHERS=0;
    Integer OI_ISOURCE=2;
    Integer CI_NO=0;
    Integer CI_0_5=0;
    Integer CI_5_10=0;
    Integer CI_10_PLUS=0;
    Integer CI_TOTAL=0;
    Integer MA_CC=0;
    Integer MA_OC=0;
    Integer MA_CR=0;
    Integer PL_O_O=0;
    Integer PL_O_P=0;
    Integer PL_L_O=0;
    Integer PL_L_P=0;
    Integer OA_LAND=0;
    Integer OA_TV=0;
    Integer OA_MC=0;
    Integer OA_FR=0;
    Integer PI_NGO=0;
    Integer PI_BANK=0;
    Integer PI_REL=0;
    Integer PI_WS=0;
    Integer PI_OS=0;
    Integer Complete=0;
    Date UpdateAt = new Date();
    Date SentAt = null;

    private static SurveySL lastSurveySL = null;

    public SurveySL() {

    }

    public  SurveySL(String vono, String memno) {
        Cursor curs = null;
        DAO da = new DAO(App.getContext());
        da.open();
        try {
            curs = da.getRecordsCursor("SELECT _id, SDate, OrgNo, OrgMemNo, Age, Education, Home, " +
                    "OI_No, OI_DL, OI_OB, OI_PB, OI_OTHERS, OI_ISOURCE, CI_NO, CI_0_5, " +
                    "CI_5_10, CI_10_PLUS, CI_TOTAL, MA_CC, MA_OC, MA_CR, PL_O_O, PL_O_P, " +
                    "PL_L_O, PL_L_P, OA_LAND, OA_TV, OA_MC, OA_FR, PI_NGO, PI_BANK, PI_REL, " +
                    "PI_WS, PI_OS, Complete, UpdateAt, SentAt FROM SurveySL " +
                    "WHERE OrgNo=? AND OrgMemNo=? " +
                    "ORDER BY SDate DESC LIMIT 1", new String[]{vono, memno});

            curs.moveToFirst();
            if (!curs.isAfterLast()) {
                set_id(curs.getInt(0));
                setSDate(curs.getString(1));
                setOrgNo(curs.getString(2));
                setOrgMemNo(curs.getString(3));
                setAge(curs.getInt(4));
                setEducation(curs.getInt(5));
                setHome(curs.getInt(6));
                setOI_No(curs.getInt(7));
                setOI_DL(curs.getInt(8));
                setOI_OB(curs.getInt(9));
                setOI_PB(curs.getInt(10));
                setOI_OTHERS(curs.getInt(11));
                setOI_ISOURCE(curs.getInt(12));
                setCI_NO(curs.getInt(13));
                setCI_0_5(curs.getInt(14));
                setCI_5_10(curs.getInt(15));
                setCI_10_PLUS(curs.getInt(16));
                setCI_TOTAL(curs.getInt(17));
                setMA_CC(curs.getInt(18));
                setMA_OC(curs.getInt(19));
                setMA_CR(curs.getInt(20));
                setPL_O_O(curs.getInt(21));
                setPL_O_P(curs.getInt(22));
                setPL_L_O(curs.getInt(23));
                setPL_L_P(curs.getInt(24));
                setOA_LAND(curs.getInt(25));
                setOA_TV(curs.getInt(26));
                setOA_MC(curs.getInt(27));
                setOA_FR(curs.getInt(28));
                setPI_NGO(curs.getInt(29));
                setPI_BANK(curs.getInt(30));
                setPI_REL(curs.getInt(31));
                setPI_WS(curs.getInt(32));
                setPI_OS(curs.getInt(33));
                setComplete(curs.getInt(34));

                UpdateAt = P8.ConvertStringToDate(curs.getString(35));
                if(curs.isNull(36))
                    SentAt = null;
                else
                    SentAt = P8.ConvertStringToDate(curs.getString(36));

                bolExists = true;
            }
            else {
                setOrgNo(vono);
                setOrgMemNo(memno);
                bolExists = false;
            }
        }
        catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        finally {
            if(curs != null)
                curs.close();
            if(da != null)
                da.close();
        }
    }

    public Boolean Exists() {
        return bolExists;

    }


    public void Save() throws Exception {
        String sql;

        if(bolExists) {
            // Update database
            DAO.executeSQL("UPDATE SurveySL SET Age=?, Education=?, Home=?, OI_No=?, OI_DL=?, " +
                            "OI_OB=?, OI_PB=?, OI_OTHERS=?, OI_ISOURCE=?, CI_NO=?, CI_0_5=?, " +
                            "CI_5_10=?, CI_10_PLUS=?, CI_TOTAL=?, MA_CC=?, MA_OC=?, MA_CR=?, " +
                            "PL_O_O=?, PL_O_P=?, PL_L_O=?, PL_L_P=?, OA_LAND=?, OA_TV=?, OA_MC=?, " +
                            "OA_FR=?, PI_NGO=?, PI_BANK=?, PI_REL=?, PI_WS=?, PI_OS=?, Complete=?, " +
                            "UpdateAt = DateTime('now', 'localtime') " +
                        "WHERE Date(SDate)=? AND OrgNo=? AND OrgMemNo=?",
                    new String[] {getAge().toString(), getEducation().toString(),
                    getHome().toString(), getOI_No().toString(), getOI_DL().toString(),
                    getOI_OB().toString(), getOI_PB().toString(), getOI_OTHERS().toString(),
                    getOI_ISOURCE().toString(), getCI_NO().toString(), getCI_0_5().toString(),
                    getCI_5_10().toString(), getCI_10_PLUS().toString(), getTotalChildren().toString(),
                    getMA_CC().toString(), getMA_OC().toString(), getMA_CR().toString(),
                    getPL_O_O().toString(), getPL_O_P().toString(), getPL_L_O().toString(),
                    getPL_L_P().toString(), getOA_LAND().toString(), getOA_TV().toString(),
                    getOA_MC().toString(), getOA_FR().toString(), getPI_NGO().toString(),
                    getPI_BANK().toString(), getPI_REL().toString(), getPI_WS().toString(),
                    getPI_OS().toString(), getComplete().toString(),
                    getSDateString(), getOrgNo(), getOrgMemNo()});

        }
        else if(getOrgNo().length() > 0 && getOrgMemNo().length() > 0) {
            // Insert new data

            DAO.executeSQL("INSERT INTO SurveySL(SDate, OrgNo, OrgMemNo, Age, Education, Home, " +
                    "OI_No, OI_DL, OI_OB, OI_PB, OI_OTHERS, OI_ISOURCE, CI_NO, CI_0_5, " +
                    "CI_5_10, CI_10_PLUS, CI_TOTAL, MA_CC, MA_OC, MA_CR, PL_O_O, PL_O_P, " +
                    "PL_L_O, PL_L_P, OA_LAND, OA_TV, OA_MC, OA_FR, PI_NGO, PI_BANK, PI_REL, " +
                    "PI_WS, PI_OS, Complete) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " + "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                        "?, ?, ?)", new String[] {getSDateString(),
                    getOrgNo(), getOrgMemNo(), getAge().toString(), getEducation().toString(),
                    getHome().toString(), getOI_No().toString(), getOI_DL().toString(),
                    getOI_OB().toString(), getOI_PB().toString(), getOI_OTHERS().toString(),
                    getOI_ISOURCE().toString(), getCI_NO().toString(), getCI_0_5().toString(),
                    getCI_5_10().toString(), getCI_10_PLUS().toString(), getTotalChildren().toString(),
                    getMA_CC().toString(), getMA_OC().toString(), getMA_CR().toString(),
                    getPL_O_O().toString(), getPL_O_P().toString(), getPL_L_O().toString(),
                    getPL_L_P().toString(), getOA_LAND().toString(), getOA_TV().toString(),
                    getOA_MC().toString(), getOA_FR().toString(), getPI_NGO().toString(),
                    getPI_BANK().toString(), getPI_REL().toString(), getPI_WS().toString(),
                    getPI_OS().toString(), getComplete().toString()});
        }
    }

    public static SurveySL getLastSurveySL() {
        return lastSurveySL;
    }

    public static void setLastSurveySL(SurveySL lastSurveySL) {
        SurveySL.lastSurveySL = lastSurveySL;
    }


    public Integer get_id() {
        return _id;
    }

    public void set_id(Integer _id) {
        this._id = _id;
    }

    public String getSDateString() {
        return P8.FormatDate(getSDate(), "yyyy-MM-dd");
    }

    public String getSDateFormated() {
        return P8.FormatDate(getSDate(), "dd-MMM-yyyy");
    }

    public Date getSDate() {
        return SDate;
    }

    public void setSDate(Date SDate) {
        this.SDate = SDate;
    }
    public void setSDate(String SDate) {
        this.SDate = P8.ConvertStringToDate(SDate, "yyyy-MM-dd");
    }

    public String getOrgNo() {
        return OrgNo;
    }

    public void setOrgNo(String orgNo) {
        OrgNo = orgNo;
    }

    public String getOrgMemNo() {
        return OrgMemNo;
    }

    public void setOrgMemNo(String orgMemNo) {
        OrgMemNo = orgMemNo;
    }

    public Integer getAge() {
        return Age;
    }

    public void setAge(Integer age) {
        Age = age;
    }

    public Integer getEducation() {
        return Education;
    }

    public void setEducation(Integer education) {
        Education = education;
    }

    public Integer getHome() {
        return Home;
    }

    public void setHome(Integer home) {
        Home = home;
    }

    public Integer getOI_No() {
        return OI_No;
    }

    public void setOI_No(Integer OI_No) {
        this.OI_No = OI_No;
    }

    public Integer getOI_DL() {
        return OI_DL;
    }

    public void setOI_DL(Integer OI_DL) {
        this.OI_DL = OI_DL;
    }

    public Integer getOI_OB() {
        return OI_OB;
    }

    public void setOI_OB(Integer OI_OB) {
        this.OI_OB = OI_OB;
    }

    public Integer getOI_PB() {
        return OI_PB;
    }

    public void setOI_PB(Integer OI_PB) {
        this.OI_PB = OI_PB;
    }

    public Integer getOI_OTHERS() {
        return OI_OTHERS;
    }

    public void setOI_OTHERS(Integer OI_OTHERS) {
        this.OI_OTHERS = OI_OTHERS;
    }

    public Integer getOI_ISOURCE() {
        return OI_ISOURCE;
    }

    public void setOI_ISOURCE(Integer OI_ISOURCE) {
        this.OI_ISOURCE = OI_ISOURCE;
    }

    public Integer getCI_NO() {
        return CI_NO;
    }

    public void setCI_NO(Integer CI_NO) {
        this.CI_NO = CI_NO;
    }

    public Integer getCI_0_5() {
        return CI_0_5;
    }

    public void setCI_0_5(Integer CI_0_5) {
        this.CI_0_5 = CI_0_5;
    }

    public Integer getCI_5_10() {
        return CI_5_10;
    }

    public void setCI_5_10(Integer CI_5_10) {
        this.CI_5_10 = CI_5_10;
    }

    public Integer getTotalChildren() {
        return getCI_0_5() + getCI_5_10() + getCI_10_PLUS();
    }

    public Integer getCI_10_PLUS() {
        return CI_10_PLUS;
    }

    public void setCI_10_PLUS(Integer CI_10_PLUS) {
        this.CI_10_PLUS = CI_10_PLUS;
    }

    public Integer getCI_TOTAL() {
        return CI_TOTAL;
    }

    public void setCI_TOTAL(Integer CI_TOTAL) {
        this.CI_TOTAL = CI_TOTAL;
    }

    public Integer getMA_CC() {
        return MA_CC;
    }

    public void setMA_CC(Integer MA_CC) {
        this.MA_CC = MA_CC;
    }

    public Integer getMA_OC() {
        return MA_OC;
    }

    public void setMA_OC(Integer MA_OC) {
        this.MA_OC = MA_OC;
    }

    public Integer getMA_CR() {
        return MA_CR;
    }

    public void setMA_CR(Integer MA_CR) {
        this.MA_CR = MA_CR;
    }

    public Integer getPL_O_O() {
        return PL_O_O;
    }

    public void setPL_O_O(Integer PL_O_O) {
        this.PL_O_O = PL_O_O;
    }

    public Integer getPL_O_P() {
        return PL_O_P;
    }

    public void setPL_O_P(Integer PL_O_P) {
        this.PL_O_P = PL_O_P;
    }

    public Integer getPL_L_O() {
        return PL_L_O;
    }

    public void setPL_L_O(Integer PL_L_O) {
        this.PL_L_O = PL_L_O;
    }

    public Integer getPL_L_P() {
        return PL_L_P;
    }

    public void setPL_L_P(Integer PL_L_P) {
        this.PL_L_P = PL_L_P;
    }

    public Integer getOA_LAND() {
        return OA_LAND;
    }

    public void setOA_LAND(Integer OA_LAND) {
        this.OA_LAND = OA_LAND;
    }

    public Integer getOA_TV() {
        return OA_TV;
    }

    public void setOA_TV(Integer OA_TV) {
        this.OA_TV = OA_TV;
    }

    public Integer getOA_MC() {
        return OA_MC;
    }

    public void setOA_MC(Integer OA_MC) {
        this.OA_MC = OA_MC;
    }

    public Integer getOA_FR() {
        return OA_FR;
    }

    public void setOA_FR(Integer OA_FR) {
        this.OA_FR = OA_FR;
    }

    public Integer getPI_NGO() {
        return PI_NGO;
    }

    public void setPI_NGO(Integer PI_NGO) {
        this.PI_NGO = PI_NGO;
    }

    public Integer getPI_BANK() {
        return PI_BANK;
    }

    public void setPI_BANK(Integer PI_BANK) {
        this.PI_BANK = PI_BANK;
    }

    public Integer getPI_REL() {
        return PI_REL;
    }

    public void setPI_REL(Integer PI_REL) {
        this.PI_REL = PI_REL;
    }

    public Integer getPI_WS() {
        return PI_WS;
    }

    public void setPI_WS(Integer PI_WS) {
        this.PI_WS = PI_WS;
    }

    public Integer getPI_OS() {
        return PI_OS;
    }

    public void setPI_OS(Integer PI_OS) {
        this.PI_OS = PI_OS;
    }

    public Integer getComplete() {
        return Complete;
    }

    public void setComplete(Integer complete) {
        Complete = complete;
    }

    public Date getUpdateAt() {
        return UpdateAt;
    }

    public Date getSentAt() {
        return SentAt;
    }

    public Boolean isSent() {
        if(SentAt == null)
            return false;
        else
            return true;
    }

    public static ArrayList<HashMap<String, String>> getMemberList(String vono) {
        ArrayList<HashMap<String, String>> ret = new ArrayList<>();
        Cursor curs = null;

        DAO da = new DAO(App.getContext());
        da.open();

        try {
            curs =  da.getRecordsCursor("SELECT [OrgNo], [OrgMemNo], [MemberName] FROM " +
                    DBHelper.TBL_SLSMEMLIST +" WHERE [OrgNo]=? ORDER BY [OrgNo], [OrgMemNo]", new String[] {vono});

            if (curs.moveToFirst()) {
                do {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(DBHelper.FLD_ORG_NO, curs.getString(0));
                    map.put(DBHelper.FLD_ORG_MEM_NO, curs.getString(1));
                    map.put(DBHelper.FLD_ORG_MEM_NAME, curs.getString(2));

                    ret.add(map);

                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
            if(da != null)
                da.close();
        }

        return ret;
    }

    public static ArrayList<HashMap<String, String>> getVOList() {
        ArrayList<HashMap<String, String>> ret = new ArrayList<>();
        Cursor curs = null;

        DAO da = new DAO(App.getContext());
        da.open();

        try {
            curs = da.getRecordsCursor("SELECT [OrgNo], [OrgName] FROM " +
                    DBHelper.TBL_SLSVOLIST +" ORDER BY [OrgNo]", null);

            if (curs.moveToFirst()) {
                do {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(DBHelper.FLD_ORG_NO, curs.getString(0));
                    map.put(DBHelper.FLD_ORG_NAME, curs.getString(1));

                    ret.add(map);

                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
            if(da != null)
                da.close();
        }

        return ret;
    }

    // Retrieve member list having survey data
    public static ArrayList<HashMap<String, String>> getSurveyDataMemberList() {
        ArrayList<HashMap<String, String>> ret = new ArrayList<>();
        Cursor curs = null;

        DAO da = new DAO(App.getContext());
        da.open();

        try {
            curs =  da.getRecordsCursor("SELECT S.[OrgNo], S.[OrgMemNo], M.[MemberName], S.[SDate] " +
                    "FROM " + DBHelper.TBL_SURVEYSL + " S INNER JOIN " + DBHelper.TBL_SLSMEMLIST + " M " +
                        "ON S.[OrgNo]=M.[OrgNo] AND S.[OrgMemNo]=M.[OrgMemNo] " +
                    "ORDER BY S.[OrgNo], S.[OrgMemNo], S.[SDate]", null);

            if (curs.moveToFirst()) {
                do {
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(DBHelper.FLD_ORG_NO, curs.getString(0));
                    map.put(DBHelper.FLD_ORG_MEM_NO, curs.getString(1));
                    map.put(DBHelper.FLD_ORG_MEM_NAME, curs.getString(2));

                    ret.add(map);

                } while (curs.moveToNext());
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            Log.e(TAG, e.toString());

        } finally {
            if (curs != null)
                curs.close();
            if(da != null)
                da.close();
        }

        return ret;
    }

}
