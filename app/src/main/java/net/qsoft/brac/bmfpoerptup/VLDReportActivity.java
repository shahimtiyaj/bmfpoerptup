package net.qsoft.brac.bmfpoerptup;

import android.content.Intent;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.DBHelper;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.data.VO;

import java.util.ArrayList;
import java.util.HashMap;

public class VLDReportActivity extends SSActivity {
	private static final String TAG = VLDReportActivity.class.getSimpleName();

	TextView branchName;
	TextView voName;
	ListView lv;
	Button okButton;

	ArrayList<HashMap<String,String>> items = null;
	String vono;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vldreport);

		branchName = (TextView) findViewById(R.id.textBranchName);
		voName = (TextView) findViewById(R.id.textVOName);

		lv = (ListView)findViewById(R.id.listVLDR);
		okButton = (Button) findViewById(R.id.okButton);

		okButton.setVisibility(View.GONE);

		if(getIntent().hasExtra(P8.VONO)) {
			vono = getIntent().getExtras().getString(P8.VONO);
//			Log.d(TAG, "In has extra " + vono);
		}

		if(VERSION.SDK_INT >= VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}

		createVLDReport();
	}

	private void createVLDReport() {
		DAO da = new DAO(this);
		da.open();
		PO po = da.getPO();
		VO vo = da.getVO(vono);
		items = da.getVODisbursementReport(vono);
		da.close();

		branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());
		voName.setText(vo.get_OrgNo() + " - " + vo.get_OrgName());

        String[] from = new String[] {DBHelper.FLD_DISBURSEMENT_DATE, DBHelper.FLD_PRINCIPAL_AMT, "LDCount"};
        int[] to = { R.id.textDisbDate, R.id.textDisbAmt, R.id.textDisbCount };

        SimpleAdapter adapter = new SimpleAdapter(getApplicationContext(), items, R.layout.vldreport_row, from, to);
        lv.setAdapter(adapter);

	    lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            	HashMap<String, String> x = (HashMap<String, String>) lv.getItemAtPosition(position);

//            	Log.d(TAG, "VO Form ItemClick: " + vono + "\nddate: " +x.get(DBHelper.FLD_DISBURSEMENT_DATE));

            	String ddate = P8.FormatDate(
            			P8.ConvertStringToDate(x.get(DBHelper.FLD_DISBURSEMENT_DATE), "dd-MMM-yyyy"),
            			"yyyy-MM-dd hh:mm:ss");

            	Intent it = new Intent(getApplicationContext(), VLDReportDetailsActivity.class);
            	it.putExtra(P8.VONO, vono);
            	it.putExtra(P8.DDATE, ddate);
            	startActivity(it);
            	
//                Toast.makeText(getBaseContext(), vono + "-" + memno, Toast.LENGTH_LONG).show();
            }
        });

	}

	public void onCancel(View view) {
		// Back
		finish();
	}	
	
}
