package net.qsoft.brac.bmfpoerptup;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import net.qsoft.brac.bmfpoerptup.Volley.Utils;
import net.qsoft.brac.bmfpoerptup.data.DAO;
import net.qsoft.brac.bmfpoerptup.data.DBHelper;
import net.qsoft.brac.bmfpoerptup.data.PO;
import net.qsoft.brac.bmfpoerptup.data.Transact;
import net.qsoft.brac.bmfpoerptup.util.SMSListener;

import java.util.ArrayList;
import java.util.HashMap;

public class SMSReportActivity extends SSActivity {
    private static final String TAG = SMSReportActivity.class.getSimpleName();

    ListView lv = null;
    TextView branchName;
    TextView coName;
    Button cmdOK;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView text;

   // public static Integer cnt = 0;


    public static ArrayList<HashMap<String, String>> items = null;
    public static HashMap<String, String[]> pendingList = new HashMap<String, String[]>();
    PO po = null;
//    String vono=null;
//    String pono=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smsreport);
        //mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);

        cmdOK = (Button) findViewById(R.id.okButton);
//        cmdOK.setVisibility(View.GONE);
        cmdOK.setText("Send All Pending SMS");


//        branchName = (TextView) findViewById(R.id.textBranchName);
//        coName = (TextView) findViewById(R.id.textCOName);

        lv = (ListView) findViewById(R.id.listViewSMS);

        setTitle("SMS Send Status");

    }

    @Override
    protected void onResume() {
        super.onResume();

        createSMSSentStatus();
    }

    public static void preparePendingList() {

        HashMap<String, String> t;

        for (int i = 0; i < items.size(); i++) {
            t = items.get(i);
            if (t.get(DBHelper.FLD_SMS_SENT_STATUS).equals("Pending")) {
                String vono = t.get(DBHelper.FLD_ORG_NO);
                String memno = t.get(DBHelper.FLD_ORG_MEM_NO);
                if (!pendingList.containsKey(vono + memno)) {
                    String phone = t.get(DBHelper.FLD_ORG_MEM_PHONE);
                    pendingList.put(vono + memno, new String[]{vono, memno, phone});
                }
            }
        }

    }


    @SuppressLint("SetTextI18n")
    private void createSMSSentStatus() {
//		Log.d(TAG, "VO No.: " + (von==null ? "": von));

        DAO da = new DAO(this);
        da.open();
        items = da.getSMSRepor();
        po = da.getPO();
        da.close();
        //  preparePendingList();

        if (pendingList.size() > 0)
            cmdOK.setVisibility(View.VISIBLE);
        else
            cmdOK.setVisibility(View.GONE);

//        branchName.setText(po.get_BranchCode() + " - " + po.get_BranchName());
//        coName.setText(pon + " - " + co.get_COName());

        String[] from = new String[]{DBHelper.FLD_ORG_NO, DBHelper.FLD_ORG_MEM_NO, DBHelper.FLD_ORG_MEM_NAME,
                DBHelper.FLD_ORG_MEM_PHONE, DBHelper.FLD_COLC_AMT, DBHelper.FLD_COLC_FOR, DBHelper.FLD_SMS_SENT_STATUS};
        int[] to = {R.id.textOrgNo, R.id.textOrgMemNo, R.id.textOrgMemName, R.id.textPhone,
                R.id.textAmount, R.id.textType, R.id.textStatus};

        final SimpleAdapter adapter = new SimpleAdapter(this, items, R.layout.sms_report_row, from, to) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                text = (TextView) view.findViewById(R.id.textStatus);
                if (text.getText().equals("Pending"))
                    view.setBackgroundColor(getResources().getColor(R.color.Orange));
                else if (text.getText().equals("Sent"))
                    view.setBackgroundColor(getResources().getColor(R.color.LightGreen));
                else
                    view.setBackgroundColor(getResources().getColor(R.color.report_background));
                return view;
            }
        };

        lv.setAdapter(adapter);
//        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                preparePendingList();
//            }
//        });

        adapter.notifyDataSetChanged();

    }


    public void SendPendingSMS() {
        // Prepare member list
         Integer cnt = 0;

        SMSListener ss = (SMSListener) App.getSMSActivity();
        DAO da = new DAO(this);
        da.open();

        try {
            for (String[] x : pendingList.values()) {
                String vono = x[0];
                String memno = x[1];
                String phone = x[2];
//                P8.ShowError(this, vono + " " + memno + " " + phone);
                ArrayList<Transact> trans = da.getTransactions(vono, memno, 0);

                if (trans.size() > 0) {
                    String projCode = trans.get(0).get_ProjectCode();
                    //                  P8.ShowError(this, projCode);;
                    StringBuilder sb = new StringBuilder(),
                            sbrec = new StringBuilder();
                    Transact.prepareSMSText(trans, sb, sbrec);
//                    P8.ShowError(this, sb.toString());
                    if (sb.length() > 0) {
                        ss.SendSMS(po.get_BranchCode(), projCode, po.get_CONo(), vono, memno, phone, sb.toString(), sbrec.toString());
                        cnt++;
                    }
                }

            }

            if (cnt == 0)
                P8.ShowError(this, "No Pending SMS !!!");
        } catch (Exception ex) {
            P8.ShowError(this, ex.getMessage());
        } finally {
            da.close();
        }
    }

    public void onOk(View view) {

        // Send all pending SMS
        DialogInterface.OnClickListener dialogClick = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    if ((Utils.isNetworkAvailable(getApplicationContext()))) {
                        SendPendingSMS();
                        finish();
                        startActivity(getIntent());
                        // cmdOK.setVisibility(View.GONE);
                        // Toast.makeText(getApplicationContext(), "SMS has been sent successfully!", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Please check your internet connection!!", Toast.LENGTH_LONG).show();
                    }
                }
                dialog.dismiss();
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("SEND SMS")
                .setMessage("Do you want to send all pending SMS's?")
                .setCancelable(false)
                .setPositiveButton(R.string.yes, dialogClick)
                .setNegativeButton(R.string.no, dialogClick).show();
    }


    public void onCancel(View view) {
        // Back
        finish();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onRestart() {
        super.onRestart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}