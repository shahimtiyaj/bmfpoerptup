package net.qsoft.brac.bmfpoerptup.sls;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import net.qsoft.brac.bmfpoerptup.R;
import net.qsoft.brac.bmfpoerptup.data.SurveySL;

import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.getRGGetValue;
import static net.qsoft.brac.bmfpoerptup.sls.SeasonalSurveyActivity.setRGClearAndSet;


public class Pg5 extends android.app.Fragment {

    RadioGroup rPLOO,rPLOP,rPLLO,rPLLP;

    public Pg5() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.sls_fragment_pg5, container, false);

            rPLOO=(RadioGroup) v.findViewById(R.id.radioPLOO);
            rPLOP=(RadioGroup) v.findViewById(R.id.radioPLOP);
            rPLLO=(RadioGroup) v.findViewById(R.id.radioPLLO);
            rPLLP=(RadioGroup) v.findViewById(R.id.radioPLLP);

        return v;
    }


    public void onResume(){
        super.onResume();
        SurveySL ssl = SurveySL.getLastSurveySL();

        setRGClearAndSet(rPLOO, ssl.getPL_O_O());
        setRGClearAndSet(rPLOP, ssl.getPL_O_P());
        setRGClearAndSet(rPLLO, ssl.getPL_L_O());
        setRGClearAndSet(rPLLP, ssl.getPL_L_P());

    }

    // @Override
    public void onPause() {
        super.onPause();
        SurveySL ssl = SurveySL.getLastSurveySL();
        ssl.setPL_O_O(getRGGetValue(rPLOO));
        ssl.setPL_O_P(getRGGetValue(rPLOP));
        ssl.setPL_L_O(getRGGetValue(rPLLO));
        ssl.setPL_L_P(getRGGetValue(rPLLP));
    }
}